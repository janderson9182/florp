<?php

namespace SpaceCadets\Florp\Services;

/**
 * Class UseCase
 * @package SpaceCadets\Florp\Services
 * @author James Anderson 40127848
 *
 * Base class for creating use cases
 * When this class is extended it can be used as follows:
 * YourClass::create()->execute($optionallyPassedInParameters);
 *
 * When you extend this class you are expected to create an execute method to follow the use case design pattern
 */
abstract class UseCase
{
    /**
     * UseCase constructor.
     * This can never be called outside of this class
     */
    private final function __construct()
    {
    }

    /**
     * @return static instance of this class
     */
    final static public function create()
    {
        return new static();
    }
}