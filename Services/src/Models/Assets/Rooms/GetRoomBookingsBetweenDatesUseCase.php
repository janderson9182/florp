<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 16/02/2018
 * Time: 18:33
 */

namespace SpaceCadets\Florp\Services\src\Models\Assets\Rooms;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Collections\RepositoryCollection;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\GreaterThan;
use Rhubarb\Stem\Filters\LessThan;
use Rhubarb\Stem\Filters\OrGroup;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetActiveBookingsBetweenDatesUseCase;
use SpaceCadets\Florp\Services\UseCase;

class GetRoomBookingsBetweenDatesUseCase extends UseCase
{
    /**
     * @param Room $room
     * @param RhubarbDateTime $startTime
     * @param RhubarbDateTime $endTime
     * @param bool $inclusive
     * @return RepositoryCollection|Booking[]
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    public function execute(
        Room $room,
        RhubarbDateTime $startTime,
        RhubarbDateTime $endTime,
        bool $inclusive = false
    ): RepositoryCollection
    {
        return $room->Bookings->filter
        (
            GetActiveBookingsBetweenDatesUseCase::create()->execute($startTime, $endTime, $inclusive)->getFilter()
        );
    }
}