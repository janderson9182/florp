<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 16/02/2018
 * Time: 14:03
 */

namespace SpaceCadets\Florp\Services\src\Models\Assets\Rooms;

use Rhubarb\Stem\Collections\Collection;
use Rhubarb\Stem\Filters\GreaterThan;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Services\UseCase;

class GetRoomsToFitNumberUseCase extends UseCase
{
    /**
     * @param int $numberOfPeople
     * @return Collection|Room[]
     */
    public function execute(int $numberOfPeople) : Collection
    {
        return Room::find(new GreaterThan(Room::COLUMN_NUMBER_OF_SEATS, $numberOfPeople, true));
    }
}