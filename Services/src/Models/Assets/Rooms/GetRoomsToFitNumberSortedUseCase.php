<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 16/02/2018
 * Time: 14:13
 */

namespace SpaceCadets\Florp\Services\src\Models\Assets\Rooms;

use Rhubarb\Stem\Collections\RepositoryCollection;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Services\UseCase;

class GetRoomsToFitNumberSortedUseCase extends UseCase
{
    /**
     * @param int $number
     * @param bool $smallestFirst
     * @return RepositoryCollection|Room[]
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    public function execute(int $number, bool $smallestFirst = true): RepositoryCollection
    {
        return GetRoomsToFitNumberUseCase::create()
            ->execute($number)
            ->addSort(Room::COLUMN_NUMBER_OF_SEATS, $smallestFirst);
    }
}