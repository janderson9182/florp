<?php
/**
 * Created by PhpStorm.
 * User: ron_0
 * Date: 27/03/2018
 * Time: 01:14
 */

namespace SpaceCadets\Florp\Services\src\Models\Assets\Rooms;


use Rhubarb\Stem\Collections\Collection;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Services\UseCase;

class GetRoomsUseCase extends UseCase
{
    /**
     * @return Collection|Room[]
     */

    //Simple use case to return all rooms for a building
    //TODO - Needed?
    public function execute() : Collection
    {
        return Room::all();
    }

}