<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 16/02/2018
 * Time: 14:22
 */

namespace SpaceCadets\Florp\Services\src\Models\Assets\Rooms;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Services\UseCase;

class IsRoomAvailableBetweenDatesUseCase extends UseCase
{
    /**
     * @param Room $room
     * @param RhubarbDateTime $startTime
     * @param RhubarbDateTime $endTime
     * @return bool
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    public function execute(Room $room, RhubarbDateTime $startTime, RhubarbDateTime $endTime): bool
    {
        // Otherwise get all of the bookings for that day, and manually check for intersections
        return GetRoomBookingsBetweenDatesUseCase::create()

                ->execute($room, $startTime, $endTime, true)
                ->count() <1;
    }
}