<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 10/01/18
 * Time: 19:40
 */

namespace SpaceCadets\Florp\Services\src\Models\Bookings;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Collections\Collection;
use Rhubarb\Stem\Exceptions\FilterNotSupportedException;
use Rhubarb\Stem\Filters\GreaterThan;
use SpaceCadets\Florp\Models\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\UseCase;

class GetActiveBookingRequestsUseCase extends UseCase
{
    /**
     * @return Collection|Booking[]
     * @throws FilterNotSupportedException
     */
    public function execute(): Collection
    {
        return BookingRequest::all()
            ->filter(new GreaterThan("StartTime", new RhubarbDateTime("now"), true));
    }
}