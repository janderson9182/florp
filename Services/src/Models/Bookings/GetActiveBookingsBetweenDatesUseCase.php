<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 24/02/2018
 * Time: 12:54
 */

namespace SpaceCadets\Florp\Services\src\Models\Bookings;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Collections\RepositoryCollection;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\GreaterThan;
use Rhubarb\Stem\Filters\LessThan;
use Rhubarb\Stem\Filters\OrGroup;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\UseCase;

class GetActiveBookingsBetweenDatesUseCase extends UseCase
{
    public function execute(
        RhubarbDateTime $startTime,
        RhubarbDateTime $endTime,
        bool $inclusive = true
    ): RepositoryCollection {
        return Booking::find(
            new AndGroup(
                new OrGroup
                (
                    [
                        //AndGroup that catches all bookings that start within the timeframe.
                        new AndGroup
                        (
                            [
                                new GreaterThan
                                (
                                    Booking::COLUMN_START_TIME,
                                    $startTime,
                                    $inclusive
                                ),
                                new LessThan
                                (
                                    Booking::COLUMN_START_TIME,
                                    $endTime,
                                    $inclusive
                                )
                            ]
                        ),
                        //AndGroup that catches all bookings that end within the timeframe.
                        new AndGroup
                        (
                            [
                                new GreaterThan
                                (
                                    Booking::COLUMN_END_TIME,
                                    $startTime,
                                    $inclusive
                                ),
                                new LessThan
                                (
                                    Booking::COLUMN_END_TIME,
                                    $endTime,
                                    $inclusive
                                )
                            ]
                        ),
                        //AndGroup that catches all bookings begin and end outside the time frame, but overlap it.
                        new AndGroup
                        (
                            [
                                new LessThan
                                (
                                    Booking::COLUMN_START_TIME,
                                    $startTime,
                                    $inclusive
                                ),
                                new GreaterThan
                                (
                                    Booking::COLUMN_END_TIME,
                                    $endTime,
                                    $inclusive
                                )
                            ]
                        )
                    ]
                ),
                new Equals(
                    Booking::COLUMN_CANCELLED,
                    false
                )
            )
        );
    }
}