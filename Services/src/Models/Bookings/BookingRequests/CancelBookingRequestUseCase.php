<?php

namespace SpaceCadets\Florp\Services\src\Models\Bookings\BookingRequests;

use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\UseCase;

class CancelBookingRequestUseCase extends UseCase
{
    public function execute(BookingRequest $bookingRequest) : BookingRequest
    {
        $bookingRequest->Cancelled = true;
        if ($bookingRequest->BookingId > 0) {
            $bookingRequest->Booking->Cancelled = true;
            $bookingRequest->Booking->save();
        }
        $bookingRequest->save();
        return $bookingRequest;
    }
}