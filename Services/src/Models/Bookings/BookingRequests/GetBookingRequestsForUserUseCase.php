<?php

namespace SpaceCadets\Florp\Services\src\Models\Bookings\BookingRequests;

use Rhubarb\Scaffolds\Authentication\User;
use Rhubarb\Stem\Collections\Collection;
use Rhubarb\Stem\Filters\Equals;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\UseCase;

class GetBookingRequestsForUserUseCase extends UseCase
{
    /**
     * @param User $user
     * @return Collection|BookingRequest[]
     */
    public function execute(User $user): Collection
    {
        return BookingRequest::find(
            new Equals(
                BookingRequest::COLUMN_USER_ID,
                $user->UserID
            )
        );
    }
}