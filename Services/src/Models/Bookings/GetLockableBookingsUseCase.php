<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 18/12/17
 * Time: 10:58
 */

namespace SpaceCadets\Florp\Services\src\Models\Bookings;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Collections\Collection;
use Rhubarb\Stem\Exceptions\FilterNotSupportedException;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\GreaterThan;
use Rhubarb\Stem\Filters\LessThan;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\UseCase;

class GetLockableBookingsUseCase extends UseCase
{
    /**
     * @return Collection|Booking[]
     * @throws FilterNotSupportedException
     */
    public function execute(): Collection
    {
        $tomorrow = new RhubarbDateTime("+1 day");
        $bookings = Booking::all();
        $bookings->filter(
            new AndGroup(
                [
                    new LessThan(
                        Booking::COLUMN_START_TIME,
                        $tomorrow,
                        true
                    ),
                    new Equals(
                        Booking::COLUMN_LOCKED,
                        false
                    )
                ]
            ));

        return $bookings;
    }
}