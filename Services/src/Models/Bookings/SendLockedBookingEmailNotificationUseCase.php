<?php
/**
 * Created by PhpStorm.
 * User: ron_0
 * Date: 30/03/2018
 * Time: 15:15
 */

namespace SpaceCadets\Florp\Services\src\Models\Bookings;


use Rhubarb\Crown\Sendables\Email\EmailProvider;
use Rhubarb\Crown\Sendables\Email\EmailRecipient;
use Rhubarb\Crown\Sendables\Email\EmailSettings;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingIsLockedNotificationEmail;
use SpaceCadets\Florp\Services\UseCase;

class SendLockedBookingEmailNotificationUseCase extends UseCase
{
    //RON: when each booking is locked, an email notification should be sent
    /**
     * @param Booking $booking
     * @throws \Rhubarb\Crown\Exceptions\EmailException
     */
    public function execute(Booking $booking){
        EmailSettings::singleton()->OnlyRecipient = false;
        //Get the user email through the booking's booking request's user
        if ($booking->BookingRequest){
            if ($booking->BookingRequest->User->Email != "") {
                $bookingUserEmailAddress = $booking->BookingRequest->User->Email;
                $bookingLockedEmail = new BookingIsLockedNotificationEmail([$bookingUserEmailAddress], $booking);
                EmailSettings::singleton()->defaultSender = new EmailRecipient("donotreply@florp-booking.com");
                EmailProvider::selectProviderAndSend($bookingLockedEmail);
            }
        }
    }
}