<?php

namespace SpaceCadets\Florp\Services\src\Models\Bookings;

use SpaceCadets\Florp\Services\UseCase;

class LockBookingsUseCase extends UseCase
{
    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function execute()
    {
        $bookings = GetLockableBookingsUseCase::create()->execute();
        foreach ($bookings as $booking) {
            $booking->Locked = true;
            $booking->save();
            SendLockedBookingEmailNotificationUseCase::create()->execute($booking);
        }
    }
}