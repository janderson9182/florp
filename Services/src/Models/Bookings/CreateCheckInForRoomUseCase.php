<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 20-Feb-18
 * Time: 17:36
 */

namespace SpaceCadets\Florp\Services\src\Models\Bookings;


use Rhubarb\Crown\DateTime\RhubarbDate;
use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Between;
use Rhubarb\Stem\Filters\GreaterThan;
use Rhubarb\Stem\Filters\LessThan;
use Rhubarb\Stem\Models\Validation\EqualTo;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\UseCase;

class CreateCheckInForRoomUseCase extends UseCase
{
    public function execute(int $roomId)
    {
        $room = new Room($roomId);
        $bookingsArray = $room->Bookings;
        $bookingsArray->filter
        (
            new AndGroup
            (
                new LessThan("StartTime", new RhubarbDateTime("now"), true),
                new GreaterThan("EndTime", new RhubarbDateTime("now"), false)
            )
        );
        if ($bookingsArray->count() > 0) {
            if ($bookingsArray[0]->StartTime->diff($bookingsArray[0]->EndTime)->totalDays<1) {
                $bookingsArray[0]->checkIn();
            }
        }
    }
}