<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 10/01/18
 * Time: 19:34
 */

namespace SpaceCadets\Florp\Services\src\Models\Bookings;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\UseCase;

class CreateBookingFromRequestUseCase extends UseCase
{
    /**
     * @param BookingRequest $requestedBooking
     * @return Booking
     * @throws \Exception
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function execute(BookingRequest $requestedBooking): Booking
    {
        $booking = new Booking();
        $booking->importRawData($requestedBooking->exportData());
        // Try and change it based on duration
        if (
            $requestedBooking->Duration
            && $requestedBooking->StartTime->add(new \DateInterval('PT' . $requestedBooking->Duration . 'M'))
            < $requestedBooking->EndTime
        ) {
            $booking->EndTime = $requestedBooking->StartTime
                ->add(new\DateInterval('PT' . $requestedBooking->Duration . 'M'));
        }
        $booking->Locked = $requestedBooking->StartTime >= new RhubarbDateTime("-1 day") ? false : true;
        $booking->BookingRequestId = $requestedBooking->Id;
        $booking->save();
        $requestedBooking->BookingId = $booking->Id;
        $requestedBooking->save();
        return $booking;
    }
}