<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 18-Feb-18
 * Time: 21:18
 */

namespace SpaceCadets\Florp\Services\src\Models\Bookings;


use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\UseCase;

class CheckInToBookingUseCase extends UseCase
{

    public function execute(Booking $booking): Booking
    {
        $booking->CheckedIn = true;
        $booking->save();
        return $booking;
    }
}