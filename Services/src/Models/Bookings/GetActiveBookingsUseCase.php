<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 10/01/18
 * Time: 23:41
 */

namespace SpaceCadets\Florp\Services\src\Models\Bookings;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Collections\Collection;
use Rhubarb\Stem\Exceptions\FilterNotSupportedException;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\GreaterThan;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\UseCase;

class GetActiveBookingsUseCase extends UseCase
{
    /**
     * @return Collection|Booking
     * @throws FilterNotSupportedException
     */
    public function execute(): Collection
    {
        $currentDate = new RhubarbDateTime("now");
        $bookings = Booking::all()
            ->filter(
                new AndGroup(
                    [
                        new GreaterThan(
                            Booking::COLUMN_START_TIME,
                            $currentDate,
                            true
                        ),
                        new Equals(
                            Booking::COLUMN_CANCELLED,
                            false
                        )
                    ]
                )
            );

        return $bookings;
    }
}