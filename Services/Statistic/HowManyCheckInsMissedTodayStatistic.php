<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 21/03/18
 * Time: 19:45
 */

namespace SpaceCadets\Florp\Services\Statistic;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\GreaterThan;
use Rhubarb\Stem\Filters\LessThan;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetActiveBookingsBetweenDatesUseCase;

class HowManyCheckInsMissedTodayStatistic extends AbstractStatistic
{

    /**
     * @return mixed
     * Perform statistical calculation - returns result
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    protected function performStatisticFunction()
    {
        return Booking::all()
            ->filter(
                new AndGroup(
                    new GreaterThan("StartTime", new RhubarbDateTime("today")),
                    new LessThan("EndTime", new RhubarbDateTime("now")),
                    new Equals(Booking::COLUMN_CANCELLED, false),
                    new Equals(Booking::COLUMN_CHECKEDIN,false)
                ))->count();
    }

    /**
     * @return string
     * Return result as HTML
     */
    protected function getDefaultResultHTML(): string
    {
        return <<<HTML
        <p>{$this->getResult()}</p>
HTML;
    }
}