<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 13/03/18
 * Time: 18:12
 */

namespace SpaceCadets\Florp\Services\Statistic;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\GreaterThan;
use Rhubarb\Stem\Filters\LessThan;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetActiveBookingsBetweenDatesUseCase;

class RoomsCurrentlyAvailableStatistic extends AbstractStatistic
{
    /**
     * @return mixed
     * Perform statistical calculation - returns result
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    protected function performStatisticFunction()
    {
        $bookingNotCheckedIn = Booking::all()
            ->filter(
                new AndGroup(
                    new LessThan(BOOKING::COLUMN_START_TIME, new RhubarbDateTime("now"), true),
                    new GreaterThan(BOOKING::COLUMN_END_TIME, new RhubarbDateTime("now"), false),
                    new Equals(Booking::COLUMN_CANCELLED, false),
                    new Equals(Booking::COLUMN_CHECKEDIN,false)

                ))->toArray();
        $bookingsCheckedIn = Booking::all()
            ->filter(
                new AndGroup(
                    new LessThan(BOOKING::COLUMN_START_TIME, new RhubarbDateTime("now"), true),
                    new GreaterThan(BOOKING::COLUMN_END_TIME, new RhubarbDateTime("now"), false),
                    new Equals(Booking::COLUMN_CANCELLED, false),
                    new Equals(Booking::COLUMN_CHECKEDIN,true)

                ))->toArray();

        $rooms = Room::all();
        foreach ($rooms as $room) {
            /** @var Room $room
             *$room
             */
            $roomArray[] = $room;
        }
        foreach ($bookingNotCheckedIn as $booking) {
            /** @var Booking $booking
             *$booking
             */
            if(new RhubarbDateTime("now -5mins")<($booking->StartTime)){
                   $bookingArray[] = $booking->Room;
            }

        }
        if(empty($roomArray[0])){
            return["No Rooms Currently In the System"];
        }
        //    if(empty($bookingArray[0])) {
        foreach ($bookingsCheckedIn as $booking) {
            /** @var Booking $booking
             *$booking
             */
            $bookingArray[]= $booking->Room;
        }
        if(empty($bookingArray[0])){
            return $roomArray;
        }
        //   return ;
        //   }
        $array = array_merge(array_diff( $bookingArray,$roomArray), array_diff($roomArray ,$bookingArray));
        if (!empty($array[0])) {
            return $array;
        }
        return["No Rooms Currently Available"];
    }
    /**
     * @return string
     * Return result as HTML
     */
    protected function getDefaultResultHTML(): string
    {
        $html = <<<HTML
        <ul>    
HTML;
        foreach ($this->getResult() as $room)
        {
            $html .= <<<HTML
            <li>
            {$room}
            </li>
HTML;

        }
        return $html . <<<HTML
        </ul>
HTML;
    }
}