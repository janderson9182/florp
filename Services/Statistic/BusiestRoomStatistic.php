<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 24/03/18
 * Time: 15:29
 */

namespace SpaceCadets\Florp\Services\Statistic;


use Rhubarb\Crown\Html\ResourceLoader;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;

class BusiestRoomStatistic
{

    /**
     * @return mixed
     * Perform statistical calculation - returns result
     */
    protected function performStatisticFunction()
    {
        $bookings = Booking::all();
        $formattedString = '';
        $bookingArray=[];
        if(!empty($bookings[0]))
            {
            foreach ($bookings as $booking) {
                /** @var Booking $booking
                 *$booking
                 */
                $bookingArray[] = $booking->Room->Name;
            }
            $roomCounts = array_count_values($bookingArray);
            foreach ($roomCounts as $room => $count) {
                $formattedString .= "['" . $room . "'," . $count . "],";
            }
        }
        return $formattedString;
    }

    /**
     * @return string
     * Return result as HTML
     */
    public function __toString(): string
    {
        ResourceLoader::loadResource("https://www.gstatic.com/charts/loader.js");
        $js = <<<JS
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                        ['Room','Number of Bookings'],
                       {$this->performStatisticFunction()}
                    ]);
                    var options = {
                        title: 'Room Usage',
                    };
                    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
                    chart.draw(data, options);
                }
JS;
        ResourceLoader::addScriptCode($js);
        return '<div id="piechart" style="width: 800px; height: 400px;"></div>';
    }
}