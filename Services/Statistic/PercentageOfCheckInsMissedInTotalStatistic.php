<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 28/03/18
 * Time: 12:25
 */

namespace SpaceCadets\Florp\Services\Statistic;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\GreaterThan;
use Rhubarb\Stem\Filters\LessThan;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetActiveBookingsBetweenDatesUseCase;

class PercentageOfCheckInsMissedInTotalStatistic extends AbstractStatistic
{

    /**
     * @return mixed
     * Perform statistical calculation - returns result
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    protected function performStatisticFunction()
    {
        $bookingsMissed = Booking::all()
            ->filter(
                new AndGroup(
                    new LessThan("EndTime", new RhubarbDateTime("now")),
                    new Equals(Booking::COLUMN_CANCELLED, false),
                    new Equals(Booking::COLUMN_CHECKEDIN,false)))
            ->count();

        $bookingsAttended = Booking::all()
            ->filter(
                new AndGroup(
                    new LessThan("EndTime", new RhubarbDateTime("now")),
                    new Equals(Booking::COLUMN_CANCELLED, false),
                    new Equals(Booking::COLUMN_CHECKEDIN,true)))
            ->count();

        if ($bookingsAttended > 1) {
        if($bookingsAttended==$bookingsMissed) {
            return "50%";
        }
                return number_format(
                    (float)
                    ($bookingsMissed / $bookingsAttended) * 100, 0, '.', ''
                    ) . "%";
            }
            return "Not enough Bookings in the system";
    }

    /**
     * @return string
     * Return result as HTML
     */
    protected function getDefaultResultHTML(): string
    {
        return <<<HTML
        <p>{$this->getResult()}</p>
HTML;
    }
}