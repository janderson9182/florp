<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 12/03/18
 * Time: 20:14
 */

namespace SpaceCadets\Florp\Services\Statistic;

use Rhubarb\Stem\Filters\Equals;
use SpaceCadets\Florp\Models\Bookings\Booking;

class BookingCountAbstractStatistic extends AbstractStatistic
{
    /**
     * @return mixed
     * Perform statistical calculation - returns result
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    protected function performStatisticFunction()
    {
        return Booking::all()
            ->filter(new Equals(Booking::COLUMN_CANCELLED, false))
            ->count();
    }

    /**
     * @return string
     * Return result as HTML
     */
    protected function getDefaultResultHTML(): string
    {
        return <<<HTML
        <p>{$this->getResult()}</p>
HTML;
    }
}