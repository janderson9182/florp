<?php

/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 12/03/18
 * Time: 20:01
 */

namespace SpaceCadets\Florp\Services\Statistic;

abstract class AbstractStatistic
{
    protected $result;

    /**
     * Statistic constructor.
     * Initialises result variable with value of statistic
     */
    public function __construct()
    {
        $this->result = $this->performStatisticFunction();
    }

    /**
     * @return mixed
     * Optional Public Getter
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return string
     * Allows us to use the following:
     * print new ConcreteStatistic();
     */
    public function __toString()
    {
        return $this->getDefaultResultHTML();
    }

    /**
     * @return mixed
     * Perform statistical calculation - returns result
     */
    protected abstract function performStatisticFunction();

    /**
     * @return string
     * Return result as HTML
     */
    protected abstract function getDefaultResultHTML(): string;
}