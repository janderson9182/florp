<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 24/03/18
 * Time: 15:13
 */

namespace SpaceCadets\Florp\Services\Statistic;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Crown\Html\ResourceLoader;
use Rhubarb\Scaffolds\AuthenticationWithRoles\User;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\GreaterThan;
use Rhubarb\Stem\Filters\LessThan;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetActiveBookingsBetweenDatesUseCase;

class GraphOfBookingsStatistic extends AbstractStatistic
{
    /**
     * @return mixed
     * Perform statistical calculation - returns result
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    protected function performStatisticFunction()
    {
        return Booking::all()
            ->filter(
                new AndGroup(
                    new GreaterThan("StartTime", new RhubarbDateTime("today")),
                    new LessThan("EndTime", new RhubarbDateTime("tomorrow")),
                    new Equals(Booking::COLUMN_CANCELLED, false)
                ))->toArray();
    }

    /**
     * @return string
     * Return result as HTML
     */
    protected function getDefaultResultHTML(): string
    {
        $rooms = Room::all();
        foreach ($rooms as $room) {
            /** @var Room $room
             *$room
             */
            $roomArray[] = $room->Name;
        }
        if(!empty($this->getResult())){
            foreach ($this->getResult() as $booking) {
                /** @var Booking $booking
                 * $booking
                 */
                $roomsWithBookingArray[] = $booking->Room->Name;
            }
        }else{
            $roomsWithBookingArray=[];
        }
        $roomsWithNoBookings = array_diff($roomArray, $roomsWithBookingArray);

        foreach ($roomsWithNoBookings as $room) {
            $roomArrayFormatted[] = "['" . $room . "'";
            array_push($roomArrayFormatted, " new Date(00,00,00,12,00,00) , new Date(00,00,00,12,00,00)]");
        }
        if(isset($roomArrayFormatted)){
            $roomArrayString = implode(",", $roomArrayFormatted);
        }
        else{
            $roomArrayString = "['All Rooms will be in use today', new Date(00,00,00,12,00,00) , new Date(00,00,00,12,00,00)]";
        }


        foreach ($this->getResult() as $booking) {
            /** @var Booking $booking
             *$booking
             */
            $bookingArray[] = "['" . $booking->Room . "'";
            array_push($bookingArray, " new Date(00,00,00," . $booking->StartTime->format('H,i,s') . ")");
            array_push($bookingArray, " new Date(00,00,00," . $booking->EndTime->format('H,i,s') . ")]");
        }
        $booking = "";
        if (!empty($bookingArray[0])) {
            $booking = implode(",", $bookingArray);
        }

        ResourceLoader::loadResource("https://www.gstatic.com/charts/loader.js");
        $js = <<<JS
        google.charts.load('current', {'packages':['timeline']});
        google.charts.setOnLoadCallback(drawChart);
        var options = {
            hAxis: {
            minValue: new Date(00, 00, 00,00),
            maxValue: new Date(00, 00, 00,24)
            }
        };
        function drawChart() {
            var container = document.getElementById('timeline');
            var chart = new google.visualization.Timeline(container);
            var dataTable = new google.visualization.DataTable();

            dataTable.addColumn({ type: 'string', id: 'President' });
            dataTable.addColumn({ type: 'date', id: 'Start' });
            dataTable.addColumn({ type: 'date', id: 'End' });
            dataTable.addRows([
            {$roomArrayString},
            {$booking}
                ])
            chart.draw(dataTable,options);
            
            //code taken from online :https://stackoverflow.com/questions/29501220/google-charts-api-add-blank-row-to-timeline?rq=1
            (function(){                                            //anonymous self calling function to prevent variable name conficts
              var el=container.getElementsByTagName("rect");      //get all the descendant rect element inside the container      
    var width=100000000;                                //set a large initial value to width
    var elToRem=[];                                     //element would be added to this array for removal
    for(var i=0;i<el.length;i++){                           //looping over all the rect element of container
        var cwidth=parseInt(el[i].getAttribute("width"));//getting the width of ith element
        if(cwidth<width){                               //if current element width is less than previous width then this is min. width and ith element should be removed
            elToRem=[el[i]];
            width=cwidth;                               //setting the width with min width
        }
        else if(cwidth==width){                         //if current element width is equal to previous width then more that one element would be removed
            elToRem.push(el[i]);        
        }
    }
    for(var i=0;i<elToRem.length;i++) // now iterate JUST the elements to remove
        elToRem[i].setAttribute("fill","none"); //make invisible all the rect element which has minimum width
})();   
        }
JS;
        ResourceLoader::addScriptCode($js);
        return '<div id="timeline" style="height: 400px;"></div>';
    }
}