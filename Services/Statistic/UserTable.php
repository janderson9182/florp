<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 28/03/18
 * Time: 23:11
 */

namespace SpaceCadets\Florp\Services\Statistic;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Crown\Html\ResourceLoader;
use Rhubarb\Scaffolds\AuthenticationWithRoles\User;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\GreaterThan;
use Rhubarb\Stem\Filters\LessThan;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;

class UserTable
{

    /**
     * @return mixed
     * Perform statistical calculation - returns result
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    protected function performStatisticFunction()
    {
        try {
            $bookings = Booking::all()
                ->filter(
                    new AndGroup(
                        new LessThan(BOOKING::COLUMN_START_TIME, new RhubarbDateTime("now"), true),
                        new GreaterThan(BOOKING::COLUMN_END_TIME, new RhubarbDateTime("now"), false),
                        new Equals(Booking::COLUMN_CHECKEDIN, true)
                    ));
        } catch (FilterNotSupportedException $e) {
        }
        $users = User::all();
        foreach ($users as $user) {
            /** @var User $user
             *$user
             */
            $userArray[] = $user->getFullName();
        }
        foreach ($bookings as $booking) {
            /** @var Booking $booking
             *$booking
             */
            $bookingArray[] = "['". $booking->Room . "'";
            array_push($bookingArray,"'".$userArray[$booking->UserId-1] . "']");
        }

        if(empty($bookingArray[0])||empty($userArray[0])) {
            return "no users in any rooms";
        }

        $returnArray =implode(",", $bookingArray);
        return $returnArray;
    }

    /**
     * @return string
     * Return result as HTML
     */
    public function __toString(): string
    {
        ResourceLoader::loadResource("https://www.gstatic.com/charts/loader.js");
        $js = <<<JS
        google.charts.load('current', {'packages':['table']});
        google.charts.setOnLoadCallback(drawTable);
      function drawTable() {
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Room Name');
          data.addColumn('string', 'User Name');
          data.addRows([
        {$this->performStatisticFunction()}
        ])
        var table = new google.visualization.Table(document.getElementById('table_div'));

        table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
      }
JS;
        ResourceLoader::addScriptCode($js);
        return '<div id="table_div" style="height: 180px;"></div>';
    }
}