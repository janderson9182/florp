<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 15/03/18
 * Time: 16:28
 */

namespace SpaceCadets\Florp\Services\Statistic;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\Response\BinaryResponse;
use Rhubarb\Stem\Exceptions\FilterNotSupportedException;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\GreaterThan;
use Rhubarb\Stem\Filters\LessThan;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetActiveBookingsBetweenDatesUseCase;
use SpaceCadets\Florp\Services\UseCase;

class DownloadLockedBookingsForToday extends UseCase
{
    /**
     * @return FileEntity
     * @throws ForceResponseException
     * @throws FilterNotSupportedException
     */
    public function execute(): FileEntity
    {
        $fileEntity = new FileEntity();

        $today = new RhubarbDateTime("today");

        $fileEntity->fileName =
            "Daily Schedule for"
            . $today->format("Y-m-d")
            . ".csv";

        $schedulesDirectory = APPLICATION_ROOT_DIR . "/data/schedules";
        if (!is_dir($schedulesDirectory)) {
            mkdir($schedulesDirectory, 0777, true);
        }

        $fileEntity->filePath = $schedulesDirectory . "/" . $fileEntity->fileName;
        file_put_contents($fileEntity->filePath, "");
        $file = fopen($fileEntity->filePath, "r+");
        fputcsv($file, ["Booking", "Room", "Date", "Start Time","End Time", "Number Of Bookings for Booking", "Checked In","User Name"]);

        /*logic for bookings*/

        $bookings = GetActiveBookingsBetweenDatesUseCase::create()
            ->execute(new RhubarbDateTime("today"), new RhubarbDateTime("tomorrow"))
            ->toArray();

        foreach ($bookings as $booking) {
            /** @var Booking $booking
             *$booking
             */
            fputcsv(
                $file,
                [
                    $booking->Id,
                    $booking->Room,
                    $booking->StartTime->format("d-m-Y"),
                    $booking->StartTime->format("H:i:s"),
                    $booking->EndTime->format("H:i:s"),
                    $booking->NumberOfSeats,
                    $booking->CheckedIn,
                    $booking->BookingRequest->User->getFullName()
                ]);
        }

        throw new ForceResponseException(
            new BinaryResponse(
                $this,
                file_get_contents($fileEntity->filePath),
                "application/csv",
                $fileEntity->fileName
        ));
    }
}