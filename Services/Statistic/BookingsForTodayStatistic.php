<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 12/03/18
 * Time: 22:09
 */

namespace SpaceCadets\Florp\Services\Statistic;


use DeepCopy\f001\A;
use Rhubarb\Crown\DateTime\RhubarbDate;
use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\GreaterThan;
use Rhubarb\Stem\Filters\LessThan;
use Rhubarb\Stem\Models\Validation\EqualTo;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetActiveBookingsBetweenDatesUseCase;

class BookingsForTodayStatistic extends AbstractStatistic
{
    /**
     * @return mixed
     * Perform statistical calculation - returns result
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    protected function performStatisticFunction()
    {
        return GetActiveBookingsBetweenDatesUseCase::create()
            ->execute(new RhubarbDateTime("today"), new RhubarbDateTime("tomorrow"))
            ->count();
    }

    /**
     * @return string
     * Return result as HTML
     */
    protected function getDefaultResultHTML(): string
    {
        return <<<HTML
        <p>{$this->getResult()}</p>
HTML;

    }
}