<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 13/03/18
 * Time: 18:11
 */

namespace SpaceCadets\Florp\Services\Statistic;


use Rhubarb\Crown\DateTime\RhubarbDate;
use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Exceptions\FilterNotSupportedException;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\GreaterThan;
use Rhubarb\Stem\Filters\LessThan;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetActiveBookingsBetweenDatesUseCase;

class LastBookingTodayStatistic extends AbstractStatistic
{
    protected function performStatisticFunction()
    {

        try {
            $sortedBookings  = GetActiveBookingsBetweenDatesUseCase::create()
                ->execute(new RhubarbDateTime("today"), new RhubarbDateTime("tomorrow"))
                ->addSort(
                    "EndTime",false
            );
        } catch (FilterNotSupportedException $e) {
        }

        $myArray = [];

        foreach ($sortedBookings as $booking) {
            /** @var Booking $booking
             *$booking
             */
            $myArray[] = $booking->EndTime->format('H:i:s');
        }
        if(!empty($myArray[0])){
            return $myArray[0];
        }
            return "No Bookings Today";
    }

    /**
     * @return string
     * Return result as HTML
     */
    protected function getDefaultResultHTML(): string
    {
        return <<<HTML
        <p>{$this->getResult()}</p>
HTML;
    }
}