<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 14/03/18
 * Time: 17:56
 */

namespace SpaceCadets\Florp\Services\Statistic;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Filters\AndGroup;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Filters\GreaterThan;
use Rhubarb\Stem\Filters\LessThan;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetActiveBookingsBetweenDatesUseCase;

class RoomsCurrentlyBookedStatistic extends AbstractStatistic
{

    /**
     * @return mixed
     * Perform statistical calculation - returns result
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    protected function performStatisticFunction()
    {
        $bookings = Booking::all()
            ->filter(
                new AndGroup(
                    new LessThan(BOOKING::COLUMN_START_TIME, new RhubarbDateTime("now"), true),
                    new GreaterThan(BOOKING::COLUMN_END_TIME, new RhubarbDateTime("now"), false),
                    new Equals(Booking::COLUMN_CANCELLED, false),
                    new Equals(Booking::COLUMN_CHECKEDIN,true)
                ))->toArray();

        foreach ($bookings as $booking) {
            /** @var Booking $booking
             *$booking
             */
            $myArray[] = $booking->Room;
        }
        if (!empty($myArray[0])){
            return $myArray;        }
        return ["No Rooms Currently Checked In"];
    }
    /**
     * @return string
     * Return result as HTML
     */
    protected function getDefaultResultHTML(): string
    {
        $html = <<<HTML
        <ul>
HTML;
        foreach ($this->getResult() as $room)
        {
            $html .= <<<HTML
            <li>
            {$room}
            </li>
HTML;

        }
        return $html . <<<HTML
        </ul>
HTML;
    }
}