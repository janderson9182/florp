<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 30/03/2018
 * Time: 21:23
 */

namespace SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceSmallestFirst;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Scaffolds\BackgroundTasks\Models\BackgroundTaskStatus;
use Rhubarb\Stem\Collections\RepositoryCollection;
use Rhubarb\Stem\Exceptions\ModelConsistencyValidationException;
use Rhubarb\Stem\Exceptions\ModelException;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingService;
use SpaceCadets\Florp\Services\src\Models\Assets\Rooms\GetRoomsToFitNumberSortedUseCase;
use SpaceCadets\Florp\Services\src\Models\Assets\Rooms\IsRoomAvailableBetweenDatesUseCase;
use SpaceCadets\Florp\Services\src\Models\Bookings\CreateBookingFromRequestUseCase;

abstract class AbstractSmallestFirstProvider extends RoomBookingService
{
    /**
     * @param BackgroundTaskStatus $status
     * @return bool
     * @throws ModelConsistencyValidationException
     * @throws ModelException
     * @throws \Exception
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    public function execute(BackgroundTaskStatus $status)
    {
        // Get Booking Request
        $bookingRequest = new BookingRequest($status->TaskSettings[self::SETTING_BOOKING_REQUEST_ID]);
        $rooms = GetRoomsToFitNumberSortedUseCase::create()->execute($bookingRequest->NumberOfSeats);
        $this->updateStatusMessage($status, "Trying to fit into smallest room");
        if (!$this->tryToFitIntoAlreadyAvailableSpace($status, $rooms, $bookingRequest)) {
            $this->updateStatusMessage($status, "Moving things around");
            $shuffleWorked = $this->moveThingsAround($bookingRequest);
            $this->updateStatusMessage(
                $status,
                $shuffleWorked ?
                    "Booking Created. Please Reload The Page" :
                    "No Booking Created"
            );
            if($shuffleWorked === false){
                $bookingRequest->Failed = true;
                $bookingRequest->save();
            }
            return $shuffleWorked;
        }
    }

    /**
     * @param RhubarbDateTime $startTime
     * @param RhubarbDateTime $endTime
     * @param Room $room
     * @return bool
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    private function roomIsAvailable(RhubarbDateTime $startTime, RhubarbDateTime $endTime, Room $room): bool
    {
        return IsRoomAvailableBetweenDatesUseCase::create()->execute($room, $startTime, $endTime);
    }

    /**
     * @param BackgroundTaskStatus $status
     * @param string $message
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    private function updateStatusMessage(BackgroundTaskStatus $status, string $message): void
    {
        $status->Message = $message;
        $status->save();
    }

    /**
     * @param BackgroundTaskStatus $status
     * @param $rooms
     * @param BookingRequest $bookingRequest
     * @return bool
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Exception
     */
    private function tryToFitIntoAlreadyAvailableSpace(
        BackgroundTaskStatus $status,
        RepositoryCollection $rooms,
        $bookingRequest
    ): bool {
        $index = 1;
        /** @var Room $room */
        foreach ($rooms as $room) {
            $status->PercentageComplete = $index / $rooms->count() * 100;
            $status->save();
            $index++;
            // If Room Is Available between the start and end times
            // Create a Booking
            // Register as a success
            // Else, try another room -> return false for now
            if (
                $this->roomIsAvailable($bookingRequest->StartTime, $bookingRequest->EndTime, $room)
                && $room->hasProperties($bookingRequest->Properties)
            ) {
                $booking = CreateBookingFromRequestUseCase::create()->execute($bookingRequest);
                $booking->RoomId = $room->Id;
                $booking->save();
                $this->updateStatusMessage($status, "Booking Created");
                $status->PercentageComplete = 100;
                $status->save();
                return true;
            }
        }
        return false;
    }

    protected abstract function moveThingsAround(BookingRequest $bookingRequestFromClient): bool;
}