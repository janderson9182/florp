<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 29/03/2018
 * Time: 10:22
 */

namespace SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceSmallestFirst;

use Rhubarb\Stem\Collections\RepositoryCollection;
use Rhubarb\Stem\Filters\Equals;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\RoomBookingServices\Entities\BookingEntity;
use SpaceCadets\Florp\Services\RoomBookingServices\Entities\BookingRequestEntity;
use SpaceCadets\Florp\Services\RoomBookingServices\Entities\RoomEntity;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\src\Models\Assets\Rooms\IsRoomAvailableBetweenDatesUseCase;
use SpaceCadets\Florp\Services\src\Models\Bookings\CreateBookingFromRequestUseCase;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetActiveBookingsBetweenDatesUseCase;
use SpaceCadets\Florp\Services\UseCase;

class SmallestFirstShuffleUseCase extends UseCase
{
    /** @var RepositoryCollection|Booking[] $allBookingsBetweenDates*/
    private $allBookingsBetweenDates;
    private $bookingEntities;
    private $roomEntities;
    private $bookingRequestEntities;
    private $bookingRequestFromClient;

    /**
     * @param BookingRequest $bookingRequestFromClient
     * @return bool
     * @throws \Exception
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function execute(BookingRequest $bookingRequestFromClient): bool
    {
        return $this->initialise($bookingRequestFromClient)->performShuffle();
    }

    /**
     * @param BookingRequest $bookingRequestFromClient
     * @return SmallestFirstShuffleUseCase
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    public function initialise(BookingRequest $bookingRequestFromClient): self
    {
        $this->bookingRequestFromClient = $bookingRequestFromClient;
        $this->allBookingsBetweenDates = GetActiveBookingsBetweenDatesUseCase::create()
            ->execute($bookingRequestFromClient->StartTime, $bookingRequestFromClient->EndTime)
            ->filter(new Equals(Booking::COLUMN_LOCKED, false));

        $this->bookingEntities = [];
        $this->roomEntities = [];
        foreach ($this->allBookingsBetweenDates as $booking) {
            $bookingEntity = new BookingEntity();
            $bookingEntity->import($booking->exportData());
            // unset the room so we can try and set it
            $bookingEntity->RoomId = null;
            $bookingEntity->Room = null;
            $this->bookingEntities[$bookingEntity->Id] = $bookingEntity;

            $roomEntity = new RoomEntity();
            $roomEntity->import($booking->Room->exportData());
            $roomEntity->free = true;
            $this->roomEntities[$roomEntity->Id] = $roomEntity;
        }

        // Get rooms that are just empty between those times
        foreach (Room::all() as $room) {
            if (IsRoomAvailableBetweenDatesUseCase::create()->execute($room, $this->bookingRequestFromClient->StartTime,
                $this->bookingRequestFromClient->StartTime)) {
                $roomEntity = new RoomEntity();
                $roomEntity->import($room->exportData());
                $this->roomEntities[$room->Id] = $roomEntity;
            }
        }

        $this->sortByNumberOfSeats($this->bookingEntities);
        $this->sortByNumberOfSeats($this->roomEntities);


        $bookingRequestsBetweenDates = [];
        $bookingRequestsBetweenDates[] = $bookingRequestFromClient;
        foreach ($this->allBookingsBetweenDates as $booking) {
            $bookingRequestsBetweenDates[] = $booking->BookingRequest;
        }

        $this->sortByNumberOfSeats($bookingRequestsBetweenDates);
        $this->bookingRequestEntities = [];

        foreach ($bookingRequestsBetweenDates as $bookingRequest) {
            $bookingRequestEntity = new BookingRequestEntity();
            $bookingRequestEntity->import($bookingRequest->exportData());
            if ($bookingRequest->Id == $bookingRequestFromClient->Id) {
                $bookingRequestEntity->isRequestedBooking = true;
            }
            $this->bookingRequestEntities[$bookingRequest->Id] = $bookingRequestEntity;
        }
        return $this;
    }

    /**
     * @return bool
     * @throws \Exception
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function performShuffle(): bool
    {
        // At this point, we have an array of Booking Request, Booking and Room Entities
        // It is vital that all the BookingRequestEntities and RoomEntities arrays are sorted by NumberOfSeats
        /** @var BookingRequestEntity $bookingRequestEntity */
        foreach ($this->bookingRequestEntities as $bookingRequestEntity) {
            // This booking request has not found a room yet
            $foundRoom = false;
            /** @var RoomEntity $roomEntity */
            foreach ($this->roomEntities as $roomEntity) {
                // This booking request entity now has a room, move onto the next one
                if ($foundRoom) {
                    continue;
                }
                // If there is space and there is no booking yet, set the room id for the booking request booking then continue
                if (
                    $roomEntity->NumberOfSeats >= $bookingRequestEntity->NumberOfSeats
                    && $roomEntity->free
                ) {
                    $roomEntity->free = false;
                    $foundRoom = true;
                    if (isset($this->bookingEntities[$bookingRequestEntity->BookingId])) {
                        $this->bookingEntities[$bookingRequestEntity->BookingId]->RoomId = $roomEntity->Id;
                    } else {
                        $bookingEntity = CreateBookingEntityFromBookingRequestEntityUseCase::create()->execute($bookingRequestEntity);
                        $bookingEntity->RoomId = $roomEntity->Id;
                        // Will always have an Id of -1, this can't be null, as it will be potentially changed to later
                        // As we still need array access
                        $this->bookingEntities[$bookingEntity->Id] = $bookingEntity;
                    }
                }
            }
            if (!$foundRoom) {
                // A booking request didn't find a room, abort
                return false;
            }
        }

        /*
         * Save all of the bookings with their new rooms, including our new one
         */

        /** @var  BookingEntity $bookingEntity */
        foreach ($this->bookingEntities as $bookingEntity) {
            // Ensure that our new booking gets an ID
            if ($bookingEntity->Id === -1) {
                $booking = CreateBookingFromRequestUseCase::create()->execute($this->bookingRequestFromClient);
            } else {
                $booking = new Booking($bookingEntity->Id);
            }
            $booking->RoomId = $bookingEntity->RoomId;
            $booking->save();
        }
        return true;
    }

    /**
     * @param array $array to ne sorted by its NumberOfSeats property value. Passed By reference rather than value
     * usort removes the keys being the IDs so we need to re set them in place, keeping the new order
     * @return void nothing as this happens to the array in place
     */
    private function sortByNumberOfSeats(array &$array)
    {
        usort(
            $array,
            function ($leftComparator, $rightComparator) {
                return $leftComparator->NumberOfSeats <=> $rightComparator->NumberOfSeats;
            }
        );

        $reKeyed = [];
        foreach ($array as $item){
            $reKeyed[$item->Id] = $item;
        }
        $array = $reKeyed;
    }
}