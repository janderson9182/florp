<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 29/03/2018
 * Time: 11:15
 */

namespace SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceSmallestFirst;

use SpaceCadets\Florp\Services\RoomBookingServices\Entities\BookingEntity;
use SpaceCadets\Florp\Services\RoomBookingServices\Entities\BookingRequestEntity;
use SpaceCadets\Florp\Services\UseCase;

class CreateBookingEntityFromBookingRequestEntityUseCase extends UseCase
{
    /**
     * @param BookingRequestEntity $bookingRequestEntity
     * @return BookingEntity
     * @throws \Exception
     */
    public function execute(BookingRequestEntity $bookingRequestEntity): BookingEntity
    {
        $bookingEntity = new BookingEntity();
        $bookingEntity->import($bookingRequestEntity->toArray());
        // Only a Model can set Id
        $bookingEntity->Id = -1;
        if (
            $bookingRequestEntity->Duration
            && $bookingRequestEntity->StartTime->add(new \DateInterval('PT' . $bookingRequestEntity->Duration . 'M'))
            < $bookingRequestEntity->EndTime
        ) {
            $bookingEntity->EndTime = $bookingRequestEntity->StartTime
                ->add(new\DateInterval('PT' . $bookingRequestEntity->Duration . 'M'));
        }
        $bookingEntity->BookingRequestId = $bookingRequestEntity->Id;
        return $bookingEntity;
    }
}