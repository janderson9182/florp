<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 14/02/2018
 */

namespace SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceSmallestFirst;

use SpaceCadets\Florp\Models\Bookings\BookingRequest;

class RoomBookingServiceSmallestFirstProvider extends AbstractSmallestFirstProvider
{
    public static function getName(): string
    {
        return "Smallest Room First";
    }

    public static function getDescription(): string
    {
        return "When a user makes a booking request, the system will attempt to create a booking in the smallest"
            . " possible room that can accommodates their needs entirely";
    }

    /**
     * @param BookingRequest $bookingRequestFromClient
     * @return bool
     * @throws \Exception
     */
    protected function moveThingsAround(BookingRequest $bookingRequestFromClient): bool
    {
        return SmallestFirstShuffleUseCase::create()->execute($bookingRequestFromClient);
    }
}