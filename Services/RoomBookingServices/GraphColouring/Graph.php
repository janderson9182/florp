<?php

namespace SpaceCadets\Florp\Services\RoomBookingServices\GraphColouring;

use SplDoublyLinkedList;

/**
 *RON - references used
 *@see https://github.com/tiger-seo/algorithms/blob/master/src/Algorithm/Graph/Graph.php
 *@see https://www.geeksforgeeks.org/graph-and-its-representations/
 */

class Graph
{

    protected $vertices;
    public $adjacencyList;

    //Constructs a new graph object
    //Graph object is a list of adjacency lists
    //Each index in the array contains a linked list of the nodes connected to the index node
    public function __construct($Vertices){
        $this->vertices = $Vertices;
        $this->adjacencyList = new SplDoublyLinkedList();
        for($x = 0; $x < $this->vertices; ++$x){
            $this->adjacencyList->push(new SplDoublyLinkedList());
        }

    }

    //Function to add an edge to the graph
    //Takes two vertices and adds each to the other's linked list
    public function addEdge($vertexOne, $vertexTwo){
        $this->adjacencyList[$vertexOne]->push($vertexTwo);
        $this->adjacencyList[$vertexTwo]->push($vertexOne);
    }

    //Function to print the adjacency list representation of the graph
    public function printGraph(){
        for($currentVertexCounter = 0; $currentVertexCounter < $this->vertices; $currentVertexCounter++){
            echo ("Adjacency list for vertex: " . $currentVertexCounter);
            echo ("Head");
            foreach ($this->adjacencyList[$currentVertexCounter] as $value) {
                echo(" -> " . $value);
            }
            echo ("\n");
        }
    }

    public function colourGraph(){
        $results = [$this->vertices];

        //Assign every value as -1 to begin
        $results = array_fill_keys(array_keys($results), -1);

        //Assign first colour to first vertex
        $results[0] = 0;

        //Temporarily store 'available' colours
        //If available[r] = False, r is assigned to one of the vertex's neighbours
        $available = array($this->vertices);

        //At first all colours are available
        $available = array_fill_keys(array_keys($available), true);

        //Assign colours to rest of vertices, i.e. starting at index 1
        for($z = 1; $z < $this->vertices; $z++){

            //Take colours of neighbours and mark as unavailable
            //In the below for loop is code for correctly traversing a linked list
            for($this->adjacencyList->rewind(); $this->adjacencyList->valid(); $this->adjacencyList->next()){
                $i = $this->adjacencyList->current();
                if($results[$i] != -1){
                    $available[$results[$i]] = false;
                }
            }

            //Find first available colour
            for ($availableColour = 0; $availableColour < $this->vertices; $availableColour++){
                if($available[$availableColour]){
                    break;
                }
            }

            //Assign this colour
            $results[$z] = $availableColour;

            //Reset all available values to true for next iteration
            $available = array_fill_keys(array_keys($available), true);
        }
    }
}