<?php
/**
 * Created by PhpStorm.
 * User: ron_0
 * Date: 26/03/2018
 * Time: 15:16
 */
namespace SpaceCadets\Florp\Services\RoomBookingServices\GraphColouring;

use Rhubarb\Scaffolds\BackgroundTasks\Models\BackgroundTaskStatus;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingService;
use SpaceCadets\Florp\Services\src\Models\Assets\Rooms\GetRoomsUseCase;

class RoomBookingServiceGraphColouringProvider extends RoomBookingService
{

    public static function getName(): string
    {
        return "Graph Colouring Sort";
    }

    public static function getDescription(): string
    {
        return "When a user creates a booking request, the system will add the booking request to a graph"
            . "data structure as a vertex. The system then attempts to colour the graph with a set of 'colours'."
            . "These 'colours' are the rooms available. An edge in the graph will be created between two"
            . "booking request vertices if the two booking requests overlap (same time, date and room).";
    }

    /**
     * Executes the long running code.
     *
     * @param BackgroundTaskStatus $status
     * @return void
     */
    public function execute(BackgroundTaskStatus $status)
    {
        //Get booking
        $bookingRequest = new BookingRequest($status->TaskSettings[self::SETTING_BOOKING_REQUEST_ID]);

        //Get rooms
        $rooms = Room::all();

        //Add the booking request to the graph object and update the status message
    }
}