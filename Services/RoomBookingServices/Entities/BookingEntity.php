<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 27/03/18
 * Time: 18:24
 */

namespace SpaceCadets\Florp\Services\RoomBookingServices\Entities;


class BookingEntity extends Entity
{
    public $Id;
    public $UserId;
    public $RoomId;
    public $BookingRequestId;
    public $StartTime;
    public $EndTime;
    public $NumberOfSeats;
    public $Cancelled;
    public $WillingToShare;
    public $Locked;
    public $Room;
    public $BookingRequest;
    public $CheckedIn;
    public $Shuffled = false;

    public function import(array $data)
    {
        foreach ($data as $key => $value) {
            if (property_exists(self::class, $key)) {
                $this->$key = $value;
            }
        }
    }
}