<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 28/03/2018
 * Time: 17:44
 */

namespace SpaceCadets\Florp\Services\RoomBookingServices\Entities;

use Rhubarb\Stem\Collections\RepositoryCollection;
use SpaceCadets\Florp\Models\Assets\Floor;
use SpaceCadets\Florp\Models\Assets\Properties\Property;
use SpaceCadets\Florp\Models\Assets\Properties\RoomImage;
use SpaceCadets\Florp\Models\Assets\Properties\RoomProperty;
use SpaceCadets\Florp\Models\Bookings\Booking;

class RoomEntity extends Entity
{
    /** @var int $Id */
    public $Id;
    /** @var  string $Name */
    public $Name;
    /** @var  string $Description */
    public $Description;
    /** @var  int $FloorId */
    public $FloorId;
    /** @var  int $NumberOfSeats */
    public $NumberOfSeats;
    /** @var  bool $CanBookIndividualSeats */
    public $CanBookIndividualSeats;
    /** @var  Floor $Floor */
    public $Floor;
    /** @var  Booking[]|RepositoryCollection $Bookings */
    public $Bookings;
    /** @var  RoomImage[]|RepositoryCollection $Images */
    public $Images;
    /** @var  RoomProperty[]|RepositoryCollection */
    public $PropertiesRaw;
    /** @var  Property[]|RepositoryCollection $Properties */
    public $Properties;
    /** @var bool $free */
    public $free = true;
}