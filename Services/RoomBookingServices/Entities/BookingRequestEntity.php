<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 28/03/2018
 * Time: 16:28
 */

namespace SpaceCadets\Florp\Services\RoomBookingServices\Entities;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Assets\Room;

class BookingRequestEntity extends Entity
{
    /** * @var int $Id */
    public $Id;
    /** * @var int $UserId */
    public $UserId;
    /** * @var int $RoomId */
    public $RoomId;
    /** * @var int $BookingRequestId */
    public $BookingId;
    /** * @var RhubarbDateTime $StartTime */
    public $StartTime;
    /** * @var RhubarbDateTime $EndTime */
    public $EndTime;
    /** * @var int $NumberOfSeats */
    public $NumberOfSeats;
    /** * @var bool $Cancelled */
    public $Cancelled;
    /** * @var bool $WillingToShare */
    public $WillingToShare;
    /** * @var bool $Locked */
    public $Locked;
    /** * @var Room $Room */
    public $Room;
    /** * @var $Booking BookingEntity */
    public $Booking;
    /** * @var bool $CheckedIn */
    public $CheckedIn;
    /** @var int $Duration */
    public $Duration;
    /** @var bool$isRequestedBooking */
    public $isRequestedBooking = false;
}