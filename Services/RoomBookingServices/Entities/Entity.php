<?php
/**
 * Created by Team Space Cadets
 * User: james & matthew
 * Date: 28/03/2018
 * Time: 16:21
 */

namespace SpaceCadets\Florp\Services\RoomBookingServices\Entities;

abstract class Entity
{
    /**
     * @param array $data
     * @author james & matthew
     * Populate all properties of the entity using a key value pair array
     * Also populates the data array
     */
    public function import(array $data)
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
            $this->data[$key] = $value;
        }
    }

    /**
     * @param $name
     * @param $value
     * @author james
     * Ensure that all values are added to our array
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
        $this->data[$name] = $value;
    }

    /**
     * @param $name
     * @return mixed
     * @author james
     * Ensure that we return the value from our array
     */
    public function __get($name)
    {
        return $this->data[$name];
    }

    /**
     * @return array all of the properties of the class
     *@author james
     */
    public function toArray()
    {
        return $this->data;
    }

    /**
     * @var array all of the properties of the class
     * @author james
     */
    protected $data = [];
}