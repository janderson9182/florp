<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 24/03/18
 * Time: 15:04
 */

namespace SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceRecursive;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\RoomBookingServices\Entities\BookingEntity;
use SpaceCadets\Florp\Services\src\Models\Assets\Rooms\GetRoomsToFitNumberSortedUseCase;
use SpaceCadets\Florp\Services\UseCase;

class RoomBookingServiceRecursiveSort extends UseCase
{


    /**
     * @param $passedInBookingRequest
     * @return bool
     * @throws \Exception
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function execute($passedInBookingRequest)
    {
        $bookingRequest = $passedInBookingRequest;
        $bookings = Booking::all();
        $bookingsClone = array();

        //sets up a cloned database of Bookings
        foreach ($bookings as $booking) {

            $bookingEnt = new BookingEntity();
            $bookingEnt->import($booking->exportData());
            $bookingsClone[] = $bookingEnt;

        }

        //shuffles the cloned database using recursion and saves it as a new array
        //recursive shuffle sets $shuffledBookings to false if no solution could be found
        $shuffledBookings = $this->recursiveShuffle($bookingRequest, $bookingsClone);
        //resolves the new/updated Bookings if $shuffledBookings is not set as false
        if ($shuffledBookings != false) {
            foreach ($shuffledBookings as $newBooking) {
                if ($newBooking->Shuffled == true) {
                    //resolving any completely new bookings
                    if ($newBooking->Id == null) {
                        $realBooking = new Booking();
                        $realBooking->StartTime = $newBooking->StartTime;
                        $realBooking->EndTime = $newBooking->EndTime;
                        $realBooking->RoomId = $newBooking->RoomId;
                        $realBooking->BookingRequestId = $newBooking->BookingRequestId;
                        $realBooking->UserId = $newBooking->UserId;
                        $realBooking->Locked = $bookingRequest->StartTime >= new RhubarbDateTime("-1 day") ? false : true;
                        $realBooking->BookingRequestId = $bookingRequest->Id;
                        $realBooking->NumberOfSeats = $newBooking->NumberOfSeats;
                        $realBooking->save();
                        $bookingRequest->BookingId = $realBooking->Id;
                        $bookingRequest->save();
                    }

                    //resolving any bookings that just have their rooms updated
                    if ($newBooking->Id != null) {
                        foreach ($bookings as $bookingToBeUpdated) {
                            if ($bookingToBeUpdated->Id == $newBooking->Id) {
                                $bookingToBeUpdated->RoomId = $newBooking->RoomId;
                                $bookingToBeUpdated->save();
                            }
                        }
                    }
                }
            }
            //returns true if the shuffle was successful
            return true;
        }
        //returns false if the shuffle was not successful
        return false;
    }


    /**
     * @param $bookingRequest
     * @param $bookingsArrayClone
     * @return bool
     * @throws \Exception
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function recursiveShuffle($bookingRequest, $bookingsArrayClone)
    {
        //gets all of the rooms who fit the requests description (minus properties)
        $rooms = GetRoomsToFitNumberSortedUseCase::create()->execute($bookingRequest->NumberOfSeats);

        //attempts to make a booking
        foreach ($rooms as $room) {

            //checks if rooms are free at the time of the request by looking at the cloned array
            if (
                $this->clonedRoomIsAvailable($bookingRequest->StartTime, $bookingRequest->EndTime, $room, $bookingsArrayClone)
                && $room->hasProperties($bookingRequest->Properties)
            ) {
                //creates a new booking entity and saves it to the cloned array
                $alreadyUpdated = false;
                foreach ($bookingsArrayClone as $updatingBooking){
                    if ($updatingBooking->BookingRequestId == $bookingRequest->Id){
                        $updatingBooking->RoomId = $room->Id;
                        $updatingBooking->Shuffled = true;
                        $alreadyUpdated = true;
                    }
                }

                if(!$alreadyUpdated) {
                    $newBooking = new BookingEntity();
                    $newBooking->StartTime = $bookingRequest->StartTime;
                    $newBooking->EndTime = $bookingRequest->EndTime;
                    $newBooking->RoomId = $room->Id;
                    $newBooking->BookingRequestId = $bookingRequest->Id;
                    $newBooking->Shuffled = true;
                    $newBooking->Id = null;
                    $newBooking->NumberOfSeats = $bookingRequest->NumberOfSeats;
                    $newBooking->UserId = $bookingRequest->UserId;
                    $bookingsArrayClone[] = $newBooking;
                }

                //if the booking was successful with no conflicts we can return from the recursive loop with a new array
                return $bookingsArrayClone;
            }
        }

        //foreach that runs if we haven't returned from the recursion yet
        foreach ($rooms as $room) {

            //gets a list of all bookings that are larger or of equal size of the request and in valid rooms
            $replaceableBookings = $this->getClonedBookingsPerRoom($bookingRequest->StartTime,
                $bookingRequest->EndTime, $room, $bookingsArrayClone);

            //attempts to replaces all of the bookings that we found to be replaceable
            foreach ($replaceableBookings as $booking) {

                //checks to see if we need to make a new booking or just update one with a new Id
                $alreadyHasBooking = false;
                foreach ($bookingsArrayClone as $presentBooking) {
                    if ($presentBooking->BookingRequestId == $bookingRequest->Id) {
                        $alreadyHasBooking = true;
                        $presentBooking->RoomId = $room->Id;
                        $presentBooking->Shuffled = true;
                    }
                }

                //runs if there is no current booking for the request
                if (!$alreadyHasBooking) {

                    //creates a new booking for the room
                    $newBooking = new BookingEntity();
                    $newBooking->RoomId = $booking->RoomId;
                    $newBooking->StartTime = $bookingRequest->StartTime;
                    $newBooking->EndTime = $bookingRequest->EndTime;
                    $newBooking->UserId = $bookingRequest->UserId;
                    $newBooking->NumberOfSeats = $bookingRequest->NumberOfSeats;
                    $newBooking->BookingRequestId = $bookingRequest->Id;
                    $newBooking->RoomId = $room->Id;

                    //shuffled is set to true so we don't move it again, avoiding getting stuck in a loop
                    $newBooking->Shuffled = true;

                    //adds it to the booking array clone
                    $bookingsArrayClone[] = $newBooking;

                    //removes the room Id for the current Booking we are swapping out
                    $booking->RoomId = null;

                }

                //passes in the request for the booking we swapped out and the cloned array to the same method
                $BookingReqToBePassed = null;
                $bookingRequests = BookingRequest::all();
                foreach ($bookingRequests as $testerRequester) {
                    if ($testerRequester->Id == $booking->BookingRequestId) {
                        $BookingReqToBePassed = $testerRequester;
                    };
                }
                $bookingsArrayClone = $this->recursiveShuffle($BookingReqToBePassed, $bookingsArrayClone);
                if ($bookingsArrayClone != false){
                    return $bookingsArrayClone;
                }
            }
        }
        //if all bookings can be made, return false
        return false;
    }


    //checks to make sure no bookings touch the room during the time of booking
    private function clonedRoomIsAvailable($StartTime, $EndTime, $room, $clonedArray)
    {
        $isFree = true;
        foreach ($clonedArray as $booking) {
            //snakes in a box
            if (
                (($booking->StartTime >= $StartTime && $booking->StartTime <= $EndTime) ||
                    ($booking->EndTime >= $StartTime && $booking->EndTime <= $EndTime) ||
                    ($booking->EndTime >= $EndTime && $booking->StartTime <= $StartTime))
                && $booking->RoomId == $room->Id
            ) {
                $isFree = false;
                return $isFree;
            }
        }
        return $isFree;
    }

    //checks for all bookings that can be replaced by our new booking in a room
    public function getClonedBookingsPerRoom($StartTime, $EndTime, $room, $clonedArray)
    {
        //declares a new empty array of bookings
        $swappableBookings = Array();

        //checks each booking in the array to see if they overlap our desired booking time
        if (is_array($clonedArray) || is_object($clonedArray)) {
            foreach ($clonedArray as $booking) {
                if (
                    ($booking->StartTime <= $StartTime && $booking->EndTime >= $EndTime)
                    && $booking->RoomId == $room->Id && $booking->Shuffled != true && $booking->Locked != true
                ) {
                    //adds the booking to the list of ones we can swap out
                    $swappableBookings[] = $booking;
                }
                return $swappableBookings;
            }
        }
        return $swappableBookings;
    }


}