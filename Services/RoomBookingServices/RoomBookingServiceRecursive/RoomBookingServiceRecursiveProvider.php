<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 30/03/18
 * Time: 16:12
 */

namespace SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceRecursive;


use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceSmallestFirst\AbstractSmallestFirstProvider;

class RoomBookingServiceRecursiveProvider extends AbstractSmallestFirstProvider
{
    /**
     * @param BookingRequest $bookingRequestFromClient
     * @return bool
     * @throws \Exception
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    protected function moveThingsAround(BookingRequest $bookingRequestFromClient): bool
    {
        return RoomBookingServiceRecursiveSort::create()->execute($bookingRequestFromClient);
    }

    public static function getName(): string
    {
        return "Recursive Sorting Algorithm Provider";
    }

    public static function getDescription(): string
    {
        return "This sorting class recursively re-arranges bookings 
                to make as many as possible fit in a timetabled block.";
    }

}