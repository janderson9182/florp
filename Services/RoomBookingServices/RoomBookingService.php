<?php

namespace SpaceCadets\Florp\Services\RoomBookingServices;

use Rhubarb\Crown\DependencyInjection\SingletonProviderTrait;
use Rhubarb\Scaffolds\BackgroundTasks\BackgroundTask;

abstract class RoomBookingService extends BackgroundTask
{
    const SETTING_BOOKING_REQUEST_ID = "BookingRequestId";

    private static $roomBookingServiceClasses;

    public static abstract function getName(): string;

    public static abstract function getDescription(): string;

    /**
     * @param RoomBookingService|string $roomBookingService
     */
    public final static function registerRoomBookingServiceClass($roomBookingService)
    {
        self::$roomBookingServiceClasses[$roomBookingService::getName()] = $roomBookingService;
    }

    /**
     * @return RoomBookingService[] static
     */
    public final static function getRegisteredClasses()
    {
        return self::$roomBookingServiceClasses;
    }

    /**
     * @param $name
     * @return RoomBookingService static
     */
    public static function findClassByName($name)
    {
        return self::$roomBookingServiceClasses[$name];
    }
}