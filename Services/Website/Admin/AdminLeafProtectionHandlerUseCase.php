<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 06/03/18
 * Time: 20:23
 */

namespace SpaceCadets\Florp\Services\Website\Admin;


use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\Response\RedirectResponse;
use SpaceCadets\Florp\Models\FlorpUser;
use SpaceCadets\Florp\Routes;
use SpaceCadets\Florp\Services\UseCase;
use SpaceCadets\Florp\Website\Login\SiteLogin;

class AdminLeafProtectionHandlerUseCase extends UseCase
{
    public function execute(){
        $currentUser = SiteLogin::getLoggedInUser();
        if(!$currentUser->hasRole(FlorpUser::USER_ROLE_ADMIN)){
            throw new ForceResponseException(new RedirectResponse("/" . Routes::APP));
        }
    }
}