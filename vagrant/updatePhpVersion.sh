#!/usr/bin/env bash

sudo yum remove php70* -y
sudo yum install php71* -y --skip-broken
# Not found bt the wildcard
sudo yum install mod_php71w.x86_64 -y

sudo service httpd restart