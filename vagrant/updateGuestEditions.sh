#!/usr/bin/env bash

sudo yum update -y

cd /opt
sudo wget -c http://download.virtualbox.org/virtualbox/5.1.26/VBoxGuestAdditions_5.1.26.iso
sudo mount VBoxGuestAdditions_5.1.26.iso -o loop /mnt
cd /mnt/
###################################################################
# -y might not work, might need to create a second script
###################################################################
sudo sh VBoxLinuxAdditions.run --nox11 -y
cd /opt
sudo rm *.iso
sudo /etc/init.d/vboxadd setup
sudo chkconfig --add vboxadd
sudo chkconfig vboxadd on

echo "You need to vagrant halt and vagrant up";