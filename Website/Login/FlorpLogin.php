<?php

namespace SpaceCadets\Florp\Website\Login;

use Rhubarb\Scaffolds\Authentication\Leaves\Login;

class FlorpLogin extends Login
{
    protected function getViewClass()
    {
        return FlorpLoginView::class;
    }
}