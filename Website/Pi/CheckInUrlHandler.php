<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 27-Feb-18
 * Time: 20:16
 */

namespace SpaceCadets\Florp\Website\Pi;


use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\Request\Request;
use Rhubarb\Crown\Response\Response;
use Rhubarb\Crown\UrlHandlers\CallableUrlHandler;
use SpaceCadets\Florp\Services\src\Models\Bookings\CreateCheckInForRoomUseCase;

class CheckInUrlHandler extends CallableUrlHandler
{
    public function __construct(array $childUrlHandlers = [])
    {
        parent::__construct(function () {

            $request = Request::current();
            $roomId = $request->get("room");
            $response = new Response();
            if (!isset($roomId)) {
                $response->setResponseCode(Response::HTTP_STATUS_CLIENT_ERROR_BAD_REQUEST);
                throw new ForceResponseException($response);
            }
            CreateCheckInForRoomUseCase::create()->execute($request->get("room"));
            $response->setResponseCode(Response::HTTP_STATUS_SUCCESS_OK);
            throw new ForceResponseException($response);
        },
            $childUrlHandlers
        );
    }

}