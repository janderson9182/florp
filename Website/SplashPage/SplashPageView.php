<?php

namespace SpaceCadets\Florp\Website\SplashPage;

use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\Layout\LayoutModule;
use Rhubarb\Crown\LoginProviders\Exceptions\NotLoggedInException;
use Rhubarb\Crown\Response\RedirectResponse;
use Rhubarb\Leaf\LayoutProviders\LayoutProvider;
use Rhubarb\Leaf\Views\View;
use Rhubarb\Scaffolds\Authentication\User;
use SpaceCadets\Florp\Website\Layouts\DefaultLayout;
use SpaceCadets\Florp\Website\Login\SiteLogin;

class SplashPageView extends View
{
    /**
     * @var SplashPageModel
     */
    protected $model;

    /**
     * @throws \Rhubarb\Crown\Layout\Exceptions\LayoutNotFoundException
     */
    protected function createSubLeaves()
    {
        LayoutModule::setLayoutClassName(SplashLayoutProvider::class);
    }

    protected function printViewContent()
    {
        try{
            // If the user is logged in, redirect them to the app page
            SiteLogin::getLoggedInUser();
            throw new ForceResponseException(new RedirectResponse("/app/"));
        } catch (NotLoggedInException $e)
        {}

        ?>
        <div class="splashContainer">
            <div class="splash">
                <img src="https://s3.us-east-2.amazonaws.com/florpdata/brands/florp/florpLogoWhite.svg"
                     alt="Florp logo">
                <p><i>Florp is a smart automation room booking system</i></p>
            </div>
        </div>
        <?php
    }
}