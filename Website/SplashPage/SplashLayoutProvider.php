<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 30/03/2018
 * Time: 15:44
 */

namespace SpaceCadets\Florp\Website\SplashPage;

use SpaceCadets\Florp\Website\Layouts\DefaultLayout;

class SplashLayoutProvider extends DefaultLayout
{
    protected function printPageHeading()
    {
        // Don't show the nav
    }
}