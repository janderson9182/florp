<?php

namespace SpaceCadets\Florp\Website\SplashPage;

use Rhubarb\Leaf\Leaves\Leaf;

class SplashPage extends Leaf
{
    /**
    * @var SplashPageModel
    */
    protected $model;
    
    protected function getViewClass()
    {
        return SplashPageView::class;
    }
    
    protected function createModel()
    {
        $model = new SplashPageModel();

        // If your model has events you want to listen to you should attach the handlers here
        // e.g.
        // $model->selectedUserChangedEvent->attachListener(function(){ ... });

        return $model;
    }
}