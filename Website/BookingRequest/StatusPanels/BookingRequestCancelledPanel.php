<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 30/03/2018
 * Time: 15:35
 */

namespace SpaceCadets\Florp\Website\BookingRequest\StatusPanels;

class BookingRequestCancelledPanel extends AbstractStatusPanel
{
    /**
     * @return string css class names for the top level container panel
     */
    protected function getParentClassNames(): string
    {
        return "panel-info";
    }

    /**
     * @return string the text you wish to display in the body of the panel
     */
    protected function getBodyText(): string
    {
        return "Booking Request Cancelled";
    }
}