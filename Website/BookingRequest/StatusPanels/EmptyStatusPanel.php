<?php

namespace SpaceCadets\Florp\Website\BookingRequest\StatusPanels;

/**
 * Class EmptyStatusPanel
 * @package SpaceCadets\Florp\Website\BookingRequest\StatusPanels
 * @author James Anderson
 *
 * Don't display anything in the panel at all in this case
 */
class EmptyStatusPanel extends AbstractStatusPanel
{
    /**
     * @inheritdoc
     */
    public function __toString()
    {
        return "";
    }

    /**
     * @inheritdoc
     */
    protected function getParentClassNames(): string
    {
        return "";
    }

    /**
     * @inheritdoc
     */
    protected function getBodyText(): string
    {
        return "";
    }
}