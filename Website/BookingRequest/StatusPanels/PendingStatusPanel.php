<?php

namespace SpaceCadets\Florp\Website\BookingRequest\StatusPanels;

/**
 * Class PendingStatusPanel
 * @package SpaceCadets\Florp\Website\BookingRequest\StatusPanels
 * @author James Anderson
 * Panel with a status of pending
 */
class PendingStatusPanel extends AbstractStatusPanel
{
    /**
     * @inheritdoc
     */
    protected function getParentClassNames(): string
    {
        return "panel-info";
    }

    /**
     * @inheritdoc
     */
    protected function getBodyText(): string
    {
        return "Pending";
    }
}