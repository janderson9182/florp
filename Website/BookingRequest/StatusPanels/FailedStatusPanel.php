<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 30/03/2018
 * Time: 21:05
 */

namespace SpaceCadets\Florp\Website\BookingRequest\StatusPanels;

class FailedStatusPanel extends AbstractStatusPanel
{
    /**
     * @return string css class names for the top level container panel
     */
    protected function getParentClassNames(): string
    {
        return "panel-danger";
    }

    /**
     * @return string the text you wish to display in the body of the panel
     */
    protected function getBodyText(): string
    {
        return "No Booking Created, Please Create a new booking request and try again";
    }
}