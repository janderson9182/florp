<?php

namespace SpaceCadets\Florp\Website\BookingRequest\StatusPanels;

/**
 * Class AbstractStatusPanel
 * @package SpaceCadets\Florp\Website\BookingRequest\StatusPanels
 * @author James Anderson
 * Follows the Template design pattern
 */
abstract class AbstractStatusPanel
{
    public function __toString()
    {
        return <<<HTML
            <div class="panel {$this->getParentClassNames()}" id="{$this->getId()}">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        {$this->getTitle()}
                    </h3>
                </div>
                <div class="panel-body">
                    {$this->getBodyText()}
                </div>
            </div>
HTML;
    }

    /**
     * @return string css class names for the top level container panel
     */
    abstract protected function getParentClassNames(): string;

    /**
     * @return string ID for the panel, set to StatusPanel by default
     */
    protected function getId(): string
    {
        return "StatusPanel";
    }

    /**
     * @return string Title of the Panel
     */
    protected function getTitle(): string
    {
        return "Status";
    }

    /**
     * @return string the text you wish to display in the body of the panel
     */
    abstract protected function getBodyText(): string;
}