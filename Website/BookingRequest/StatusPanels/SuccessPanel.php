<?php

namespace SpaceCadets\Florp\Website\BookingRequest\StatusPanels;

use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Routes;

/**
 * Class SuccessPanel
 * @package SpaceCadets\Florp\Website\BookingRequest\StatusPanels
 * @author James Anderson
 * Panel with a status of pending
 */
class SuccessPanel extends AbstractStatusPanel
{
    /** @var Booking $booking */
    private $booking;

    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * @inheritdoc
     */
    protected function getParentClassNames(): string
    {
        return "panel-success";
    }

    /**
     * @inheritdoc
     */
    protected function getBodyText(): string
    {
        $bookingUrl = "/" . Routes::APP . Routes::BOOKING . $this->booking->UniqueIdentifier . "/";
        return <<<HTML
Success! Click <a href="{$bookingUrl}">here</a> to view booking details 
HTML;

    }
}