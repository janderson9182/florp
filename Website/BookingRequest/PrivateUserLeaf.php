<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 04/04/2018
 * Time: 17:25
 */

namespace SpaceCadets\Florp\Website\BookingRequest;

use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\Response\RedirectResponse;
use Rhubarb\Leaf\Crud\Leaves\CrudLeaf;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Models\FlorpUser;
use SpaceCadets\Florp\Website\Login\SiteLogin;

abstract class PrivateUserLeaf extends CrudLeaf
{
    /**
     * PrivateUserLeaf constructor.
     * @param null $modelOrCollection
     * @throws ForceResponseException
     * @throws \Rhubarb\Leaf\Exceptions\InvalidLeafModelException
     */
    public function __construct($modelOrCollection = null)
    {
        parent::__construct($modelOrCollection);
        /** @var FlorpUser $user */
        $user = SiteLogin::getLoggedInUser();
        /** @var Booking|BookingRequest $model */
        $model = $this->model->restModel;
        if
        (
            !$model->isNewRecord()
            && (
                $model->UserId != $user->UserID
                && !$user->hasRole("admin")
            )
        ) {
            throw new ForceResponseException(new RedirectResponse("../"));
        }
    }
}