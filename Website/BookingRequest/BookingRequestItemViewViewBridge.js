var bridge = function (leafPath) {
    window.rhubarb.viewBridgeClasses.ViewBridge.apply(this, arguments);
};
// Slight hack, there is probably a better way
var invalidFields = 0;

bridge.prototype = new window.rhubarb.viewBridgeClasses.ViewBridge();

bridge.prototype.attachEvents = function () {
    var self = this;
    var backgroundTaskId = 0;
    const startTime = this.findChildViewBridge("StartTime");
    const endTime = this.findChildViewBridge("EndTime");
    var submitBookingRequest = document.getElementById('CheckForBookings');
    if (submitBookingRequest != null) {
        submitBookingRequest.onclick = function (event) {
            event.preventDefault();
            this.setAttribute('disabled', true);
            /**
             * param 1 : Event Name to raise in our Model (In the Model, this has "Event" at the end of it's name
             * param 2 : Success Callback
             * param 3 : Failure Callback
             */
            self.raiseServerEvent(
                "ProcessBookingRequest",
                function (taskStatusId) {
                    // Pass the status ID to the progress presenter.
                    backgroundTaskId = taskStatusId;
                    var progress = self.findViewBridge("Progress");
                    progress.setBackgroundTaskStatusId(taskStatusId);
                    console.log("tsid" + taskStatusId);
                    progress.onProgressReported = function (progress) {
                        this.progressNode.style.width = progress.percentageComplete + "%";
                        this.messageNode.innerHTML = progress.message;
                        if (progress.message === "No Booking Created") {
                            alert("The Booking Was Not Created, Please create a new request and try again");
                            var inputs = document.getElementsByTagName("input");
                            for (var i = 0; i < inputs.length; i++) {
                                inputs[i].disabled = true;
                            }
                        } else if (progress.message === "Booking Created") {
                            alert("Booking Created. Redirecting to your booking page");
                            location.href = location.href.replace("-request", "s");
                        }
                    };
                },
                function () {
                    alert("Something went wrong")
                }
            );
        };
    }

    // Ensure that the StartTime cannot be greater than the EndTime
    // Ensure we can't submit the form without the validation passing
};

bridge.prototype.constructor = bridge;

window.rhubarb.viewBridgeClasses.BookingRequestItemViewViewBridge = bridge;

window.rhubarb.validation.common.endTimeGreaterThan = function (startTime) {
    return function (endTime, successCallback, failedCallback) {
        if (!(startTime.getDate() === endTime.getDate())) {
            invalidFields++;
            return failedCallback("The Selected 'End Time' should be on the same day as the 'Start Day'");
        }

        if (!(endTime > startTime)) {
            invalidFields++;
            return failedCallback("The 'End Time' should be later than the 'Start Time'");
        }
        invalidFields--;
        successCallback();
    };
};