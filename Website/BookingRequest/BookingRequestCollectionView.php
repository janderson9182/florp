<?php

namespace SpaceCadets\Florp\Website\BookingRequest;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Leaf\Crud\Leaves\CrudModel;
use Rhubarb\Leaf\Crud\Leaves\CrudView;
use Rhubarb\Leaf\Leaves\LeafDeploymentPackage;
use Rhubarb\Stem\Collections\Collection;
use Rhubarb\Stem\Filters\GreaterThan;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\src\Models\Bookings\BookingRequests\GetBookingRequestsForUserUseCase;
use SpaceCadets\Florp\Website\Login\SiteLogin;

class BookingRequestCollectionView extends CrudView
{
    /**
     * @var CrudModel
     */
    protected $model;

    protected function getAdditionalResourceUrls()
    {
        return [
            "https://unpkg.com/ag-grid/dist/ag-grid.min.js"
        ];
    }

    /**
     * @throws \Rhubarb\Scaffolds\Authentication\LoginProviders\NotLoggedInException
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    protected function printViewContent()
    {
        $json = GetBookingRequestsForUserUseCase::create()
            ->execute(SiteLogin::getLoggedInUser())
            ->filter
            (
                new GreaterThan(
                    BookingRequest::COLUMN_START_TIME,
                    new RhubarbDateTime("now - 12 hours"),
                    true
                )
            );

        $results = $this->prepareBookingsForAGGrid($json);

        ?>

        <div class="container">
            <h2>Upcoming Bookings & Requests</h2>
            <div class="flex flex-row flex-space-between">
                <div>
                    <a href="add/" class="btn btn-success"><i class="fa fa-plus fa-2" aria-hidden="true"></i> add</a>
                </div>
                <div><a href="#" id="ShowOldBookings" class="btn btn-primary">Show Old Booking In This List</a>
                </div>
            </div>
            <div id="myGrid" style="height: 300px;width:800px;" class="ag-theme-material"></div>
        </div>

        <script>

            //Grid columns
            var columnDefs = [
                {
                    headerName: "Booking Requests",
                    children: [
                        {headerName: "Start Time", field: "StartTime", valueFormatter: startDateFormatter},
                        {headerName: "End Time", field: "EndTime", valueFormatter: hourFormatter},
                        {headerName: "View", field: "ViewLink", cellRenderer: viewFormatter}
                    ]
                }
            ];

            function startDateFormatter(params) {
                var options = {
                    weekday: "long", year: "numeric", month: "short",
                    day: "numeric", hour: "2-digit", minute: "2-digit"
                };
                var formattedStartTime = new Date(params.data.StartTime);
                var optionsFormattedStartTime = formattedStartTime.toLocaleDateString('en-us', options);
                return optionsFormattedStartTime;
            };

            function viewFormatter(data) {
                return data.data.ViewLink;
            }

            function hourFormatter(params) {
                var formattedEndTime = new Date(params.data.EndTime);
                var formattedEndTimeHours = "";
                var formattedEndTimeMinutes = formattedEndTime.getMinutes();

                if (formattedEndTime.getHours() > 12) {
                    var formattedEndTimeString = ((formattedEndTime.getHours() - 12) + "." + formattedEndTimeMinutes + " " + "PM");
                } else {
                    var formattedEndTimeString = (formattedEndTime.getHours() + "." + formattedEndTimeMinutes + " " + "AM");
                }
                ;
                //formattedEndTimeString.padEnd(2 [, 0])
                return formattedEndTimeString;
            };

            var rowData = <?=json_encode($results);?>;

            rowData.forEach(function (row) {
                var startDate = new Date(row.StartDate);
                var startDateString = startDate.getDate();
                row.StartDate = startDateString;
            });

            //Grid options
            var gridOptions = {
                columnDefs: columnDefs,
                rowData: rowData,

                //Make the grid data sortable
                enableSorting: true,

                //Make the grid data filterable
                enableFilter: true,

                onGridReady: function (params) {
                    params.api.sizeColumnsToFit();
                }
            };

            //Wait for the document to be loaded, otherwise ag-Grid will not find the div in the document.
            var mygrid;
            document.addEventListener("DOMContentLoaded", function () {

                var eGridDiv = document.querySelector('#myGrid');
                mygrid = new agGrid.Grid(eGridDiv, gridOptions);
                mygrid.redrawAllRows = function (data) {
                    gridOptions.api.setRowData(data);
                }
            });

        </script>

        <?php

    }

    protected function getViewBridgeName()
    {
        return "BookingRequestCollectionViewBridge";
    }

    public function getDeploymentPackage()
    {
        return new LeafDeploymentPackage(__DIR__ . "/BookingRequestCollectionViewBridge.js");
    }

    /**
     * @param $json
     * @return array
     */
    public function prepareBookingsForAGGrid(Collection $json): array
    {
        $results = [];
        $i = 0;
        foreach ($json as $item) {
            $exportedData = $item->exportData();
            $exportedData["ViewLink"] = $item->ViewLink;
            $results[$i] = $exportedData;
            $i++;
        }
        return $results;
    }
}
