var bridge = function (leafPath) {
    window.rhubarb.viewBridgeClasses.ViewBridge.apply(this, arguments);
};

bridge.prototype = new window.rhubarb.viewBridgeClasses.ViewBridge();

bridge.prototype.attachEvents = function () {
    var self = this;
    var showOldBookings = document.getElementById("ShowOldBookings");
    showOldBookings.onclick = function (event) {
        event.preventDefault();
        event.stopPropagation();
        var updateDataValueForTable = function (json) {
            mygrid.redrawAllRows(json);
        };
        self.raiseServerEvent(
            "ShowOldBookings",
            updateDataValueForTable
        );
    }

};

bridge.prototype.constructor = bridge;

window.rhubarb.viewBridgeClasses.BookingRequestCollectionViewBridge = bridge;