var bridge = function (leafPath) {
    window.rhubarb.viewBridgeClasses.ViewBridge.apply(this, arguments);
};

bridge.prototype = new window.rhubarb.viewBridgeClasses.ViewBridge();

bridge.prototype.attachEvents = function () {
    var self = this;

    var faultyPropertyHtmlCollection = document.getElementsByClassName("js-property-faulty");
    var faultyPropertyHtmlElements = [].slice.call(faultyPropertyHtmlCollection);
    faultyPropertyHtmlElements.forEach(function (currentProperty) {

        currentProperty.onclick = function (event) {
            var successfulReport = function () {
                alert("successfully reported as faulty");
                location.reload()
            };
            event.preventDefault();
            event.stopPropagation();
            self.raiseServerEvent(
                "PropertyBroken",
                this.attributes.rpid.value,
                successfulReport()
            );
        }
    });

};

bridge.prototype.constructor = bridge;

window.rhubarb.viewBridgeClasses.BookingItemViewBridge = bridge;