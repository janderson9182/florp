<?php

namespace SpaceCadets\Florp\Website\BookingRequest\Booking;

use Rhubarb\Leaf\Crud\Leaves\CrudLeaf;
use Rhubarb\Leaf\Crud\Leaves\CrudModel;

class BookingCollection extends CrudLeaf
{
    /**
     * @var CrudModel
     */
    protected $model;

    /**
     * Returns the name of the standard view used for this leaf.
     *
     * @return string
     */
    protected function getViewClass()
    {
        return BookingCollectionView::class;
    }
}