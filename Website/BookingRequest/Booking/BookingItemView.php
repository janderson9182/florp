<?php

namespace SpaceCadets\Florp\Website\BookingRequest\Booking;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\LoginProviders\Exceptions\NotLoggedInException;
use Rhubarb\Crown\Response\RedirectResponse;
use Rhubarb\Leaf\Controls\Common\Buttons\Button;
use Rhubarb\Leaf\Crud\Leaves\CrudModel;
use Rhubarb\Leaf\Crud\Leaves\CrudView;
use Rhubarb\Leaf\Leaves\LeafDeploymentPackage;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\FlorpUser;
use SpaceCadets\Florp\Routes;
use SpaceCadets\Florp\Website\Login\SiteLogin;

class BookingItemView extends CrudView
{
    /**
     * @var BookingItemModel
     */
    protected $model;

    protected function createSubLeaves()
    {
        parent::createSubLeaves();
        $this->registerSubLeaf(
            $cancelBooking = new Button("CancelBookingEvent", "Cancel Booking",
                function () {
                    $this->model->CancelBookingEvent->raise();
                }
            )
        );
        $cancelBooking->setConfirmMessage("Are you sure you want to cancel your booking?");
        $cancelBooking->addCssClassNames("btn btn-primary");
    }

    /**
     * @throws ForceResponseException
     * @throws \Rhubarb\Scaffolds\Authentication\LoginProviders\NotLoggedInException
     */
    protected function printViewContent()
    {
        try {
            /** @var FlorpUser $user */
            $user = SiteLogin::getLoggedInUser();
            /** @var Booking $booking */
            $booking = $this->model->restModel;
            if ($booking->UserId != $user->UserID) {
                // Only the user who owns the booking should be able to see their booking details
                throw new ForceResponseException(new RedirectResponse("/" . Routes::APP));
            }
            $checkedIn = '<input type="checkbox" name="CheckedIn" id="CheckedIn" ';
            $checkedIn .= $booking->CheckedIn ? "checked" : "";
            $checkedIn .= ' disabled>';
            print '<div class="container">';
            $leaves = [
                "Booking Status" => $this->getBookingStatus($booking)
            ];

            if($booking->Locked)
            {
                $leaves["Room Description"] = $booking->Room->Description;
            }
            if (!$booking->Cancelled) {
                $leaves = array_merge(
                    $leaves,
                    [
                        "Booking Status" => $this->getBookingStatus($booking),
                        "Building" => $booking->Room->Floor->Building->Name,
                        "Booking Created By" => htmlentities($user->getFullName()),
                        "Start Time" => $booking->StartTime->format(Booking::DateFormat),
                        "End Time" => $booking->EndTime->format(Booking::DateFormat),
                        "Checked In" => $checkedIn
                    ]
                );
                // You can cancel a booking until it starts
                if (new RhubarbDateTime("now") <= $booking->StartTime) {
                    $leaves[] = "CancelBookingEvent";
                }
            }
            $this->layoutItemsWithContainer(
                "Booking Details",
                $leaves
            );
            print '<div class="container">';
            if (!$booking->Cancelled && $booking->BookingRequest->Properties->count()>0) {
                ?>
                <h3>Properties You Requested</h3>
                <ul>
                    <?php
                    foreach ($booking->BookingRequest->Properties as $property) {
                        ?>
                        <li><?= $property->Name ?></li>
                        <?php
                    }
                    ?>
                </ul>
                <?php
            }
            print $this->printAssistanceDuringBooking($booking);
            print $this->printImagesOfRoom();
            print '</div>';
        } catch (NotLoggedInException $e) {
            throw new ForceResponseException(new RedirectResponse(Routes::LOGIN));
        }
    }

    protected function getViewBridgeName()
    {
        return "BookingItemViewBridge";
    }

    public function getDeploymentPackage()
    {
        return new LeafDeploymentPackage(__DIR__ . "/BookingItemViewBridge.js");
    }

    protected function printImagesOfRoom()
    {
        /** @var Booking $booking */
        $booking = $this->model->restModel;
        if ($booking->Locked) {
            if ($booking->Room->Images->count() > 0) {
                ?>
                <h2>Images of this room</h2>
                <div class="room-image-list">
                    <?php
                    foreach ($booking->Room->Images as $image) {
                        ?>
                        <img src="<?= $image->FilePath ?>" alt="<?= $image->ImageName ?>">
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
        }
    }

    /**
     * @param $booking
     * @return string assistance HTML
     */
    protected function printAssistanceDuringBooking($booking): string
    {
        $html = "";
        if ($booking->Locked && $booking->CheckedIn) {
            $html .= <<<HTML
            <h2>Assistance During Booking</h2>
            <h4>To Notify building staff of problems in this room, please click on the items below</h4>
            <ul>
HTML;
            foreach ($booking->Room->PropertiesRaw as $roomProperty) {
                $html .= "<li>";
                if ($roomProperty->Faulty) {
                    $html .= $roomProperty->Property->Name . " has been reported faulty";
                } else {
                    $html .= <<<HTML
                            <a href="#" rpid="{$roomProperty->Id}" class="js-property-faulty">
                                Report {$roomProperty->Property->Name} as faulty
                            </a>
HTML;
                }
                $html .= "</li>";
            }
            $html .= "</ul>";
        }
        return $html;
    }

    /**
     * @param $booking
     * @return string
     */
    protected function getBookingStatus(Booking $booking): string
    {
        $bookingStatus = $booking->Locked ?
            "Locked in, your room is: " . htmlentities($booking->Room->Name) :
            "Pending, your booking is now guaranteed. Please check back 24 hours before your booking to find out which room you are in";
        if ($booking->Cancelled) {
            return "Booking Cancelled";
        }

        return $bookingStatus;
    }
}