<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 20/03/2018
 * Time: 17:49
 */

namespace SpaceCadets\Florp\Website\BookingRequest\Booking;

use Rhubarb\Crown\Events\Event;
use Rhubarb\Leaf\Crud\Leaves\CrudModel;

class BookingItemModel extends CrudModel
{
    /**
     * @var Event $PropertyBrokenEvent
     */
    public $PropertyBrokenEvent;
    /**
     * @var Event $CancelBookingEvent
     */
    public $CancelBookingEvent;

    public function __construct()
    {
        parent::__construct();
        $this->PropertyBrokenEvent = new Event();
        $this->CancelBookingEvent = new Event();
    }
}