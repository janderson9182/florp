<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 20/03/2018
 * Time: 17:49
 */
namespace SpaceCadets\Florp\Website\BookingRequest\Booking;

use Rhubarb\Crown\Events\EventEmitter;
use Rhubarb\Leaf\Crud\Leaves\CrudLeaf;
use SpaceCadets\Florp\Models\Assets\Properties\RoomProperty;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Website\BookingRequest\PrivateUserLeaf;

class BookingItem extends PrivateUserLeaf
{
    use EventEmitter;
    /**
     * @var BookingItemModel
     */
    protected $model;

    /**
     * Returns the name of the standard view used for this leaf.
     *
     * @return string
     */
    protected function getViewClass()
    {
        return BookingItemView::class;
    }

    protected function createModel()
    {
        return new BookingItemModel();
    }

    protected function onModelCreated()
    {
        parent::onModelCreated();

        $this->model->PropertyBrokenEvent->attachHandler(
            function ($roomPropertyId) {
                $roomProperty = new RoomProperty($roomPropertyId);
                $roomProperty->Faulty = true;
                $roomProperty->save();

                return true;
            }
        );

        $this->model->CancelBookingEvent->attachHandler(
            function()
            {
                /** @var Booking $booking */
                $booking = $this->model->restModel;
                $booking->Cancelled = true;
                $booking->save();
                $booking->BookingRequest->Cancelled = true;
                $booking->BookingRequest->save();
            }
        );
    }
}