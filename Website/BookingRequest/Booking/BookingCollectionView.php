<?php

namespace SpaceCadets\Florp\Website\BookingRequest\Booking;

use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\Response\RedirectResponse;
use Rhubarb\Leaf\Crud\Leaves\CrudModel;
use Rhubarb\Leaf\Crud\Leaves\CrudView;
use SpaceCadets\Florp\Routes;

class BookingCollectionView extends CrudView
{
    /**
     * @var CrudModel
     */
    protected $model;

    protected function createSubLeaves()
    {
        parent::createSubLeaves();
    }

    /**
     * @throws ForceResponseException
     */
    protected function printViewContent()
    {
        // No one should be able to get to this page for now
        throw new ForceResponseException(new RedirectResponse("/" . Routes::APP));
    }

}