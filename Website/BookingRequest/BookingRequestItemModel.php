<?php
/**
 * Created by Team Space Cadets
 * User: james & stephen
 * Date: 15/02/2018
 * Time: 17:16
 */

namespace SpaceCadets\Florp\Website\BookingRequest;

use Rhubarb\Crown\Events\Event;
use Rhubarb\Leaf\Crud\Leaves\CrudModel;
use Rhubarb\Stem\Collections\Collection;

class BookingRequestItemModel extends CrudModel
{
    /** @var Event $ProcessBookingRequestEvent */
    public $ProcessBookingRequestEvent;
    public $BackgroundTaskStatusId;
    /**
     * @var Collection $AllProperties
     */
    public $AllProperties;
    /**
     * @var Event $addPropertiesToBookingRequestButtonEvent
     */
    public $addPropertiesToBookingRequestButtonEvent;

    public function __construct()
    {
        parent::__construct();
        $this->ProcessBookingRequestEvent = new Event();
        $this->addPropertiesToBookingRequestButtonEvent = new Event();
    }
}