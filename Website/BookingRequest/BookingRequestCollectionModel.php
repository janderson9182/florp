<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 17/03/2018
 * Time: 12:27
 */

namespace SpaceCadets\Florp\Website\BookingRequest;

use Rhubarb\Crown\Events\Event;
use Rhubarb\Leaf\Crud\Leaves\CrudModel;

class BookingRequestCollectionModel extends CrudModel
{
    /**
     * @var Event $ShowOldBookingsEvent
     */
    public $ShowOldBookingsEvent;

    public function __construct()
    {
        parent::__construct();
        $this->ShowOldBookingsEvent = new Event();
    }
}