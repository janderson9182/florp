<?php

namespace SpaceCadets\Florp\Website\BookingRequest;

use Rhubarb\Leaf\Crud\Leaves\CrudLeaf;
use Rhubarb\Leaf\Crud\Leaves\CrudModel;
use SpaceCadets\Florp\Services\src\Models\Bookings\BookingRequests\GetBookingRequestsForUserUseCase;
use SpaceCadets\Florp\Website\Login\SiteLogin;

class BookingRequestCollection extends CrudLeaf
{
    /**
     * @var BookingRequestCollectionModel
     */
    protected $model;
    /**
     * @var BookingRequestCollectionView $view
     */
    protected $view;

    /**
     * Returns the name of the standard view used for this leaf.
     *
     * @return string
     */
    protected function getViewClass()
    {
        return BookingRequestCollectionView::class;
    }

    protected function createModel()
    {
        return new BookingRequestCollectionModel();
    }

    protected function onModelCreated()
    {
        parent::onModelCreated();

        $this->model->ShowOldBookingsEvent->attachHandler(function () {
            return $this->view->prepareBookingsForAGGrid(
                GetBookingRequestsForUserUseCase::create()->execute(SiteLogin::getLoggedInUser())
            );
        });
    }
}