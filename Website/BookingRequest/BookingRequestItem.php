<?php

namespace SpaceCadets\Florp\Website\BookingRequest;

use Rhubarb\Crown\Exceptions\ClassMappingException;
use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\Response\RedirectResponse;
use Rhubarb\Leaf\Crud\Leaves\CrudLeaf;
use SpaceCadets\Florp\FlorpSettings;
use SpaceCadets\Florp\Models\Bookings\BookingAlgorithmNotSpecifiedException;
use Rhubarb\Crown\Request\Request;
use SpaceCadets\Florp\Models\Assets\Properties\Property;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Website\Login\SiteLogin;

class BookingRequestItem extends PrivateUserLeaf
{
    /**
     * @var BookingRequestItemModel
     */
    protected $model;

    /**
     * Returns the name of the standard view used for this leaf.
     *
     * @return string
     */
    protected function getViewClass()
    {
        return BookingRequestItemView::class;
    }

    protected function createModel()
    {
        return new BookingRequestItemModel();
    }

    protected function onModelCreated()
    {
        parent::onModelCreated();
        $this->model->ProcessBookingRequestEvent->attachHandler(function () {
            try {
                $florpSettings = FlorpSettings::singleton();
                $roomBookingServiceProvider = $florpSettings->roomBookingProviderClass;
                $status = $roomBookingServiceProvider::initiate(
                    [
                        "BookingRequestId" => $this->model->restModel->UniqueIdentifier
                    ]
                );
                return $status->BackgroundTaskStatusID;
            } catch (ClassMappingException $e) {
                throw new BookingAlgorithmNotSpecifiedException();
            }
        });

        $this->model->AllProperties = Property::all();
        $this->model->addPropertiesToBookingRequestButtonEvent->attachHandler(function () {
            /** @var BookingRequest $bookingRequest */
            $bookingRequest = $this->model->restModel;
            $bookingRequest->addProperty(new Property(Request::current()->post("BookingRequestItem_AllProperties")));
            return "Success";
        });
    }

    /**
     * @throws ForceResponseException
     */
    protected function redirectAfterSave()
    {
        throw new ForceResponseException(new RedirectResponse("../" . $this->model->restModel->UniqueIdentifier . "/"));
    }

    /**
     * @return \Rhubarb\Stem\Models\Model
     * @throws \Rhubarb\Scaffolds\Authentication\LoginProviders\NotLoggedInException
     *
     * Before we save the BookingRequirement, we need to set the UserID from the current logged in user who is making
     * the request
     */
    protected function saveRestModel()
    {
        /** @var BookingRequest $bookingRequest */
        $bookingRequest = $this->model->restModel;
        $bookingRequest->UserId = SiteLogin::getLoggedInUser()->UniqueIdentifier;

        if ($bookingRequest->Duration <= 0) {
            $bookingRequest->Duration = $bookingRequest->EndTime->diff($bookingRequest->StartTime)->totalMinutes;
        }

        return parent::saveRestModel();
    }
}