<?php

namespace SpaceCadets\Florp\Website\BookingRequest;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Leaf\Controls\Common\Buttons\Button;
use Rhubarb\Leaf\Controls\Common\SelectionControls\DropDown\DropDown;
use Rhubarb\Leaf\Controls\Common\Text\NumericTextBox;
use Rhubarb\Leaf\Crud\Leaves\CrudView;
use Rhubarb\Leaf\Leaves\LeafDeploymentPackage;
use Rhubarb\Scaffolds\BackgroundTasks\Leaves\BackgroundTaskFullFocus;
use Rhubarb\Scaffolds\BackgroundTasks\Leaves\BackgroundTaskProgress;
use Rhubarb\Scaffolds\BackgroundTasks\Models\BackgroundTaskStatus;
use Rhubarb\Stem\Exceptions\RecordNotFoundException;
use Rhubarb\Stem\Filters\Equals;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Website\BookingRequest\StatusPanels\AbstractStatusPanel;
use SpaceCadets\Florp\Website\BookingRequest\StatusPanels\BookingRequestCancelledPanel;
use SpaceCadets\Florp\Website\BookingRequest\StatusPanels\EmptyStatusPanel;
use SpaceCadets\Florp\Website\BookingRequest\StatusPanels\FailedStatusPanel;
use SpaceCadets\Florp\Website\BookingRequest\StatusPanels\PendingStatusPanel;
use SpaceCadets\Florp\Website\BookingRequest\StatusPanels\SuccessPanel;
use SpaceCadets\Florp\Website\DateTimePickerControl\BootstrapDateTimePicker;

class BookingRequestItemView extends CrudView
{
    /**
     * @var BookingRequestItemModel
     */
    protected $model;
    protected $showProgressBar;

    /**
     * @throws \Rhubarb\Leaf\Exceptions\InvalidLeafModelException
     */
    protected function createSubLeaves()
    {
        parent::createSubLeaves();

        /** @var Button $save */
        $save = $this->leaves["Save"];
        /** @var Button $cancel */
        $cancel = $this->leaves["Cancel"];
        /** @var Button $delete */
        $delete = $this->leaves["Delete"];

        $this->registerSubLeaf(
            $numberOfSeats = new NumericTextBox("NumberOfSeats"),
            $duration = new NumericTextBox("Duration"),
            $progressBar = new BackgroundTaskProgress("ProgressBar"),
            $allProperties = new DropDown("AllProperties"),
            $addPropertyToBookingRequest = new Button("AddPropertyToBookingRequest", "Add", function () {
                $this->model->addPropertiesToBookingRequestButtonEvent->raise();
            }),
            new BootstrapDateTimePicker("StartTime"),
            new BootstrapDateTimePicker("EndTime"),
            $progress = new BackgroundTaskProgress("Progress")
        );

        try {
            $runningTask = BackgroundTaskStatus::findLast(new Equals("TaskStatus", "Running"));
            $progress->setBackgroundTaskStatusId($runningTask->UniqueIdentifier);
            $this->showProgressBar = true;
        } catch (RecordNotFoundException $er) {}


        $allProperties->addCssClassNames("form-control");

        $cssClassNames = "form-control";
        $numberOfSeats->addCssClassNames($cssClassNames);
        $numberOfSeats->setInputType("number");
        $duration->addCssClassNames($cssClassNames);

        $save->setButtonText("Make Request");
        $save->addCssClassNames("btn btn-success");

        $cancel->setButtonText("Back");
        $cancel->addCssClassNames("btn btn-warning");

        $delete->setButtonText("Cancel Request");
        $delete->addCssClassNames("btn btn-danger");

        $addPropertyToBookingRequest->setButtonText("Add");
        $addPropertyToBookingRequest->addCssClassNames("btn btn-warning");

        $numberOfSeats->addHtmlAttribute("required", "");
        $allProperties->setSelectionItems([$this->model->AllProperties]);
    }

////////////////////////////////////////////////////////////////////////////////
/// 1. User goes to /add/
/// 2. User enters in Start & End Times and Number of seats, then presses next
///  (Which saves the booking request giving us a booking request id)
/// 3. User Selects any extras they might want such as data projectors
/// 4. User presses button to run algorithm 
////////////////////////////////////////////////////////////////////////////////
    protected function printViewContent()
    {
        /** @var BookingRequest $bookingRequest */
        $bookingRequest = $this->model->restModel;
        $allowedToEdit = $bookingRequest->isNewRecord();
        ?>
        <link rel="stylesheet"
              href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
        <div class="container">
            <?= $this->getStatusPanel($bookingRequest) ?>
            <div class="simple-form-thin">
                <div class="cont">
                    <?= $this->printSteps();?>
                </div>
                <br>
                <h2>Booking Start Time</h2>
                <div class="container">
                    <?= $allowedToEdit ? $this->leaves["StartTime"] : $bookingRequest->StartTime->format(Booking::DateFormat) ?>
                </div>
                <br>
                <h2>Booking End Time</h2>
                <div class="container">
                    <?= $allowedToEdit ? $this->leaves["EndTime"] : $bookingRequest->EndTime->format(Booking::DateFormat) ?>
                </div>
                <h2>Number of Seats Required</h2>
                <?= $allowedToEdit ? $this->leaves["NumberOfSeats"] : $bookingRequest->NumberOfSeats ?>
                <?php
                if (!$allowedToEdit) {
                    if (!$bookingRequest->isNewRecord() && !$bookingRequest->Cancelled) {
                        ?>
                        <h2>Add a property to the request</h2>
                        <div id=addPropertyDiv class="form-horizontal">
                            <?= $this->leaves["AllProperties"] ?>
                            <?= $this->leaves["AddPropertyToBookingRequest"] ?>
                        </div>
                        <h2>Properties added:</h2>
                        <ul id="property-list">
                            <?php
                            if (!$allowedToEdit) {
                                foreach ($bookingRequest->Properties as $property) {
                                    ?>
                                    <li><?= $property->Name ?></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                        <?php
                    }
                }
                print $this->printButtonBar();
                print $this->leaves["Progress"];
                ?>
            </div>
        <?php
    }

    protected function getViewBridgeName()
    {
        return "BookingRequestItemViewViewBridge";
    }

    public function getDeploymentPackage()
    {
        return new LeafDeploymentPackage(__DIR__ . '/BookingRequestItemViewViewBridge.js');
    }

    /**
     * @param BookingRequest $bookingRequest
     * @return AbstractStatusPanel
     * @author James Anderson
     * Returns the HTML of the correct Status Panel
     */
    private function getStatusPanel(
        BookingRequest $bookingRequest
    ): AbstractStatusPanel {
//         print the empty div for new records
        if ($bookingRequest->isNewRecord()) {
            return new EmptyStatusPanel();
        }

        if($bookingRequest->Failed){
            return new FailedStatusPanel();
        }

        if($bookingRequest->Cancelled){
            return new BookingRequestCancelledPanel();
        }

        if ($bookingRequest->BookingId == "") {
            return new PendingStatusPanel();
        }
        return new SuccessPanel($bookingRequest->Booking);
    }

    protected function printButtonBar()
    {
        /** @var BookingRequest $bookingRequest */
        $bookingRequest = $this->model->restModel;
        if(!$bookingRequest->Failed) {
            // Container for buttons
            print '<div class="button-bar">';
            // Decision of whether to print a save button to get us a new record, or an ajax button to run our algorithm
            if ($bookingRequest->isNewRecord()) {
                print $this->leaves["Save"];
            } else {
                if ($bookingRequest->BookingId > 0 || $bookingRequest->Cancelled) {
                    // Don't let them save at all!
                } else {
                    print '<a href="#" id="CheckForBookings" class="btn btn-success">Update</a>';
                    print $this->leaves["Delete"];
                }
            }
            print '<a href="../" class="btn btn-warning">Back</a>';
            print '</div>';
        }
    }

    protected function printSteps()
    {
        if ($this->model->restModel->isNewRecord()) {
            print <<<HTML
<div class="form-step-list">
    <ol>
        <li class="current">Enter Basic Details for your request ⬅️</li>
        <li class="">Add properties that you need then check for availability by clicking update</li>
    </ol>
</div>
HTML;
        } elseif ($this->model->restModel->BookingId == null) {
            print <<<HTML
<div class="form-step-list">
    <ol>
        <li class="current">Enter Basic Details for your request ✅</li>
        <li class="">Add properties that you need then check for availability by clicking update ⬅️</li>
    </ol>
</div>
HTML;
        }
    }
}