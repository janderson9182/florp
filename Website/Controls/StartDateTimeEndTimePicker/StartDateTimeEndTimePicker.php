<?php

namespace SpaceCadets\Florp\Website\Controls\StartDateTimeEndTimePicker;

use Rhubarb\Leaf\Leaves\Controls\Control;

class StartDateTimeEndTimePicker extends Control
{
    protected function getViewClass()
    {
        return StartDateTimeEndTimePickerView::class;
    }
}
