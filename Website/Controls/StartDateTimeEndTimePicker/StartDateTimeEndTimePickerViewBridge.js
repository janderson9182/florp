var bridge = function (leafPath) {
    window.rhubarb.viewBridgeClasses.ViewBridge.apply(this, arguments);
};

bridge.prototype = new window.rhubarb.viewBridgeClasses.ViewBridge()
bridge.prototype.constructor = bridge

bridge.prototype.attachEvents = function () {

}

bridge.prototype.setValue = function (value) {
    window.rhubarb.viewBridgeClasses.StartDateTimeEndTimePickerViewBridge.prototype.setValue.call(this, value)
}

bridge.prototype.getValue = function () {
    return 1;
}
window.rhubarb.viewBridgeClasses.StartDateTimeEndTimePickerViewBridge = bridge
