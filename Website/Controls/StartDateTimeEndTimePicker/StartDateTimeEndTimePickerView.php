<?php

namespace SpaceCadets\Florp\Website\Controls\StartDateTimeEndTimePicker;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Crown\Request\WebRequest;
use Rhubarb\Leaf\Controls\Common\DateTime\DateTime;
use Rhubarb\Leaf\Controls\Common\DateTime\DateTimeView;
use Rhubarb\Leaf\Controls\Common\DateTime\DateView;
use Rhubarb\Leaf\Leaves\Controls\ControlView;
use Rhubarb\Leaf\Leaves\LeafDeploymentPackage;

class StartDateTimeEndTimePickerView extends ControlView
{
    public function getDeploymentPackage()
    {
        return new LeafDeploymentPackage(__DIR__ . '/' . $this->getViewBridgeName() . '.js');
    }

    protected function getViewBridgeName()
    {
        return 'StartDateTimeEndTimePickerViewBridge';
    }

    protected function printViewContent()
    {
        ?>
        <div class="container">
            <div class='col-md-5'>
                <div class="form-group">
                    <div class='input-group date' id="datetimepickerStartTime">
                        <input type='text' class="form-control" id="StartTime"/>
                        <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class='col-sm-6'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepickerEndTime'>
                                <input type='text' class="form-control" id='EndTime'/>
                                <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepickerEndTime').datetimepicker({
                                format: 'LT'
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
        <?php
    }

    protected function getValueClass()
    {
        return RhubarbDateTime::class;
    }

    protected function createTimeStringFromRequest(WebRequest $request)
    {
        parent::parseRequest($request);
    }
}
