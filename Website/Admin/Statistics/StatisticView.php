<?php

namespace SpaceCadets\Florp\Website\Admin\Statistics;

use Rhubarb\Leaf\Controls\Common\Buttons\Button;
use Rhubarb\Leaf\Views\View;
use SpaceCadets\Florp\Services\Statistic\BookingCountAbstractStatistic;
use SpaceCadets\Florp\Services\Statistic\BookingsForTodayStatistic;
use SpaceCadets\Florp\Services\Statistic\BusiestRoomStatistic;
use SpaceCadets\Florp\Services\Statistic\FirstBookingTodayStatistic;
use SpaceCadets\Florp\Services\Statistic\GraphOfBookingsStatistic;
use SpaceCadets\Florp\Services\Statistic\HowManyCheckInsMissedTodayStatistic;
use SpaceCadets\Florp\Services\Statistic\LastBookingTodayStatistic;
use SpaceCadets\Florp\Services\Statistic\PercentageOfCheckInsMissedInTotalStatistic;
use SpaceCadets\Florp\Services\Statistic\RoomsCurrentlyAvailableStatistic;
use SpaceCadets\Florp\Services\Statistic\RoomsCurrentlyBookedStatistic;
use SpaceCadets\Florp\Services\Statistic\UserTable;
use SpaceCadets\Florp\Services\Statistic\WhichUserIsInEachRoom;

class StatisticView extends View
{
    /**
     * @var StatisticModel
     */
    protected $model;

    protected function createSubLeaves()
    {
        parent::createSubLeaves();
        $this->registerSubLeaf(
            $DownloadBookingCsvEvent = new Button("DownloadBookingCsvEvent", "Download Bookings", function () {
                $this->model->DownloadBookingCsvEvent->raise();
            })
        );

        $DownloadBookingCsvEvent->addCssClassNames("btn", "btn-primary");
    }


    protected function printViewContent()
    {
        ?>
        <div class="container flex-column statistic statistic-item">
            <div class="flex-item">
            <table>
                <tr>
                    <td><div1><h2>Number Of Bookings In Total:</h2></div></td>
        <td><div2> <?= new BookingCountAbstractStatistic(); ?></div2></td>
        </tr>
        </table>

        </div>
        <div class="flex-item">
            <table>
                <tr>
                    <td><div1><h2>Number Of Bookings Today:</h2></div></td>
        <td><div2><?= new BookingsForTodayStatistic(); ?></div2></td>
        </tr>
        </table>
        </div>

        <div class="flex-item">
            <table>
                <tr>
                    <td><div1> <h2>Number of Bookings Missed Today:</h2></div></td>
        <td><div2><?= new HowManyCheckInsMissedTodayStatistic(); ?></div2></td>
        </tr>
        </table>
        </div>
        <div class="flex-item">
            <table>
                <tr>
                    <td><div1> <h2>First Booking Today Starts at:</h2></div></td>
        <td><div2><?= new FirstBookingTodayStatistic(); ?></div2></td>
        </tr>
        </table>
        </div>
        <div class="flex-item">
            <table>
                <tr>
                    <td><div1> <h2>Last Booking Today Finishes at:</h2></div></td>
        <td><div2><?= new LastBookingTodayStatistic(); ?></div2></td>
        </tr>
        </table>
        </div>
        <div class="flex-item scrollable-vertical">
            <h2>Rooms Currently Checked In:</h2>
            <?= new RoomsCurrentlyBookedStatistic(); ?>
        </div>
        <div class="flex-item scrollable-vertical">
            <h2>Rooms Currently Available:</h2>
            <?= new RoomsCurrentlyAvailableStatistic(); ?>
        </div>
        <div class="flex-item">
            <table>
                <tr>
                    <td><div1> <h2>Percentage of Bookings Missed in Total:</h2></div></td>
        <td><div2><?= new PercentageOfCheckInsMissedInTotalStatistic(); ?></div2></td>
        </tr>
        </table>
        </div>

        <div class="flex-item">
            <h2>Download Spreadsheet of all Bookings for Today</h2>
            <?= $this->leaves["DownloadBookingCsvEvent"] ?>
        </div>
        <div id="graphs"></div>
        <ul class="nav nav-pills statistic-button">
            <li><a href="?graphBooking#graphs">Bookings For Today</a></li>
            <li><a href="?pieChart#graphs">Busiest Room</a></li>
            <!--            <li><a href="?userTable#graphs">User Table</a></li> -->
        </ul>
        </div>

        <?php
        if (isset($_GET['graphBooking'])) {
            ?>
            <div class="adminCardContainerPadding">
            <?php
            print new GraphOfBookingsStatistic();
        }
        if (isset($_GET['pieChart'])) {
            print new BusiestRoomStatistic();
        }
        if (isset($_GET['userTable'])) {
            print new UserTable();
        }

    }
}