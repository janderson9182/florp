<?php

namespace SpaceCadets\Florp\Website\Admin\Statistics;

use Rhubarb\Leaf\Leaves\Leaf;
use SpaceCadets\Florp\Services\Statistic\DownloadLockedBookingsForToday;
use SpaceCadets\Florp\Website\Admin\AdminLeaf;

class Statistic extends AdminLeaf
{
    /**
    * @var StatisticModel
    */
    protected $model;

    protected function getViewClass()
    {
        return StatisticView::class;
    }

    protected function createModel()
    {
        $model = new StatisticModel();

        // If your model has events you want to listen to you should attach the handlers here
        // e.g.
        // $model->selectedUserChangedEvent->attachListener(function(){ ... });

        return $model;
    }

    protected function onModelCreated()
    {
        parent::onModelCreated();

        $this->model->DownloadBookingCsvEvent->attachHandler(
            function(){
                DownloadLockedBookingsForToday::create()->execute();
            }
        );
    }


}