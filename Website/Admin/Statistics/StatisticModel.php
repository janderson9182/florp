<?php

namespace SpaceCadets\Florp\Website\Admin\Statistics;

use Rhubarb\Crown\Events\Event;
use Rhubarb\Leaf\Leaves\LeafModel;

class StatisticModel extends LeafModel
{
    /**
     * @var Event
     */
    public $DownloadBookingCsvEvent;

    public function __construct()
    {
        parent::__construct();
        $this->DownloadBookingCsvEvent = new Event();
    }
}