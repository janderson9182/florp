<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 06/03/18
 * Time: 20:01
 */

namespace SpaceCadets\Florp\Website\Admin;

use Rhubarb\Leaf\Crud\Leaves\CrudLeaf;
use SpaceCadets\Florp\Services\Website\Admin\AdminLeafProtectionHandlerUseCase;

abstract class AdminCrudLeaf extends CrudLeaf
{
    public function __construct($modelOrCollection = null)
    {
        parent::__construct($modelOrCollection);
        AdminLeafProtectionHandlerUseCase::create()->execute();
    }

}