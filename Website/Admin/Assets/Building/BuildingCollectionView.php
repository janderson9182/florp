<?php

namespace SpaceCadets\Florp\Website\Admin\Assets\Building;

use Rhubarb\Leaf\Crud\Leaves\CrudModel;
use Rhubarb\Leaf\Crud\Leaves\CrudView;
use Rhubarb\Leaf\Table\Leaves\Table;
use SpaceCadets\Florp\Models\Assets\Building;
use SpaceCadets\Florp\Routes;

class BuildingCollectionView extends CrudView
{
    /**
     * @var CrudModel
     */
    protected $model;

    protected function createSubLeaves()
    {
        parent::createSubLeaves();
        $this->registerSubLeaf(
          $table = new Table(Building::all())
        );
        $table->columns=[
            "Name"=>'<a href="{Id}/">{Name} </a>'
        ];
    }

    protected function printViewContent()
    {
        ?>
        <div class="adminTitlePadding">
            <h2 class="adminH2">Here, you may access all current buildings and add new buildings.</h2>
        </div>
        <div class="adminCardContainerMargin">
            <div class="adminCardContainerMain">
                <div class="adminCardContainerPadding">
                    <div class="adminCardContainerTitleDiv">
                        <h3 class="adminH3"><?=$this->getTitle()?></h3>
                    </div>
                    <div class="adminCardContainerItemMargin">
                        <a href="add/" class="btn btn-primary">Add New Building</a>
                    </div>
                    <div class="adminCardContainerItemMargin">
                        <?php
                        print $this->leaves["Table"];
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

}