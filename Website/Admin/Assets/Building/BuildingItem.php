<?php

namespace SpaceCadets\Florp\Website\Admin\Assets\Building;

use Rhubarb\Leaf\Crud\Leaves\CrudModel;
use SpaceCadets\Florp\Website\Admin\AdminCrudLeaf;

class BuildingItem extends AdminCrudLeaf
{
    /**
     * @var CrudModel
     */
    protected $model;

    /**
     * Returns the name of the standard view used for this leaf.
     *
     * @return string
     */
    protected function getViewClass()
    {
        return BuildingItemView::class;
    }
}