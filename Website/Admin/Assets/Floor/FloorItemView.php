<?php

namespace SpaceCadets\Florp\Website\Admin\Assets\Floor;

use Rhubarb\Leaf\Crud\Leaves\CrudModel;
use Rhubarb\Leaf\Crud\Leaves\CrudView;
use SpaceCadets\Florp\Models\Assets\Floor;

class FloorItemView extends CrudView
{
    /**
     * @var CrudModel
     */
    protected $model;

    protected function createSubLeaves()
    {
        parent::createSubLeaves();
        $this->registerSubLeaf(
            "Name",
            "FloorNumber",
            "BuildingId"
        );
        $buttonClass = "btn";
        $this->leaves["Save"]->addCssClassNames($buttonClass);
        $this->leaves["Cancel"]->addCssClassNames($buttonClass);
        $this->leaves["Delete"]->addCssClassNames($buttonClass);

    }

    protected function printViewContent()
    {
        /** @var Floor $floor */
        $floor = $this->model->restModel;
        ?>
        <div class="adminDividerLeft">
            <div class="adminCardContainerMargin">
                <div class="adminCardContainerWithinHalf">
                    <div class="adminCardContainerPadding">
                        <?php
                        $this->layoutItemsWithContainer(
                            $this->getTitle(),
                            [
                                "Name",
                                "FloorNumber",
                                "BuildingId"
                            ]
                        );
                        ?>
                        <div class="adminCardContainerPadding">
                            <?php

                            print $this->leaves["Save"];
                            print $this->leaves["Cancel"];
                            print $this->leaves["Delete"];

                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if ($floor->isNewRecord() == false) {
            ?>
            <div  class="adminDividerRight">
                <div class="adminCardContainerMargin">
                    <div class="adminCardContainerWithinHalf">
                        <h2 class="adminH2">Rooms on this floor:</h2>
                    <?php
                    foreach ($floor->Rooms as $room) {
                        ?>
                        <h3><?= $room->Name ?></h3>
                        <p><?= $room->Description ?></p>
                        <hr>
                        <?php
                    }
                }
                ?>
                    </div>
                </div>
        </div>

        <br>
        <?php
    }
}