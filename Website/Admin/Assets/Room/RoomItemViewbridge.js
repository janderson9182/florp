var bridge = function (leafPath) {
    window.rhubarb.viewBridgeClasses.ViewBridge.apply(this, arguments);
};

bridge.prototype = new window.rhubarb.viewBridgeClasses.ViewBridge();

bridge.prototype.attachEvents = function () {
    var self = this;
    var reportedNotFaultyHtmlCollection = document.getElementsByClassName("js-markfixed");
    var faultyPropertyHtmlElements = [].slice.call(reportedNotFaultyHtmlCollection);
    faultyPropertyHtmlElements.forEach(function (currentProperty) {
        currentProperty.onclick = function (event) {
            var successfulReport = function () {
                alert("successfully reported as fixed");
                location.reload()
            };
            event.preventDefault();
            event.stopPropagation();
            self.raiseServerEvent(
                "PropertyFixed",
                this.attributes.rpid.value,
                successfulReport()
            );
        }
    });
};

bridge.prototype.constructor = bridge;

window.rhubarb.viewBridgeClasses.RoomItemViewBridge = bridge;
