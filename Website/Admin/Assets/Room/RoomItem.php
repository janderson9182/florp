<?php

namespace SpaceCadets\Florp\Website\Admin\Assets\Room;

use Rhubarb\Crown\Request\Request;
use Rhubarb\Leaf\Controls\Common\FileUpload\UploadedFileDetails;
use SpaceCadets\Florp\Models\Assets\Properties\Property;
use SpaceCadets\Florp\Models\Assets\Properties\RoomImage;
use SpaceCadets\Florp\Models\Assets\Properties\RoomProperty;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Website\Admin\AdminCrudLeaf;

class RoomItem extends AdminCrudLeaf
{
    /**
     * @var RoomItemModel
     */
    protected $model;

    /**
     * Returns the name of the standard view used for this leaf.
     *
     * @return string
     */
    protected function getViewClass()
    {
        return RoomItemView::class;
    }

    protected function createModel()
    {
        return new RoomItemModel();
    }

    protected function onModelCreated()
    {
        $this->model->AllProperties = Property::all();
        $this->model->addPropertiesToRoomButtonEvent->attachHandler(function () {
            /** @var Room $room */
            $room = $this->model->restModel;
            $room->addProperty(new Property(Request::current()->post("RoomItem_AllProperties")));
            return "Success";
        });

        $this->model->UploadImageEvent->attachHandler(function (UploadedFileDetails $details) {

            $imageRootDirectory =  "/static/data/images/";

            // Store image in database
            $roomImage = new RoomImage();
            $roomImage->ImageName = $details->originalFilename;
            $roomImage->RoomId = $this->model->restModel->UniqueIdentifier;
            $roomImage->save();

            // Create directory to save the image
            $pathName = $imageRootDirectory . $this->model->restModel->UniqueIdentifier . "/";
            if (!is_dir(APPLICATION_ROOT_DIR . $pathName)) {
                mkdir(APPLICATION_ROOT_DIR . $pathName, 777, true);
            }
            // Store the location of the image in the database
            $roomImage->FilePath = $pathName . $details->originalFilename;
            $roomImage->save();

            $didItWork = file_put_contents(APPLICATION_ROOT_DIR . $roomImage->FilePath, file_get_contents($details->tempFilename));
        });

        $this->model->deleteImageEvent->attachHandler(function($index){
            $imageToBeDeleted = new RoomImage($index);
            $imageToBeDeleted->delete();
        });

        $this->model->PropertyFixedEvent->attachHandler(
            function ($roomPropertyId) {
                $roomProperty = new RoomProperty($roomPropertyId);
                $roomProperty->Faulty = false;
                $roomProperty->save();
                return true;
            }
        );
        parent::onModelCreated();
    }
}