<?php

namespace SpaceCadets\Florp\Website\Admin\Assets\Room;

use Rhubarb\Crown\Events\Event;
use Rhubarb\Leaf\Crud\Leaves\CrudModel;
use Rhubarb\Stem\Collections\Collection;

class RoomItemModel extends CrudModel
{
    /**
     * @var Collection $AllProperties
     */
    public $AllProperties;
    /**
     * @var Event $addPropertiesToRoomButtonEvent
     */
    public $addPropertiesToRoomButtonEvent;
    public $UploadImageEvent;

    public $deleteImageEvent;

    /**
     * @var Event $PropertyFixedEvent
     */
    public $PropertyFixedEvent;

    public function __construct()
    {
        parent::__construct();
        $this->addPropertiesToRoomButtonEvent = new Event();
        $this->UploadImageEvent = new Event();
        $this->deleteImageEvent = new Event();
        $this->PropertyFixedEvent = new Event();
    }
}