<?php

namespace SpaceCadets\Florp\Website\Admin\Assets\Room;

use Rhubarb\Leaf\Controls\Common\Buttons\Button;
use Rhubarb\Leaf\Controls\Common\FileUpload\UploadedFileDetails;
use Rhubarb\Leaf\Controls\Common\SelectionControls\DropDown\DropDown;
use Rhubarb\Leaf\Controls\Html5Upload\Html5FileUpload;
use Rhubarb\Leaf\Crud\Leaves\CrudView;
use SpaceCadets\Florp\Models\Assets\Room;
use Rhubarb\Leaf\Leaves\LeafDeploymentPackage;

class RoomItemView extends CrudView
{
    /**
     * @var RoomItemModel
     */
    protected $model;

    protected function createSubLeaves()
    {
        parent::createSubLeaves();
        $this->registerSubLeaf(
            "Name",
            "Description",
            "FloorId",
            "NumberOfSeats",
            $allProperties = new DropDown("AllProperties"),
            $addPropertyToRoom = new Button("AddPropertyToRoom", "Add Property", function (){
                $this->model->addPropertiesToRoomButtonEvent->raise();
            })
        ,$diagram = new  Html5FileUpload("UploadImageEvent"),
            new Button("DeleteImage", "Delete Image", function($index){
                $this->model->deleteImageEvent->raise($index);
            })
        );
        $buttonClass = "btn";
        $this->leaves["Save"]->addCssClassNames($buttonClass);
        $this->leaves["Cancel"]->addCssClassNames($buttonClass);
        $this->leaves["Delete"]->addCssClassNames($buttonClass);
        $this->leaves["DeleteImage"]->addCssClassNames($buttonClass);
        $this->leaves["AddPropertyToRoom"]->addCssClassNames($buttonClass);

        $diagram->fileUploadedEvent->attachHandler(function (UploadedFileDetails $content) {
            $this->model->UploadImageEvent->raise($content);
        });

        $allProperties->setSelectionItems([$this->model->AllProperties]);
    }

    protected function printViewContent()
    {
        /** @var Room $room */
        $room = $this->model->restModel;
        ?>
        <div class="adminDividerLeft">
            <div class="adminCardContainerMargin">
                <div class="adminCardContainerWithinHalf">
                    <div class="adminCardContainerPadding">
                        <?php
                        $this->layoutItemsWithContainer(
                            $this->getTitle(),
                            [
                                "Name",
                                "Description",
                                "NumberOfSeats",
                                "FloorId"
                            ]
                        );
                        ?>
                        <div class="adminCardContainerPadding">
                            <?php

                            print $this->leaves["Save"];
                            print $this->leaves["Cancel"];
                            print $this->leaves["Delete"];

                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>

        <?php

        if (!$room->isNewRecord()) {
            /** @var DropDown $allProperties */
            ?>
            <div  class="adminDividerRight">
                <div class="adminCardContainerMargin">
                    <div class="adminCardContainerWithinHalf">
                        <h3>Properties of this room:</h3>
                        <ul id="property-list">
                <?php
                foreach ($room->PropertiesRaw as $property) {
                    ?>
                    <li>
                        <?= $property->Property->Name ?>
                        <?php
                        if ($property->Faulty) {
                            ?>
                            <p>Property has been marked faulty!</p>
                            <a href="#" class="js-markfixed" rpid="<?= $property->Id ?>">Mark as fixed</a>
                            <?php
                        }
                        ?>
                    </li>
                    <?php
                }
                ?>
            </ul>
            <h3>Add new property to this room</h3>
        <?= $this->leaves["AllProperties"] ?>
        <?= $this->leaves["AddPropertyToRoom"] ?>
            <h3>Images</h3>
            <?php

            $images = $room->Images;
            foreach ($images as $image) {
                ?>
                <img src="<?= $image->FilePath?>" class="full-width">
                <?php
                $this->leaves['DeleteImage']->printWithIndex($image->Id);
            }
            ?>
            <h3>Upload a new image</h3>
            <?= $this->leaves["UploadImageEvent"] ?>
                    </div>
                </div>
            </div>
            <?php
        }
    }


    protected function getViewBridgeName()
    {
        return "RoomItemViewBridge";
    }

    public function getDeploymentPackage()
    {
        return new LeafDeploymentPackage(__DIR__ . "/RoomItemViewbridge.js");
    }
}