<?php

namespace SpaceCadets\Florp\Website\Admin\Assets\Properties;

use Rhubarb\Leaf\Crud\Leaves\CrudModel;
use SpaceCadets\Florp\Website\Admin\AdminCrudLeaf;

class PropertiesCollection extends AdminCrudLeaf
{
    /**
     * @var CrudModel
     */
    protected $model;

    /**
     * Returns the name of the standard view used for this leaf.
     *
     * @return string
     */
    protected function getViewClass()
    {
        return PropertiesCollectionView::class;
    }
}