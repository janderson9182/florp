<?php

namespace SpaceCadets\Florp\Website\Admin\Assets\Properties;

use Rhubarb\Leaf\Crud\Leaves\CrudModel;
use Rhubarb\Leaf\Crud\Leaves\CrudView;

class PropertiesItemView extends CrudView
{
    /**
     * @var CrudModel
     */
    protected $model;

    protected function createSubLeaves()
    {
        parent::createSubLeaves();
        $this->registerSubLeaf(
            "Name",
            "Description"
        );
        $buttonClass = "btn";
        $this->leaves["Save"]->addCssClassNames($buttonClass);
        $this->leaves["Cancel"]->addCssClassNames($buttonClass);
        $this->leaves["Delete"]->addCssClassNames($buttonClass);
    }

    protected function printViewContent()
    {
        ?>
        <div class="adminCardContainerMargin">
            <div class="adminCardContainerThin">
                <div class="adminCardContainerPadding">
                    <?php
                    $this->layoutItemsWithContainer(
                        $this->getTitle(),
                        [
                            "Name",
                            "Description"
                        ]
                    );
                    ?>
                    <div class="adminCardContainerPadding">
                        <?php

                        print $this->leaves["Save"];
                        print $this->leaves["Cancel"];
                        print $this->leaves["Delete"];

                        ?>
                    </div>
                </div>
            </div>
        </div>

        <br>

        <?php
    }

}