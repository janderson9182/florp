<?php
namespace SpaceCadets\Florp\Website\Admin\Users;

use Rhubarb\Leaf\Controls\Common\Buttons\Button;
use Rhubarb\Leaf\Controls\Common\Text\TextBox;
use Rhubarb\Leaf\Crud\Leaves\CrudModel;
use Rhubarb\Leaf\Crud\Leaves\CrudView;
use SpaceCadets\Florp\Models\FlorpUser;

class UsersItemView extends CrudView
{
    /**
     * @var UsersItemModel
     */
    protected $model;

    /**
     * @throws \Rhubarb\Leaf\Exceptions\InvalidLeafModelException
     */
    protected function createSubLeaves()
    {
        parent::createSubLeaves();
        $this ->registerSubLeaf(
            "Forename",
            "Surname",
            "Username",
            "Email",
            $newPassword = new TextBox("NewPassword"),
            $promoteToAdmin = new Button("PromoteToAdminEvent", "Promote To Admin", function()
            {
                $this->model->PromoteToAdminEvent->raise();
            })
        );
        $buttonClass = "btn";
        $this->leaves["Save"]->addCssClassNames($buttonClass);
        $this->leaves["Cancel"]->addCssClassNames($buttonClass);
        $this->leaves["Delete"]->addCssClassNames($buttonClass);
        $promoteToAdmin->addCssClassNames($buttonClass);

        $newPassword->setInputType('password');
    }

    protected function printViewContent()
    {
        ?>
        <div class="adminCardContainerMargin">
            <div class="adminCardContainerThin">
                <div class="adminCardContainerPadding">
                    <?php
                    $this->layoutItemsWithContainer(
                        $this->getTitle(),
                        [
                            "Forename",
                            "Surname",
                            "Username",
                            "Email",
                            "NewPassword"
                        ]
                    );
                    /** @var FlorpUser $user */
                    $user = $this->model->restModel;
                    if(!$user->isNewRecord()){
                        $adminSection = [
                          "User Is An Admin?" => $user->hasRole("admin") ? "Yes" : "No"
                        ];
                        if(!$user->hasRole("admin")){
                            $adminSection["Promote To Admin"] = "PromoteToAdminEvent";
                        }
                        $this->layoutItemsWithContainer("Admin Section", $adminSection);
                    }
                    ?>
                    <div class="adminCardContainerPadding">
                        <?php

                        print $this->leaves["Save"];
                        print $this->leaves["Cancel"];
                        print $this->leaves["Delete"];

                        ?>
                    </div>
                </div>
            </div>
        </div>

        <br>

        <?php
    }


}