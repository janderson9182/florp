<?php
namespace SpaceCadets\Florp\Website\Admin\Users;

use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\Response\RedirectResponse;
use Rhubarb\Scaffolds\AuthenticationWithRoles\Role;
use Rhubarb\Scaffolds\AuthenticationWithRoles\UserRole;
use Rhubarb\Stem\Filters\Equals;
use SpaceCadets\Florp\Models\FlorpUser;
use SpaceCadets\Florp\Website\Admin\AdminCrudLeaf;

class UsersItem extends AdminCrudLeaf
{
    /**
     * @var UsersItemModel
     */
    protected $model;

    /**
     * Returns the name of the standard view used for this leaf.
     *
     * @return string
     */
    protected function getViewClass()
    {
        return UsersItemView::class;
    }

    protected function saveRestModel()
    {
        /** @var FlorpUser $user */
        $user = parent::saveRestModel();
        if ($this->model->NewPassword)
        {
            $user->setNewPassword($this->model->NewPassword);
            $user->save();
        }
        return $user;
    }

    protected function createModel()
    {
        return new UsersItemModel();
    }

    protected function onModelCreated()
    {
        parent::onModelCreated();

        $this->model->PromoteToAdminEvent->attachHandler(
            function () {
                /** @var FlorpUser $user */
                $user = $this->model->restModel;
                if (!$user->hasRole("admin")) {
                    $role = Role::findFirst(new Equals("RoleName", "admin"));
                    $userRole = new UserRole();
                    $userRole->UserID = $user->UserID;
                    $userRole->RoleID = $role->RoleID;
                    $userRole->save();
                }
                throw new ForceResponseException(new RedirectResponse("."));
            }
        );
    }
}