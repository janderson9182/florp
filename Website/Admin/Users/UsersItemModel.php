<?php
/**
 * Created by PhpStorm.
 * User: ron_0
 * Date: 08/12/2017
 * Time: 15:12
 */

namespace SpaceCadets\Florp\Website\Admin\Users;


use Rhubarb\Crown\Events\Event;
use Rhubarb\Leaf\Crud\Leaves\CrudModel;

class UsersItemModel extends CrudModel
{

    public $NewPassword;
    public $PromoteToAdminEvent;

    public function __construct()
    {
        parent::__construct();
        $this->PromoteToAdminEvent = new Event();
    }
}