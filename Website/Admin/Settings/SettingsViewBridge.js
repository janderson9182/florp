var bridge = function (leafPath) {
    window.rhubarb.viewBridgeClasses.ViewBridge.apply(this, arguments);
};

bridge.prototype = new window.rhubarb.viewBridgeClasses.ViewBridge();

bridge.prototype.attachEvents = function () {
    var self = this;
    // Only enable the save button if something has changed
    self.findChildViewBridge("AlgorithmSelection").attachClientEventHandler("ValueChanged", function () {
        self.findChildViewBridge("Save").viewNode.disabled = false;
    })
};

bridge.prototype.constructor = bridge;

window.rhubarb.viewBridgeClasses.SettingsViewBridge = bridge;
