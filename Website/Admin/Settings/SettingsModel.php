<?php

namespace SpaceCadets\Florp\Website\Admin\Settings;

use Rhubarb\Crown\Events\Event;
use Rhubarb\Leaf\Leaves\LeafModel;

class SettingsModel extends LeafModel
{
    /**
     * @var Event $Save
     */
    public $Save;

    public $AlgorithmSelection;

    public $AdminEmailAddress = "";

    public function __construct()
    {
        parent::__construct();
        $this->Save = new Event();
    }
}