<?php

namespace SpaceCadets\Florp\Website\Admin\Settings;

use Rhubarb\Leaf\Controls\Common\Buttons\Button;
use Rhubarb\Leaf\Controls\Common\SelectionControls\DropDown\DropDown;
use Rhubarb\Leaf\Controls\Common\Text\TextBox;
use Rhubarb\Leaf\Leaves\LeafDeploymentPackage;
use Rhubarb\Leaf\Views\View;
use SpaceCadets\Florp\FlorpSettings;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingService;

class SettingsView extends View
{
    /**
     * @var SettingsModel
     */
    protected $model;

    /**
     * @throws \Rhubarb\Leaf\Exceptions\InvalidLeafModelException
     */
    protected function createSubLeaves()
    {
        $this->registerSubLeaf(
            $algorithm = new DropDown("AlgorithmSelection"),
            $save = new Button("Save", "Save Settings", function () {
                $this->model->Save->raise();
            }),
            $adminEmailAddress = new TextBox("AdminEmailAddress")
        );

        $selectionItems = [];
        foreach (RoomBookingService::getRegisteredClasses() as $shortName => $class) {
            $selectionItems[] = $shortName;
        }
        $algorithm->setSelectionItems($selectionItems);
        $algorithm->setPlaceholderText("Please select an algorithm");
        $florpSettings = FlorpSettings::singleton();
        $algorithm->setSelectedItems($florpSettings->roomBookingProviderClass);

        $save->addCssClassNames("btn btn-success");

        $adminEmailAddress->setInputType("email");
    }

    protected function printViewContent()
    {
        $currentClass = FlorpSettings::singleton()->roomBookingProviderClass;
        $noneSelected = "None Selected.";
        $currentClassName = $currentClass != null ? $currentClass::getName() : $noneSelected;

        ?>
        <div class="adminTitlePadding">
            <h2 class="adminH2">Here, you may view all current settings for your system.</h2>
            <h2 class="adminH2">The room sorting algorithm that you have currently selected
                is: <u><b><?= $currentClassName ?></b></u></h2>
        </div>
        <div class="adminCardContainerMargin">
            <div class="adminCardContainerMain">
                <div class="adminCardContainerPadding">
                    <div class="adminCardContainerItemMargin">
                        <?php
                        $this->layoutItemsWithContainer(
                            "Settings",
                            [
                                "AlgorithmSelection",
                                "AdminEmailAddress",
                                "Save"
                            ]
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php


    }

    protected function getViewBridgeName()
    {
        return "SettingsViewBridge";
    }

    public function getDeploymentPackage()
    {
        return new LeafDeploymentPackage(__DIR__ . "/SettingsViewBridge.js");
    }
}