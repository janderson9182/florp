<?php

namespace SpaceCadets\Florp\Website\Admin\Settings;

use SpaceCadets\Florp\FlorpSettings;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingService;
use SpaceCadets\Florp\Website\Admin\AdminLeaf;

class Settings extends AdminLeaf
{
    /**
     * @var SettingsModel
     */
    protected $model;

    protected function getViewClass()
    {
        return SettingsView::class;
    }

    protected function createModel()
    {
        return new SettingsModel();
    }

    protected function onModelCreated()
    {
        parent::onModelCreated();
        $florpSettings = FlorpSettings::singleton();
        $this->model->AlgorithmSelection = $florpSettings->roomBookingProviderClass;
        $this->model->AdminEmailAddress = $florpSettings->adminEmailAddress;
        $this->model->Save->attachHandler(function () {
            $selection = $this->model->AlgorithmSelection;
            $florpSettings = FlorpSettings::singleton();
            $florpSettings->roomBookingProviderClass = RoomBookingService::findClassByName($selection);
            $florpSettings->adminEmailAddress = $this->model->AdminEmailAddress;
        });
    }
}