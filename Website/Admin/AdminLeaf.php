<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 06/03/18
 * Time: 20:21
 */

namespace SpaceCadets\Florp\Website\Admin;

use Rhubarb\Leaf\Leaves\Leaf;
use SpaceCadets\Florp\Services\Website\Admin\AdminLeafProtectionHandlerUseCase;

abstract class AdminLeaf extends Leaf
{
    public function __construct(?string $name = null, ?callable $initialiseModelBeforeView = null)
    {
        parent::__construct($name, $initialiseModelBeforeView);
        AdminLeafProtectionHandlerUseCase::create()->execute();
    }

}