<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 27/02/18
 * Time: 15:29
 */

namespace SpaceCadets\Florp\Website\DateTimePickerControl;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Leaf\Leaves\Controls\ControlView;

class BootstrapDateTimePickerView extends ControlView
{
    protected function printViewContent()
    {
        parent::printViewContent();
        ?>
        <div class="row">
            <div class="form-group">
                <div class='input-group date' id='<?= $this->model->leafPath ?>'>
                    <input type='text' class="form-control" name="<?= $this->model->leafPath ?>"
                           id="<?= $this->model->leafPath ?>"
                           value="<?= $this->model->value->format("m/d/Y H:i:s") ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <script type="text/javascript">
                $(function () {
                    var <?=$this->model->leafPath?> =
                    $('#<?=$this->model->leafPath?>').datetimepicker();
                    <?=$this->model->leafPath?>.
                    val()
                    var datePicker = <?=$this->model->leafPath?>.
                    data("DateTimePicker");
                    <?php
                    if($this->model->value == new RhubarbDateTime())
                    {
                    ?>
                    var today = new Date();
                    var tomorrow = new Date(today);
                    tomorrow.setDate(today.getDate() + 1);
                    datePicker.options({
                        defaultDate: tomorrow,
                        minDate: tomorrow
                    })
                    <?php

                    }
                    ?>
                });
            </script>
        </div>
        <?php

    }
}