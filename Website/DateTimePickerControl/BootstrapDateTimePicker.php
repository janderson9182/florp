<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 27/02/18
 * Time: 15:27
 */
namespace SpaceCadets\Florp\Website\DateTimePickerControl;
use Rhubarb\Leaf\Leaves\Controls\Control;
use SpaceCadets\Florp\Website\BookingRequest\BookingRequestItemModel;

class BootstrapDateTimePicker extends Control
{
    protected function createModel()
    {
        return new BootstrapDateTimePickerModel();
    }

    protected function getViewClass()
    {
        return BootstrapDateTimePickerView::class;
    }


}