<?php

namespace SpaceCadets\Florp\Website\Layouts;

use Rhubarb\Crown\Html\ResourceLoader;
use Rhubarb\Crown\Layout\LayoutModule;
use Rhubarb\Crown\LoginProviders\Exceptions\NotLoggedInException;
use Rhubarb\Crown\Settings\HtmlPageSettings;
use Rhubarb\Patterns\Layouts\BaseLayout;
use SpaceCadets\Florp\Models\FlorpUser;
use SpaceCadets\Florp\Routes;
use SpaceCadets\Florp\Website\Login\FlorpLogin;
use SpaceCadets\Florp\Website\Login\SiteLogin;

class DefaultLayout extends BaseLayout
{
    protected function printPageHeading()
    {
        try {
            /**
             * @var $currentUser FlorpUser
             */
            $currentUser = SiteLogin::getLoggedInUser();
            $baseLogoUrl = "https://s3.us-east-2.amazonaws.com/florpdata";
            $parts = explode("@", $currentUser->Email);
            $class = "brands-none";
            if (isset($parts[1])) {
                switch ($parts[1]) {
                    case "qub.ac.uk":
                        $class = "brand-qub";
                        $logo = $baseLogoUrl . "/brands/qub/queens-logo.svg";
                        break;
                    case"citi.com":
                        $class = "brand-citi";
                        $logo = $baseLogoUrl . "/brands/citi/300px-Citi.svg.png";
                        break;
                    case"florp.com":
                        $class = "brand-florp";
                        $logo = $baseLogoUrl . "/brands/florp/florpLogoWhite.svg";
                        break;
                    default:
                        $class = "brand-florp";
                        $logo = $baseLogoUrl . "/brands/florp/florpLogoWhite.svg";
                }
            }
            $app = "/" . Routes::APP;
            $br = $app . Routes::BOOKING_REQUEST;

            $adminNavBar = "";
            if ($currentUser->hasRole(FlorpUser::USER_ROLE_ADMIN)) {
                $adminUrls = [
                    "Admin Floor" => $app . Routes::ADMIN_FLOOR,
                    "Admin Room" => $app . Routes::ADMIN_ROOM,
                    "Admin Property" => $app . Routes::ADMIN_PROPERTY,
                    "Admin Settings" => $app . Routes::ADMIN_SETTINGS,
                    "Admin Building" => $app . Routes::ADMIN_BUILDING,
                    "Admin User" => $app . Routes::ADMIN_USER,
                    "Admin Statistics" => $app . Routes::ADMIN_STATISTICS
                ];

                $adminNavBar = <<<HTML
                         <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Admin
                                <span class="caret"></span>
                            </a>
                           <ul class="dropdown-menu dropdown-menu-nav">
HTML;
                foreach ($adminUrls as $linkName => $url) {
                    $adminNavBar .= <<<HTML
                                <li>
                                    <a href="{$url}">
                                        {$linkName}
                                    </a>
                                </li>
HTML;
                }
                $adminNavBar .= <<<HTML
                        </li>
                    </ul>
HTML;
            }
            $navBarHtml = <<<HTML
            
            <nav class="navbar navbar-default {$class}">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="{$app}">
                            <img class="navbar-brand-logo" style="height:35px;max-width: 114px;" src={$logo} alt="">
                        </a>
                    </div>
                    <ul class="nav navbar-nav">
                        <li><a href="{$app}">Home</a></li>
                        <li><a href="{$br}">My Bookings</a></li>
                        <li><a href="{$br}add/">Add Booking</a></li>
                         {$adminNavBar}
                        <li><a href="/login/logout" class="pull-right right">Logout</a></li>
                </div>
            </nav>
HTML;
        } catch (NotLoggedInException $e) {
            $navBarHtml = <<<HTML

HTML;
        }

        ?>
        <div id="top">
            <?php
            print $navBarHtml;
            $title = $this->getTitle();

            if ($title != "") {
                print "<h1>" . $title . "</h1>";
            }

            ?>
        </div>
        <div id="content">
        <?php
    }

    protected function printTail()
    {
        parent::printTail();

        ?>
        </div>
        <div id="tail">

        </div>
        <?php
    }

    protected function getTitle()
    {
        $pageSettings = HtmlPageSettings::singleton();
        return $pageSettings->pageTitle;
    }

    protected function printContent($content)
    {
        print $content;
    }

    protected function printLayout($content)
    {
        $resources = [
            "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css",
            "//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css",
            "//code.jquery.com/jquery-2.1.1.min.js",
            "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js",
        ];
        foreach ($resources as $resource) {
            ResourceLoader::loadResource($resource);
        }
        ResourceLoader::loadResource("/static/css/app.css");
        ?>
        <html>
        <head>
            <title><?= $this->getTitle(); ?></title>
            <?= LayoutModule::getHeadItemsAsHtml(); ?>
            <?= ResourceLoader::getResourceInjectionHtml(); ?>
        <body>
        <?= LayoutModule::getBodyItemsAsHtml(); ?>
        <?php $this->printPageHeading(); ?>
        <?php $this->printContent($content); ?>
        <?php $this->printTail(); ?>
        </body>
        </html>
        <?php
    }
}
