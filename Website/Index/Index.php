<?php

namespace SpaceCadets\Florp\Website\Index;

use Rhubarb\Leaf\Leaves\Leaf;

class Index extends Leaf
{
    /**
     * @var IndexModel
     */
    protected $model;

    protected function getViewClass()
    {
        return IndexView::class;
    }

    protected function createModel()
    {
        $model = new IndexModel();

        // If your model has events you want to listen to you should attach the handlers here
        // e.g.
        // $model->selectedUserChangedEvent->attachListener(function(){ ... });

        return $model;
    }
}