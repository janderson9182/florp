<?php

namespace SpaceCadets\Florp\Website\Index;

use Rhubarb\Crown\Exceptions\ForceResponseException;
use Rhubarb\Crown\LoginProviders\Exceptions\NotLoggedInException;
use Rhubarb\Crown\Response\RedirectResponse;
use Rhubarb\Leaf\Views\View;
use SpaceCadets\Florp\Models\FlorpUser;
use SpaceCadets\Florp\Routes;
use SpaceCadets\Florp\Website\Login\SiteLogin;

class IndexView extends View
{
    /**
     * @var IndexModel
     */
    protected $model;

    protected function createSubLeaves()
    {
        parent::createSubLeaves();

    }

    /**
     * @throws ForceResponseException
     */
    protected function printViewContent()
    {
        /**
         * @var $currentUser FlorpUser
         */
        try {
            $currentUser = SiteLogin::getLoggedInUser();
        } catch (NotLoggedInException $e) {
            throw new ForceResponseException(new RedirectResponse("/login/"));
        }
        ?>
        <div class="page-header">
            <h2>Welcome, <?= $currentUser->getFullName()?></h2>
        <h3>With Florp we aim to provide a smart, efficient room booking service.</h3>
        <h4>We use smart algorithms to determine where to place your bookings.</h4>
            <div class="container text-center">
                <a class="btn btn-success btn-lg btn-get-started" href="/<?=Routes::APP . Routes::BOOKING_REQUEST?>add">Create Booking Request</a>
                <a class="btn btn-success btn-lg btn-get-started" href="/<?=Routes::APP . Routes::TIMETABLE?>">What's Available Now?</a>

            </div>
        </div>
        <?php
    }
}