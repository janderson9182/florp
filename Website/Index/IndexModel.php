<?php

namespace SpaceCadets\Florp\Website\Index;

use Rhubarb\Leaf\Leaves\LeafModel;

class IndexModel extends LeafModel
{
    public function __construct()
    {
        parent::__construct();
    }
}