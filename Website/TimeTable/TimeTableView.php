<?php

namespace SpaceCadets\Florp\Website\TimeTable;

use Rhubarb\Leaf\Views\View;
use SpaceCadets\Florp\Services\Statistic\GraphOfBookingsStatistic;

class TimeTableView extends View
{
    /**
    * @var TimeTableModel
    */
    protected $model;
    
    protected function printViewContent()
    {
        ?>
        <h1>What's Currently free</h1>
        <div class="adminCardContainerPadding">
        <h3>Below are all the bookings for the day</h3>

        <?php print new GraphOfBookingsStatistic(); ?>
        </div>
        <?php

    }
}