<?php

namespace SpaceCadets\Florp\Website\TimeTable;

use Rhubarb\Leaf\Leaves\Leaf;

class TimeTable extends Leaf
{
    /**
    * @var TimeTableModel
    */
    protected $model;
    
    protected function getViewClass()
    {
        return TimeTableView::class;
    }
    
    protected function createModel()
    {
        $model = new TimeTableModel();

        // If your model has events you want to listen to you should attach the handlers here
        // e.g.
        // $model->selectedUserChangedEvent->attachListener(function(){ ... });

        return $model;
    }
}