# Florp By SpaceCadets
## Local Development Environment installation 
### Linux / Ubuntu / macOS (Windows not recommended)
Follow these steps *in order:*
* Install virtual box https://www.virtualbox.org/wiki/Downloads or from your package manager 
* Install vagrant https://www.vagrantup.com/downloads.html or from your package manager 
* Install php (applicable source):
```sh
# macOS (Homebrew):
brew install php7.1*
# apt repo (Ubuntu etc) 
sudo apt-get install php7.1
# yum repository (centos / redhat etc)
sudo yum install php7.1*
```

* Install composer globally
```php
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

* Install the project and boot

```sh
git clone git@gitlab.com:janderson9182/florp.git
cd florp/code
composer install --ignore-platform-reqs
vagrant up
```

* Open localhost:8080/login/ in your browser. The admin account is `florp` and the password is `password`

## Centos Live AWS Server installation instructions
1: Install PHP
```sh
sudo yum install -y php71.x86_64 php71-cli.x86_64 php71-common.x86_64 php71-gd.x86_64 php71-json.x86_64 php71-mbstring.x86_64 php71-mcrypt.x86_64 php71-mysqlnd.x86_64 php71-pdo.x86_64 php71-pecl-imagick.x86_64 php71-process.x86_64
xml.x86_64
```

2: Install apache web server
```sh
sudo yum install -y httpd24.x86_64 httpd24-tools.x86_64
```

3: Install git
```
sudo yum install -y git
```

4: Install composer: https://getcomposer.org/download/

5: Create ssh key pair, enter the command and press enter 4 times
```sh
ssh-keygen
```

You will find the newly generated public key in ~.ssh/idrsa_pub
Add the public key to git lab
6:
clone the repo & run composer install
```sh
git clone git@gitlab.com:janderson9182/florp.git
cd csc3032-1718-space-cadets/code
composer install --ignore-platform-reqs
```
7: 
In your Apache config ensure that you include the following two lines: 
```
DocumentRoot /var/www/html/florp/code/
Include [yourpathtoflorp]/florp/vendor/rhubarbphp/rhubarb/platform/standard-development-apache-rewrites.conf 
```

