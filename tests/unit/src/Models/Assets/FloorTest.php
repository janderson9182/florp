<?php

namespace SpaceCadets\Florp\Tests\Unit\src\Models;

use SpaceCadets\Florp\Models\Assets\Floor;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\Services\Assets\Room\RoomTestHelper;

class FloorTest extends FlorpTestCase
{
    use  RoomTestHelper;

    public function testCanCreateFloor()
    {
        (new Floor())->save();
    }

    public function testFlorCanHaveManyRooms()
    {
        $room1 = $this->getRoomModel();
        $room1->Name = "Room1";
        $room1->save();

        $room2 = $this->getRoomModel();
        $room2->Name = "Room2";
        $room2->save();

        $floor = new Floor();
        $floor->save();

        $floor->Rooms->append($room1);
        $floor->Rooms->append($room2);

        $this->assertEquals(2, $floor->Rooms->count());
    }
}