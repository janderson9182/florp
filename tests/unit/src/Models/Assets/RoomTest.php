<?php

namespace SpaceCadets\Florp\Tests\Unit\src\Models;

use PHPUnit\Framework\Assert;
use Rhubarb\Stem\Collections\RepositoryCollection;
use SpaceCadets\Florp\Models\Assets\Properties\Property;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\Services\Assets\Room\RoomTestHelper;
use SpaceCadets\Florp\Tests\Unit\src\Models\Assets\Properties\PropertyTestHelper;

class RoomTest extends FlorpTestCase
{
    use PropertyTestHelper;
    use RoomTestHelper;

    public function testCanCreateRoom()
    {
        $room = new Room();
        $room->save();
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    public function testRoomHasProperty()
    {
        $propertyThatRoomDoesNotHave = $this->getPropertyModel(true);
        $property = $this->getPropertyModel(true);
        $room = $this->getRoomModel(true);

        $room->addProperty($property);
        Assert::greaterThan(0)->evaluate($room->Properties->count(), "The Room Should have 1 property");
        Assert::equalTo(1)->evaluate($room->Properties->count(), "The Room Should have 1 property");

        $this->assertTrue($room->hasProperty($property));
        $this->assertFalse($room->hasProperty($propertyThatRoomDoesNotHave));
    }

    public function testRoomHasProperties()
    {
        $property1 = $this->getPropertyModel(true);
        $property2 = $this->getPropertyModel(true);

        $room = $this->getRoomModel(true);

        $room->addProperty($property1);
        $room->addProperty($property2);
        Assert::assertTrue($room->hasProperties($room->Properties));
    }
}