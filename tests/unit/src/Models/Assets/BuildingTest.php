<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 07/03/18
 * Time: 17:26
 */

namespace SpaceCadets\Florp\Tests\Unit\src\Models\Assets\Building;

use SpaceCadets\Florp\Models\Assets\Building;
use SpaceCadets\Florp\Models\Assets\Floor;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class BuildingTest extends FlorpTestCase
{
    public function testCanCreateBuilding()
    {
        $building = new Building();
        $building->save();
        $foundBuilding = Building::findFirst();
        $this->assertEquals($building->Id,$foundBuilding->Id);
    }

    public function testCanAddFloorToBuilding()
    {
        $building = new Building();
        $testFloor = new Floor();
        $testFloor->save();
        $building->save();
        $building->Floors->append($testFloor);
        $this->assertEquals($building->Floors->count(),1);
        $this->assertEquals($building->Floors->count(),Building::findFirst()->Floors->count());
    }

    public function testCanAddRoomToFloorToBuilding()
    {
        $building = new Building();
        $building->save();
        $testFloor = new Floor();
        $testFloor->save();
        $testRoom = new Room();
        $testRoom->save();
        $testFloor->Rooms->append($testRoom);
        $building->Floors->append($testFloor);
        $this->assertEquals($building->Floors[0]->Rooms->count(),1);
        $this->assertEquals($building->Floors[0]->Rooms->count(),Building::findFirst()->Floors[0]->Rooms->count());
    }

    public function testCanAccessBuildingFromROomAndRoomFromBuilding()
    {
        $building = new Building();
        $building->Name="CSB";
        $building->save();
        $testFloor1 = new Floor();
        $testFloor1->save();
        $testRoom1 = new Room();
        $testRoom1->Name="Lab1";
        $testRoom1->save();
        $testFloor1->Rooms->append($testRoom1);
        $building->Floors->append($testFloor1);
        $this->assertEquals($building->Floors[0]->Rooms[0]->Name,"Lab1");
        $this->assertEquals($testRoom1->Floor->Building->Name,"CSB");
    }
}