<?php

namespace SpaceCadets\Florp\Tests\Unit\src\Models\Assets\Properties;

use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\Services\Assets\Room\RoomTestHelper;

class RoomPropertyTest extends FlorpTestCase
{
    use RoomTestHelper;
    use PropertyTestHelper;

    public function testRoomCanHaveProperties()
    {
        $room = $this->getRoomModel(true);
        $property = $this->getPropertyModel(true);
        $room->addProperty($property);
        $this->assertEquals($room->Properties->count(), 1);
    }
    public function testRoomCanHaveNoProperties()
    {
        $room = $this->getRoomModel(true);
        $this->assertEquals($room->Properties->count(),0);
    }

    public function testPropertyCanHaveRooms()
    {
        $room = $this->getRoomModel(true);
        $property = $this->getPropertyModel(true);
        $property->addRoom($room);
        $this->assertEquals($property->Rooms->count(), 1);
    }
}