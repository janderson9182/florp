<?php

namespace SpaceCadets\Florp\Tests\Unit\src\Models\Assets\Properties;

use SpaceCadets\Florp\Models\Assets\Properties\Property;
use PHPUnit\Framework\TestCase;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;

class PropertyTest extends TestCase
{
    use PropertyTestHelper;

    public function testCanCreateNewProperty()
    {
        $property = new Property();
        $property->save();
    }

    public function testCanPropertyHasCorrectColumns()
    {
        $property = new Property();
        $property->save();
        $publicData = $property->exportPublicData();
        $this->assertArrayHasKey("Id", $publicData);
        $this->assertArrayHasKey("Name", $publicData);
        $this->assertArrayHasKey("Description", $publicData);
    }

    /**
     * Note: If this test fails, a new column may have been added
     *
     */
    public function testCanPropertyCanBePopulated()
    {
        $property = $this->getPropertyModel(true);

        $publicData = $property->exportPublicData();
        $firstFoundProperty = Property::findFirst();
        foreach ($publicData as $publicDatum) {
            $this->assertEquals(
                $property->$publicDatum,
                $firstFoundProperty->$publicDatum,
                "The first one we find should have all the right data"
            );
        }
    }

    public function testCanPropertyCanAddBookingRequest()
    {
        $property = $this->getPropertyModel(true);
        $bookingRequest = new BookingRequest();
        $bookingRequest->save();

        $property->addBookingRequest($bookingRequest);
        $this->assertEquals($bookingRequest->Id, $property->BookingRequests[0]->Id);
    }
}