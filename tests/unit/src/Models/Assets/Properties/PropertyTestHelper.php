<?php

namespace SpaceCadets\Florp\Tests\Unit\src\Models\Assets\Properties;

use SpaceCadets\Florp\Models\Assets\Properties\Property;

trait PropertyTestHelper
{
    public function getPropertyModel($save = false): Property
    {
        $property = new Property();
        $property->Name = "Test Name";
        $property->Description = "Test Description";
        if ($save) {
            $property->save();
        }
        return $property;
    }
}