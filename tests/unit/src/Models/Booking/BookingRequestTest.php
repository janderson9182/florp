<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 14-Dec-17
 * Time: 13:05
 */

namespace SpaceCadets\Florp\Tests\unit\src\Models\Booking;



use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Filters\Equals;
use SpaceCadets\Florp\Models\Assets\Properties\Property;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Models\FlorpUser;
use SpaceCadets\Florp\Services\src\Models\Bookings\CreateBookingFromRequestUseCase;
use SpaceCadets\Florp\Services\src\Models\Bookings\CreateCheckInForRoomUseCase;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\unit\src\Models\User\UserTestHelper;

class BookingRequestTest extends FlorpTestCase
{

    use BookingRequestHelper;
    use UserTestHelper;

    public function testCanCreateBookingRequest(){
        $bookingRequest = $this->getBookingRequestModel();
        $bookingRequest->save();
        $foundBookingRequest = BookingRequest::findFirst();
        $this->assertEquals($bookingRequest->StartTime,$foundBookingRequest->StartTime);
    }

    public function testCanAddPropertyToBookingRequest()
    {
        $propertyProjector = new Property();
        $propertyProjector->Name=("Projector");
        $propertyProjector->save();
        $bookingRequest = $this->getBookingRequestModel(true);
        $bookingRequest->addProperty($propertyProjector);
        $foundBookingRequest = BookingRequest::findFirst();
        $this->assertEquals($bookingRequest->Properties,$foundBookingRequest->Properties);
        $this->assertEquals(1,$bookingRequest->Properties->count());
    }

    public function testCanAddPropertiesToBookingRequest()
    {
        $propertyProjector = new Property();
        $propertyProjector->Name=("Projector");
        $propertyProjector->save();
        $propertyFlipChart = new Property();
        $propertyFlipChart->Name=("FlipChart");
        $propertyFlipChart->save();
        $bookingRequest = $this->getBookingRequestModel(true);
        $bookingRequest->addProperty($propertyProjector);
        $bookingRequest->addProperty($propertyFlipChart);
        $foundBookingRequest = BookingRequest::findFirst();
        $this->assertEquals($bookingRequest->Properties,$foundBookingRequest->Properties);
        $this->assertEquals(2,$bookingRequest->Properties->count());

    }

    public function testCanAddBookingRequestForUser()
    {
        $bookingRequest = $this->getBookingRequestModel(true);
        $propertyProjector = new Property();
        $propertyProjector->Name=("Projector");
        $bookingRequest->addProperty($propertyProjector);
        $user = $this->getUserModel(true);
        $user->addBookingRequest($bookingRequest);
        $user->save();
        $foundUser = FlorpUser::findFirst();
        $this->assertEquals($user->BookingRequests[0]->StartTime,$foundUser->BookingRequests[0]->StartTime);
    }

    public function testCanEditBookingRequest()
    {
        $bookingRequest = $this->getBookingRequestModel();
        $bookingRequest->NumberOfSeats=100;
        $bookingRequest->save();
        $this->assertEquals($bookingRequest->NumberOfSeats,100);
    }

    public function testCanCancelBooking()
    {
        $bookingRequest = new BookingRequest();
        $bookingRequest->StartTime=new RhubarbDateTime("now");
        $bookingRequest->EndTime=new RhubarbDateTime("now");
        $bookingRequest->NumberOfSeats=10;
        $bookingRequest->WillingToShare=true;
        $bookingRequest->save();
        $booking = new Booking();
        $booking->save();
        //Ignore this error, this is a phpstorm Error
        $bookingRequest->Booking=$booking;
        $bookingRequest->BookingId=$booking->Id;
        $bookingRequest->save();
        $bookingRequest->cancel();
        $this->assertEquals($bookingRequest->Cancelled,true);
        $this->assertEquals($booking->Cancelled,true);
    }

    public function testCanAddBookingFromBookingRequest(){
        $bookingRequest = new BookingRequest();
        $bookingRequest->StartTime=new RhubarbDateTime("now");
        $bookingRequest->EndTime=new RhubarbDateTime("now");
        $bookingRequest->NumberOfSeats=10;
        $bookingRequest->Duration=1;
        $bookingRequest->WillingToShare=true;
        $booking = CreateBookingFromRequestUseCase::create()->execute($bookingRequest);
        $this->assertEquals($bookingRequest->StartTime,$booking->StartTime);
    }

/*
    public function testBookingIsCreatedAfterBookingRequestIsSaved()
    {
        $bookingRequest = new BookingRequest();
        $bookingRequest->StartTime = new RhubarbDateTime("now");
        $bookingRequest->EndTime = new RhubarbDateTime("now");
        $bookingRequest->NumberOfSeats = 10;
        $bookingRequest->Duration = 1;
        $bookingRequest->WillingToShare = true;
        $bookingRequest->save();
        $this->assertNotNull($bookingRequest->BookingId, "BookingRequest should have a BookingId after saving");
        $this->assertNotNull($bookingRequest->Booking, "BookingRequest should access to a booking object after save");
    }
*/

    public function testBookingRequestCanLoadBookingFromJustBookingId()
    {
        $booking = new Booking();
        $booking->save();

        $bookingRequst = new BookingRequest();
        $bookingRequst->BookingId = $booking->Id;
        $bookingRequst->save();

        $this->assertEquals($booking->Id, $bookingRequst->Booking->Id);
    }

    public function testBookingCanLoadBookingRequestFromJustBookingRequestId()
    {
        $bookingRequst = new BookingRequest();
        $bookingRequst->save();

        $booking = new Booking();
        $booking->BookingRequestId = $bookingRequst->Id;
        $booking->save();

        $this->assertEquals($bookingRequst->Id, $booking->BookingRequest->Id);
    }

    public function testBookingRequestCantBeDeleted()
    {
        $bookingRequstOne = new BookingRequest();
        $bookingRequstOne->save();

        $bookingRequst = new BookingRequest();
        $bookingRequst->save();

        $countBefore = BookingRequest::all()->count();
        $bookingRequst->delete();
        $this->assertEquals(
            $countBefore,
            BookingRequest::all()->count(),
            "Cancelling a booking request should not delete the booking request"
        );
    }
}