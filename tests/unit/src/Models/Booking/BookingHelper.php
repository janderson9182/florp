<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 22-Feb-18
 * Time: 17:03
 */

namespace SpaceCadets\Florp\Tests\Unit\src\Models\Booking;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;

trait BookingHelper
{
    /**
     * @param bool $save
     * @return Booking
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function getBookingModel($save = true): Booking
    {
        $booking = new Booking();
        $booking->StartTime = new RhubarbDateTime("now");
        $booking->EndTime = new RhubarbDateTime("+1 hour");
        $booking->save();
        if ($save) {
            $booking->save();
        }
        return $booking;
    }

    /**
     * @param bool $save
     * @return Room
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function getRoomModel($save = true): Room
    {
        $room = new Room();
        $room->Name = "CheckInRoom";
        if ($save) {
            $room->save();
        }
        return $room;
    }
}