<?php
/**
 * Created by Team Space Cadets
 * User: stephen
 * Date: 18-Dec-17
 * Time: 11:12
 */

namespace SpaceCadets\Florp\Tests\Unit\src\Models\Booking;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;

trait BookingRequestHelper
{
    public function getBookingRequestModel($save = false): BookingRequest
    {
        $bookingRequest = new BookingRequest();
        $bookingRequest->StartTime=new RhubarbDateTime("now");
        $bookingRequest->EndTime=new RhubarbDateTime("now");
        $bookingRequest->NumberOfSeats=10;
        $bookingRequest->WillingToShare = false;
        if ($save) {
            $bookingRequest->save();
        }
        return $bookingRequest;
    }

    /**
     * @param $size
     * @return BookingRequest
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     * @author james
     */
    private function createBookingRequestOfSize($size): BookingRequest
    {
        $bookingRequest = $this->getBookingRequestModel(true);
        $bookingRequest->NumberOfSeats = $size;
        $bookingRequest->save();
        return $bookingRequest;
    }
}