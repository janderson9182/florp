<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 22-Feb-18
 * Time: 16:18
 */

namespace SpaceCadets\Florp\Tests\Unit\src\Models\Booking;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Filters\Equals;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\src\Models\Bookings\CreateBookingFromRequestUseCase;
use SpaceCadets\Florp\Services\src\Models\Bookings\CreateCheckInForRoomUseCase;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class BookingTest extends FlorpTestCase

{
    use BookingHelper;

    public function testCheckInIsSetToFalseByDefaultForABooking()
    {
        $booking = new Booking();
        $booking->save();
        $this->assertEquals($booking->CheckedIn,false);
    }
    public function testCheckInCanBeSetForBookingCreatedFromBookingRequest(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->RoomId=$room->Id;
        $booking->save();
        CreateCheckInForRoomUseCase::create()->execute($room->Id);
        $booking->reload();
        $this->assertEquals($booking->CheckedIn,true);
    }

    public function testWillNotCheckInIfNotValid(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->StartTime= new RhubarbDateTime("+1 hour");
        $booking->EndTime= new RhubarbDateTime("+2 hour");
        $booking->RoomId=$room->Id;
        $booking->save();
        CreateCheckInForRoomUseCase::create()->execute($room->Id);
        $booking->reload();
        $this->assertEquals($booking->CheckedIn,false);
    }

    public function testCheckInWillNotTriggerForPastBookings(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->StartTime= new RhubarbDateTime("-1 hour");
        $booking->EndTime= new RhubarbDateTime("now");
        $booking->RoomId=$room->Id;
        $booking->save();
        CreateCheckInForRoomUseCase::create()->execute($room->Id);
        $booking->reload();
        $this->assertEquals($booking->CheckedIn,false);
    }

    public function testCheckInWillNotTriggerForOtherRooms(){
        $room=$this->getRoomModel();
        $room2 = new Room();
        $room2->Name="CheckInRoom";
        $room2->save();
        $booking=$this->getBookingModel();
        $booking->StartTime= new RhubarbDateTime("now");
        $booking->EndTime= new RhubarbDateTime("+1 hour");
        $booking->RoomId=$room->Id;
        $booking->save();
        CreateCheckInForRoomUseCase::create()->execute($room2->Id);
        $booking->reload();
        $this->assertEquals($booking->CheckedIn,false);
    }
    public function testCanCheckInWorkForExtremeTimes(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->StartTime= new RhubarbDateTime("-1 min");
        $booking->EndTime= new RhubarbDateTime("+30 min");
        $booking->RoomId=$room->Id;
        $booking->save();
        CreateCheckInForRoomUseCase::create()->execute($room->Id);
        $booking->reload();
        $this->assertEquals($booking->CheckedIn,true);
    }
    public function testCanCheckInWorkForExtremeDate(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->StartTime= new RhubarbDateTime("yesterday");
        $booking->EndTime= new RhubarbDateTime("tomorrow");
        $booking->RoomId=$room->Id;
        $booking->save();
        CreateCheckInForRoomUseCase::create()->execute($room->Id);
        $booking->reload();
        $this->assertEquals($booking->CheckedIn,false);
    }
    public function testCheckInWillNotTriggerIfEndDateIsBeforeStartDate(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->StartTime= new RhubarbDateTime("tomorrow");
        $booking->EndTime= new RhubarbDateTime("now");
        $booking->RoomId=$room->Id;
        $booking->save();
        CreateCheckInForRoomUseCase::create()->execute($room->Id);
        $booking->reload();
        $this->assertEquals($booking->CheckedIn,false);
    }
    public function testCanCheckInBeSetForABooking()
    {
        $room=$this->getRoomModel();
        $booking1=$this->getBookingModel();
        $booking1->RoomId=$room->Id;
        $booking2=$this->getBookingModel();
        $booking2->RoomId=$room->Id;
        $booking2->StartTime=(new RhubarbDateTime("+1 hour"));
        $booking2->EndTime=(new RhubarbDateTime("+2 hour"));
        $booking3=$this->getBookingModel();
        $booking3->RoomId=$room->Id;
        $booking3->StartTime=(new RhubarbDateTime("+2 hour"));
        $booking3->EndTime=(new RhubarbDateTime("+3 hour"));
        $booking4=$this->getBookingModel();
        $booking4->RoomId=$room->Id;
        $booking4->StartTime=(new RhubarbDateTime("+3 hour"));
        $booking4->EndTime=(new RhubarbDateTime("+4 hour"));
        $booking1->save();
        $booking2->save();
        $booking3->save();
        $booking4->save();
        CreateCheckInForRoomUseCase::create()->execute($room->Id);
        $booking1->reload();
        $booking2->reload();
        $booking3->reload();
        $booking4->reload();
        $this->assertEquals($booking1->CheckedIn,true);
        $this->assertEquals($booking2->CheckedIn,false);
        $this->assertEquals($booking3->CheckedIn,false);
        $this->assertEquals($booking4->CheckedIn,false);
    }

}