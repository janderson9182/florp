<?php
/**
 * Created by PhpStorm.
 * User: ron_0
 * Date: 16/04/2018
 * Time: 01:36
 */

namespace SpaceCadets\Florp\Tests\Unit\src\Models\Booking;

use PHPUnit\Framework\Assert;
use SpaceCadets\Florp\Models\Assets\Floor;
use SpaceCadets\Florp\Models\Bookings\BookingIsLockedNotificationEmail;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class BookingIsLockedNotificationEmailTest extends FlorpTestCase
{

    use BookingHelper;

    public function testSubject(){
        $testBooking = $this->getBookingModel();
        $testEmail = "test@email.com";
        $lockedBookingEmail = new BookingIsLockedNotificationEmail([$testEmail], $testBooking);
        Assert::assertEquals("Room Booking Successfully Locked In!", $lockedBookingEmail->getSubject(), "Email subject is as expected.");
    }

    public function testRecipientListWithOneEmail(){
        $testBooking = $this->getBookingModel();
        $testEmail = "test@email.com";
        $lockedBookingEmail = new BookingIsLockedNotificationEmail([$testEmail], $testBooking);
        Assert::assertEquals("test@email.com", $lockedBookingEmail->getRecipientList(), "Email recipients are as expected.");
    }

    public function testRecipientListWithMultipleEmails(){
        $testBooking = $this->getBookingModel();
        $testEmail1 = "test1@email.com";
        $testEmail2 = "test2@email.com";
        $testEmail3 = "test3@email.com";
        $testEmails = [$testEmail1, $testEmail2, $testEmail3];
        $lockedBookingEmail = new BookingIsLockedNotificationEmail($testEmails, $testBooking);
        Assert::assertEquals("test1@email.com, test2@email.com, test3@email.com", $lockedBookingEmail->getRecipientList(), "Email recipients are as expected.");
    }

    public function testGetHtml() {

    }

    public function testGetHtmlTemplateBody(){
        $testBooking = $this->getBookingModel();
        $testBooking->RoomId = $this->getRoomModel(true)->Id;

        //Creating a test floor for the email
        $testFloor = new Floor();
        $testFloor->Name = "Test Room";
        $testFloor->save();
        $testBooking->Room->FloorId = $testFloor->Id;

        $testEmail = "test@email.com";
        $lockedBookingEmail = new BookingIsLockedNotificationEmail([$testEmail], $testBooking);
        Assert::assertContains("<h1>", $lockedBookingEmail->getText(), "Returns expected HTML.");
        Assert::assertContains("<h1>", $lockedBookingEmail->getHtml(), "Returns expected HTML.");
    }

}
