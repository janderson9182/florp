<?php

namespace SpaceCadets\Florp\Tests\unit\src\Models\User;

use SpaceCadets\Florp\Models\Assets\Properties\Property;
use SpaceCadets\Florp\Models\FlorpUser;

trait UserTestHelper
{
    public function getPropertyModel($save = false): Property
    {
        $property = new Property();
        $property->Name=("Projector");
        $property->Description=("Lorum Ipsum");
        if ($save) {
            $property->save();
        }
        return $property;
    }


    /**
     * @param bool $save
     * @return FlorpUser
     */
    public function getUserModel($save = false): FlorpUser
    {
        $user = new FlorpUser();
        $user->Forename="John";
        $user->Surname="Smith";
        $user->Username="JohnSmith";
        $user->Email="test@gmail.com";
        $user->setNewPassword("Password");
        if ($save) {
            $user->save();
        }
        return $user;

    }
}