<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 15-Dec-17
 * Time: 13:04
 */

namespace SpaceCadets\Florp\Tests\Unit\src\Models\User;


use Rhubarb\Scaffolds\AuthenticationWithRoles\Role;
use SpaceCadets\Florp\Models\FlorpUser;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class UserTest extends FlorpTestCase
{
    use UserTestHelper;

    public function testCreateNewUser()
    {
        $user = new FlorpUser();
        $user->Forename="John";
        $user->Surname="Smith";
        $user->Username="JohnSmith";
        $user->Email="test@gmail.com";
        $user->setNewPassword("Password");
        $user->save();
        $foundUser = FlorpUser::findFirst();
        $this->assertEquals($user->Forename,$foundUser->Forename);
    }

    public function testCanAddAdminRoleToUser(){
        $user = new FlorpUser();
        $user->Forename="John";
        $user->Surname="Smith";
        $user->Username="JohnSmith";
        $user->Email="test@gmail.com";
        $user->setNewPassword("Password");
        $user->save();

        $adminRole = new Role();
        $adminRole->RoleName=FlorpUser::USER_ROLE_ADMIN;
        $adminRole->save();

        $user->addToRole($adminRole);
        $user->save();

        $this->assertEquals($user->Roles[0]->RoleName,FlorpUser::USER_ROLE_ADMIN);
    }



}