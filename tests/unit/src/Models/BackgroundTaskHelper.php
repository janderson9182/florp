<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 16/02/2018
 * Time: 13:55
 */

namespace SpaceCadets\Florp\Tests\Unit\src\Models;

use Rhubarb\Scaffolds\BackgroundTasks\Models\BackgroundTaskStatus;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceSmallestFirst\RoomBookingServiceSmallestFirstProvider;

trait BackgroundTaskHelper
{
    public function getBackgroundTaskModel(bool $save = false)
    {
        $backgroundTaskStatus = new BackgroundTaskStatus();
        if ($save) {
            $backgroundTaskStatus->save();
        }
        return $backgroundTaskStatus;
    }

    /**
     * @param int $bookingRequestId
     * @return BackgroundTaskStatus
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    private function getBackgroundTaskForBookingRequest(int $bookingRequestId): BackgroundTaskStatus
    {
        $backgroundTaskStatus = $this->getBackgroundTaskModel(true);
        $backgroundTaskStatus->TaskSettings = [RoomBookingServiceSmallestFirstProvider::SETTING_BOOKING_REQUEST_ID => $bookingRequestId];
        $backgroundTaskStatus->save();
        return $backgroundTaskStatus;
    }
}