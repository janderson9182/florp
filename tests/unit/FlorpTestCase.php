<?php

namespace SpaceCadets\Florp\Tests\Unit;

use Codeception\Test\Unit;
use Rhubarb\Crown\Encryption\HashProvider;
use Rhubarb\Crown\Encryption\Sha512HashProvider;
use Rhubarb\Crown\Layout\LayoutModule;
use Rhubarb\Crown\LoginProviders\LoginProvider;
use Rhubarb\Crown\Sendables\Email\EmailProvider;
use Rhubarb\Crown\Tests\Fixtures\UnitTestingEmailProvider;
use Rhubarb\Stem\Repositories\Offline\Offline;
use Rhubarb\Stem\Repositories\Repository;
use Rhubarb\Stem\Schema\SolutionSchema;
use Rhubarb\Stem\Tests\unit\Fixtures\TestLoginProvider;
use SpaceCadets\Florp\Florp;
use SpaceCadets\Florp\Models\FlorpSolutionSchema;

class FlorpTestCase extends Unit
{
    protected $application;

    protected function _before()
    {
        parent::_before();

        $this->application = new Florp();
        $this->application->unitTesting = true;
        $this->application->initialiseModules();

        Repository::setDefaultRepositoryClassName(Offline::class);
        SolutionSchema::registerSchema('App', FlorpSolutionSchema::class);

        LayoutModule::disableLayout();

        HashProvider::setProviderClassName(Sha512HashProvider::class);
        EmailProvider::setProviderClassName(UnitTestingEmailProvider::class);
        LoginProvider::setProviderClassName(TestLoginProvider::class);
    }

    public function assertThrowsException($expectedException, callable $callable, $message = null)
    {
        $thrown = false;
        try {
            $callable();
        } catch (\Exception $ex) {
            $thrown = true;
            $this->assertInstanceOf($expectedException, $ex);
        }

        $this->assertTrue($thrown, $message !== null ? $message : "Expected a {$expectedException} to be thrown");
    }
}