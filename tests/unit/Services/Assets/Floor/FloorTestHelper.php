<?php

namespace SpaceCadets\Florp\Tests\Unit\Services\Assets\Floor;

use SpaceCadets\Florp\Models\Assets\Floor;

trait FloorTestHelper
{
    public function getFloorModel() : Floor
    {
        $floor = new Floor();
        $floor->Name = "TestFloorName";
        $floor->Number = 1;
        $floor->WheelchairAccess = true;
        return $floor;
    }
}