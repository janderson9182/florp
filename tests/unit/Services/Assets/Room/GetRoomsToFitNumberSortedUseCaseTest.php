<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 16/02/2018
 * Time: 14:14
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\Assets\Room;

use SpaceCadets\Florp\Services\src\Models\Assets\Rooms\GetRoomsToFitNumberSortedUseCase;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class GetRoomsToFitNumberSortedUseCaseTest extends FlorpTestCase
{
    use RoomTestHelper;
    public function testCollectionIsNotEmpty()
    {
        $this->getRoomOfSize(1);
        $rooms = GetRoomsToFitNumberSortedUseCase::create()->execute(1);
        verify($rooms->count())->greaterThan(0);
        verify($rooms->count())->equals(1);
    }

    public function testCollectionContainsOneRoomWhenSearchingForOne()
    {
        $this->getRoomOfSize(1);
        $this->getRoomOfSize(2);
        $rooms = GetRoomsToFitNumberSortedUseCase::create()->execute(2);
        verify($rooms->count())->greaterThan(0);
        verify($rooms->count())->equals(1);
    }
}