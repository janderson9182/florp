<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 16/02/2018
 * Time: 14:25
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\Assets\Room;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Assets\Rooms\IsRoomAvailableBetweenDatesUseCase;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class IsRoomAvailableBetweenDatesUseCaseTest extends FlorpTestCase
{
    use RoomTestHelper;

    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testRoomWithNoBookingsYetIsAvailable()
    {
        $room = $this->getRoomModel(true);
        $room->save();
        $now = new RhubarbDateTime("now");
        /*
        0:00               00:30                01:00
        |                   |                    |
        [                                        ] <- Lack of a booking
        [----------------------------------------] <- Time to see if I we can make a booking
        */
        verify(IsRoomAvailableBetweenDatesUseCase::create()->execute($room, $now, $now))->true();
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testRoomWithBookingsBeforeRequestedDateIsAvailable()
    {
        $room = $this->getRoomModel(true);
        $room->save();

        $booking = new Booking();
        $booking->StartTime = new RhubarbDateTime("Yesterday");
        $booking->EndTime = new RhubarbDateTime("Yesterday");
        $booking->RoomId = $room->Id;
        $booking->save();
        $now = new RhubarbDateTime("now");
        /*
        0:00               00:30                01:00
        |                   |                    |
        [////////////////////                    ] <- Booking before our check
                             [------------------]  <- Time to see if I we can make a booking
        */
        verify(IsRoomAvailableBetweenDatesUseCase::create()->execute($room, $now, $now))->true();
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testRoomWithBookingsAfterRequestedDateIsAvailable()
    {
        $room = $this->getRoomModel(true);
        $room->save();

        $booking = new Booking();
        $booking->StartTime = new RhubarbDateTime("Tomorrow");
        $booking->EndTime = new RhubarbDateTime("Tomorrow");
        $booking->RoomId = $room->Id;
        $booking->save();
        /*
        0:00               00:30                01:00
        |                   |                    |
        [                    ////////////////////] <- Booking after our check
        [------------------]             <- Time to see if I we can make a booking
        */
        $now = new RhubarbDateTime("now");
        verify(IsRoomAvailableBetweenDatesUseCase::create()->execute($room, $now, $now))->true();
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testRoomWithBookingsDuringSameTimeIsNotAvailable()
    {
        $room = $this->getRoomModel(true);
        $room->save();

        $startTime = new RhubarbDateTime("2017-01-01 00:00:00");
        $endTime = new RhubarbDateTime("2017-01-01 01:00:00");

        $booking = new Booking();
        $booking->StartTime = $startTime;
        $booking->EndTime = $endTime;
        $booking->RoomId = $room->Id;
        $booking->save();
        /*
        0:00               00:30                01:00
        |                   |                    |
        [////////////////////////////////////////] <- Existing Booking
        [----------------------------------------]             <- Time to see if I we can make a booking
        */
        verify(IsRoomAvailableBetweenDatesUseCase::create()->execute($room, $startTime, $endTime))->false();
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testRoomWithSearchInTheMiddleOfAnotherBookingIsNotAvailable()
    {
        $room = $this->getRoomModel(true);
        $room->save();

        $existingStartTime = new RhubarbDateTime("2017-01-01 00:00:00");
        $startTime = new RhubarbDateTime("2017-01-01 00:15:00");
        $existingEndTime = new RhubarbDateTime("2017-01-01 01:00:00");
        $endTime = new RhubarbDateTime("2017-01-01 00:45:00");

        $booking = new Booking();
        $booking->StartTime = $existingStartTime;
        $booking->EndTime = $existingEndTime;
        $booking->RoomId = $room->Id;
        $booking->save();
        /*
        0:00               00:30                01:00
        |                   |                    |
        [////////////////////////////////////////] <- Existing Booking
                 [--------------------]            <- Time to see if I we can make a booking
        */
        verify(IsRoomAvailableBetweenDatesUseCase::create()->execute($room, $startTime, $endTime))->false();
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testRoomIsNotFreeLowerBoundaryOfExistingBooking()
    {
        $room = $this->getRoomModel(true);
        $room->save();

        $existingStartTime = new RhubarbDateTime("2017-01-01 00:30:00");
        $existingEndTime = new RhubarbDateTime("2017-01-01 01:00:00");

        $startTime = new RhubarbDateTime("2017-01-01 00:15:00");
        $endTime = new RhubarbDateTime("2017-01-01 00:45:00");

        $booking = new Booking();
        $booking->StartTime = $existingStartTime;
        $booking->EndTime = $existingEndTime;
        $booking->RoomId = $room->Id;
        $booking->save();
        /*
        0:00               00:30                01:00
        |                   |                    |
        [                     ///////////////////] <- Existing Booking
                 [--------------------]            <- Time to see if I we can make a booking
        */
        verify(IsRoomAvailableBetweenDatesUseCase::create()->execute($room, $startTime, $endTime))->false();
    }
    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testRoomIsNotFreeUpperBoundaryOfExistingBooking()
    {
        $room = $this->getRoomModel(true);
        $room->save();

        $existingStartTime = new RhubarbDateTime("2017-01-01 00:15:00");
        $existingEndTime = new RhubarbDateTime("2017-01-01 00:45:00");

        $startTime = new RhubarbDateTime("2017-01-01 00:30:00");
        $endTime = new RhubarbDateTime("2017-01-01 01:00:00");

        $booking = new Booking();
        $booking->StartTime = $existingStartTime;
        $booking->EndTime = $existingEndTime;
        $booking->RoomId = $room->Id;
        $booking->save();
        /*
        0:00               00:30                01:00
        |                   |                    |
        [        ///////////////////] <- Existing Booking
                             [--------------------]  <- Time to see if I we can make a booking
        */
        verify(IsRoomAvailableBetweenDatesUseCase::create()->execute($room, $startTime, $endTime))->false();
    }

}