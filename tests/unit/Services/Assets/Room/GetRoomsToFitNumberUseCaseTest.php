<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 16/02/2018
 * Time: 14:05
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\Assets\Room;

use SpaceCadets\Florp\Services\src\Models\Assets\Rooms\GetRoomsToFitNumberUseCase;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class GetRoomsToFitNumberUseCaseTest extends FlorpTestCase
{
    use RoomTestHelper;
    public function testCollectionIsNotEmpty()
    {
        $this->getRoomOfSize(1);
        $rooms = GetRoomsToFitNumberUseCase::create()->execute(1);
        verify($rooms->count())->greaterThan(0);
        verify($rooms->count())->equals(1);
    }

    public function testCollectionContainsOneRoomWhenSearchingForOne()
    {
        $this->getRoomOfSize(1);
        $this->getRoomOfSize(2);
        $rooms = GetRoomsToFitNumberUseCase::create()->execute(2);
        verify($rooms->count())->greaterThan(0);
        verify($rooms->count())->equals(1);
    }
}