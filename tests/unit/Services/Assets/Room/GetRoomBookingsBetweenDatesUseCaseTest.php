<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 16/02/2018
 * Time: 18:37
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\Assets\Room;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Assets\Rooms\GetRoomBookingsBetweenDatesUseCase;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class GetRoomBookingsBetweenDatesUseCaseTest extends FlorpTestCase
{
    /**
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    public function testFindBookingsBetweenDates()
    {
        $room = new Room();
        $room->save();
        //searchable range is 0010 to 0020
        //booking meets range
        $startTime = new RhubarbDateTime("2017-01-01 00:10:00");
        $endTime = new RhubarbDateTime("2017-01-01 00:20:00");
        //booking is shifted 5 mins left
        $startTime0 = new RhubarbDateTime("2017-01-01 00:00:00");
        $endTime0 = new RhubarbDateTime("2017-01-01 00:15:00");
        //booking is shifted 5 mins right
        $startTime1 = new RhubarbDateTime("2017-01-01 00:15:00");
        $endTime1 = new RhubarbDateTime("2017-01-01 00:25:00");
        //booking is expanded 5 mins each side
        $startTime2 = new RhubarbDateTime("2017-01-01 00:05:00");
        $endTime2 = new RhubarbDateTime("2017-01-01 00:25:00");
        //booking is shifted 30 mins right
        $startTime3 = new RhubarbDateTime("2017-01-01 00:35:00");
        $endTime3 = new RhubarbDateTime("2017-01-01 00:55:00");


        $booking = new Booking();
        $booking->RoomId = $room->Id;
        $booking->StartTime = $startTime;
        $booking->EndTime = $endTime;
        $booking->save();

        $booking0 = new Booking();
        $booking0->RoomId = $room->Id;
        $booking0->StartTime = $startTime0;
        $booking0->EndTime = $endTime0;
        $booking0->save();

        $booking1 = new Booking();
        $booking1->RoomId = $room->Id;
        $booking1->StartTime = $startTime1;
        $booking1->EndTime = $endTime1;
        $booking1->save();

        $booking2 = new Booking();
        $booking2->RoomId = $room->Id;
        $booking2->StartTime = $startTime2;
        $booking2->EndTime = $endTime2;
        $booking2->save();

        $booking3 = new Booking();
        $booking3->RoomId = $room->Id;
        $booking3->StartTime = $startTime3;
        $booking3->EndTime = $endTime3;
        $booking3->save();

        verify
        (
            GetRoomBookingsBetweenDatesUseCase::create()
                ->execute(
                    $room,
                    $startTime,
                    $endTime,
                    true
                )
                ->count()
        )
            ->equals(4);
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    public function testCantFindBookingsBetweenNonInclusiveDates()
    {
        $room = new Room();
        $room->save();

        //searchable range is 0010 to 0020
        //booking meets range
        $startTime = new RhubarbDateTime("2017-01-01 00:10:00");
        $endTime = new RhubarbDateTime("2017-01-01 00:20:00");
        //booking is shifted 5 mins left
        $startTime0 = new RhubarbDateTime("2017-01-01 00:00:00");
        $endTime0 = new RhubarbDateTime("2017-01-01 00:15:00");
        //booking is shifted 5 mins right
        $startTime1 = new RhubarbDateTime("2017-01-01 00:15:00");
        $endTime1 = new RhubarbDateTime("2017-01-01 00:25:00");
        //booking is expanded 5 mins each side
        $startTime2 = new RhubarbDateTime("2017-01-01 00:05:00");
        $endTime2 = new RhubarbDateTime("2017-01-01 00:25:00");


        $booking = new Booking();
        $booking->RoomId = $room->Id;
        $booking->StartTime = $startTime;
        $booking->EndTime = $endTime;
        $booking->save();

        $booking0 = new Booking();
        $booking0->RoomId = $room->Id;
        $booking0->StartTime = $startTime0;
        $booking0->EndTime = $endTime0;
        $booking0->save();

        $booking1 = new Booking();
        $booking1->RoomId = $room->Id;
        $booking1->StartTime = $startTime1;
        $booking1->EndTime = $endTime1;
        $booking1->save();

        $booking2 = new Booking();
        $booking2->RoomId = $room->Id;
        $booking2->StartTime = $startTime2;
        $booking2->EndTime = $endTime2;
        $booking2->save();

        verify
        (
            GetRoomBookingsBetweenDatesUseCase::create()
                ->execute(
                    $room,
                    $startTime,
                    $endTime,
                    false
                )
                ->count()
        )
            ->equals(3);
    }
}