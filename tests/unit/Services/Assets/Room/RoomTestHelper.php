<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 16/02/2018
 * Time: 14:22
 */
namespace SpaceCadets\Florp\Tests\Unit\Services\Assets\Room;

use SpaceCadets\Florp\Models\Assets\Room;

trait RoomTestHelper
{
    /**
     * @param bool $save
     * @return Room
     */
    public function getRoomModel($save = false): Room
    {
        $room = new Room();
        $room->Name = "TestName";
        $room->Description = "Test room description";
        if ($save) {
            $room->save();
        }
        return $room;
    }

    private function getRoomOfSize(int $size): Room
    {
        $room = $this->getRoomModel(true);
        $room->NumberOfSeats = $size;
        $room->save();
        return $room;
    }
}