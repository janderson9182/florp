<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 24/02/2018
 * Time: 12:55
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\src\Models\Bookings;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Collections\RepositoryCollection;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetActiveBookingsBetweenDatesUseCase;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class GetActiveBookingsBetweenDatesUseCaseTest extends FlorpTestCase
{
    /**
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testReturnsBookings()
    {
        $eightFiftyNineAm = new RhubarbDateTime("2018-01-01 08:59:59");
        $eightAm = new RhubarbDateTime("2018-01-01 08:59:59");
        $oneMinutePastTen = new RhubarbDateTime("2018-01-01 10:01:00");
        $elevenAm = new RhubarbDateTime("2018-01-01 11:00:00");

        $nineAm = new RhubarbDateTime("2018-01-01 09:00:00");
        $tenAm = new RhubarbDateTime("2018-01-01 10:00:00");

        $foundBookings = GetActiveBookingsBetweenDatesUseCase::create()->execute($nineAm, $tenAm, true);
        $this->assertInstanceOf(
            RepositoryCollection::class,
            $foundBookings,
            "A RepositoryCollection of Bookings should be returned"
        );

        // Add some bookings before and after the expected Booking

        $priorBooking = new Booking();
        $priorBooking->StartTime = $eightAm;
        $priorBooking->EndTime = $eightFiftyNineAm;
        $priorBooking->save();

        $expectedBooking = new Booking();
        $expectedBooking->StartTime = $nineAm;
        $expectedBooking->EndTime = $tenAm;
        $expectedBooking->save();

        $subsequentBooking = new Booking();
        $subsequentBooking->StartTime = $oneMinutePastTen;
        $subsequentBooking->EndTime = $elevenAm;
        $subsequentBooking->save();

        $foundBookings = GetActiveBookingsBetweenDatesUseCase::create()->execute($nineAm, $tenAm, true);
        verify($foundBookings->count())->equals(1);

        $now = new RhubarbDateTime("now");

        $expectedBooking = new Booking();
        $expectedBooking->StartTime = $now;
        $expectedBooking->EndTime = $now;
        $expectedBooking->save();

        $foundBookings = GetActiveBookingsBetweenDatesUseCase::create()->execute($nineAm, $tenAm, true);
        verify($foundBookings->count())->equals(1);
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\DeleteModelException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testCancelledBookingNotReturned()
    {
        $now = new RhubarbDateTime("now");

        $cancelledBooking = new Booking();
        $cancelledBooking->StartTime = $now;
        $cancelledBooking->EndTime = $now;
        $cancelledBooking->Cancelled = true;
        $cancelledBooking->save();

        $activeBooking = new Booking();
        $activeBooking->StartTime = $now;
        $activeBooking->EndTime = $now;
        $activeBooking->save();

        $foundBookings = GetActiveBookingsBetweenDatesUseCase::create()->execute($now, $now, true);
        verify($foundBookings->count())->equals(1);
    }
}