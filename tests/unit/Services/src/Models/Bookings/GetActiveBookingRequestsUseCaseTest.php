<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 20/02/2018
 * Time: 22:28
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\src\Models\Bookings;

use PHPUnit\Framework\Assert;
use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Exceptions\ModelConsistencyValidationException;
use Rhubarb\Stem\Exceptions\ModelException;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetActiveBookingRequestsUseCase;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class GetActiveBookingRequestsUseCaseTest extends FlorpTestCase
{
    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws ModelConsistencyValidationException
     * @throws ModelException
     */
    public function testCanFindActiveBookings()
    {
        $now = new RhubarbDateTime("tomorrow");

        $bookingRequest = new BookingRequest();
        $bookingRequest->StartTime = $now;
        $bookingRequest->EndTime = $now;
        $bookingRequest->save();

        Assert::equalTo($bookingRequest->Id)->evaluate(GetActiveBookingRequestsUseCase::create()->execute()[0]->Id);
    }
}