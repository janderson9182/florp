<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 20-Jan-18
 * Time: 17:57
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\src\Models\Bookings;


use PHPUnit\Framework\Assert;
use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetActiveBookingsUseCase;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetLockableBookingsUseCase;
use SpaceCadets\Florp\Services\src\Models\Bookings\LockBookingsUseCase;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class GetActiveBookingsUseCaseTest extends FlorpTestCase
{
    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testBookingLocks()
    {
        $futureBooking = new Booking();
        $futureBooking->StartTime = new RhubarbDateTime("+ 19 hours");
        $futureBooking->save();

        $futureBooking2 = new Booking();
        $futureBooking2->StartTime = new RhubarbDateTime("+ 19 hours");
        $futureBooking2->save();

        $futureBooking2 = new Booking();
        $futureBooking2->StartTime = new RhubarbDateTime("- 19 hours");
        $futureBooking2->save();

        LockBookingsUseCase::create()->execute();
        $ActiveBookingsList = GetActiveBookingsUseCase::create()->execute();
        $this->assertEquals(2, $ActiveBookingsList->count());
    }
}