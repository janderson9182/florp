<?php

namespace SpaceCadets\Florp\Tests\Unit\Services\src\Models\Bookings;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Bookings\GetLockableBookingsUseCase;
use SpaceCadets\Florp\Services\src\Models\Bookings\LockBookingsUseCase;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class GetLockableBookingsUseCaseTest extends FlorpTestCase
{
    public function testBookingLocks()
    {
        $futureBooking = new Booking();
        $futureBooking->StartTime = new RhubarbDateTime("+ 20 hours");
        $futureBooking->EndTime = new RhubarbDateTime("+ 28 hours");
        $futureBooking->save();

        $futureBooking2 = new Booking();
        $futureBooking2->StartTime = new RhubarbDateTime("+ 19 hours");
        $futureBooking2->EndTime = new RhubarbDateTime("+ 27 hours");
        $futureBooking2->save();

        LockBookingsUseCase::create()->execute();
        $lockedBookingsList = GetLockableBookingsUseCase::create()->execute();
        $LockedBooking = null;
        foreach ($lockedBookingsList as $booking) {
            $LockedBooking = $booking;
            $this->assertEquals(true, $LockedBooking->locked);
        }

    }
}