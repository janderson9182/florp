<?php

namespace SpaceCadets\Florp\Tests\Unit\Services\src\Models\Bookings\BookingRequests;

use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\src\Models\Bookings\BookingRequests\GetBookingRequestsForUserUseCase;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\unit\src\Models\User\UserTestHelper;

class GetBookingRequestsForUserUseCaseTest extends FlorpTestCase
{
    use UserTestHelper;

    public function testGetBookingRequestsForUserUseCaseExecuteMethodExists()
    {
        self::classHasAttribute("execute");
    }

    public function testGetBookingRequestsForUserUseCaseExecuteMethodReturnsCollectionOfBookingRequests()
    {
        $user = $this->getUserModel(true);

        $user->addBookingRequest((new BookingRequest()));
        $user->addBookingRequest((new BookingRequest()));

        $this->isInstanceOf(BookingRequest::class)->evaluate(GetBookingRequestsForUserUseCase::create()->execute($user)[0]);
    }

    public function testGetBookingRequestsForUserUseCaseExecuteMethodReturnsCollectionOfBookingRequestsForUser()
    {
        $user = $this->getUserModel(true);

        $bookingRequest = new BookingRequest();
        $bookingRequest->save();
        $user->addBookingRequest($bookingRequest);

        $bookingRequest = new BookingRequest();
        $bookingRequest->save();
        $user->addBookingRequest($bookingRequest);

        $anotherUser = $this->getUserModel();
        $anotherUser->Username = "User2";
        $anotherUser->save();

        $bookingRequest = new BookingRequest();
        $bookingRequest->save();
        $anotherUser->addBookingRequest($bookingRequest);

        $bookingRequests = GetBookingRequestsForUserUseCase::create()->execute($user);
        $this->assertEquals(2, $bookingRequests->count());

        foreach ($bookingRequests as $bookingRequest) {
            $this->assertEquals($user->UserID, $bookingRequest->UserId);
        }
    }
}