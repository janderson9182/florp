<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 14/01/18
 * Time: 12:52
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\src\Models\Bookings;

use PHPUnit\Framework\Assert;
use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\src\Models\Bookings\CreateBookingFromRequestUseCase;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingHelper;
use SpaceCadets\Florp\Tests\unit\src\Models\User\UserTestHelper;

class CreateBookingFromRequestUseCaseTest extends FlorpTestCase
{
    use UserTestHelper;
    use BookingHelper;

    /**
     * @throws \Exception
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testBookingMadeFromRequest()
    {
        $bookingReq = new BookingRequest();
        $startTime = new RhubarbDateTime("2030-10-10 13:05:00");
        $bookingReq->StartTime = $startTime;
        $endTime = new RhubarbDateTime("2030-10-10 18:05:00");
        $bookingReq->EndTime = $endTime;
        $bookingReq->Duration = 90;
        $bookingReq->save();
        $booking = CreateBookingFromRequestUseCase::create()->execute($bookingReq);
        $bookingReq->reload();
        $this->assertEquals($booking->StartTime, $bookingReq->StartTime);
        $this->assertEquals($bookingReq->StartTime, $startTime, "The BookingRequest StartTime should remain the same");
        $this->assertEquals($bookingReq->EndTime, $endTime, "The BookingRequest EndTime should remain the same");
        $this->isInstanceOf(RhubarbDateTime::class)->evaluate($booking->StartTime);
        $this->isInstanceOf(RhubarbDateTime::class)->evaluate($booking->EndTime);
        $this->isFalse()->evaluate($booking->Locked);
        $this->assertEquals($booking->Id, $bookingReq->BookingId);

        $bookingReq = new BookingRequest();
        $bookingReq->StartTime = new RhubarbDateTime("-1 day");
        $bookingReq->EndTime = new RhubarbDateTime("-1 day");
        $booking = CreateBookingFromRequestUseCase::create()->execute($bookingReq);
        $this->isTrue()->evaluate($booking->Locked);
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     * @throws \Exception
     */
    public function testBasicBookingRequestDetailsAreCopiedOver()
    {
        // To ensure that the BookingID at the end is higher than 1
        $this->getBookingModel(true);

        $user = $this->getUserModel(true);
        $bookingRequest = new BookingRequest();
        $bookingRequest->UserId = 1;
        $bookingRequest->StartTime = new RhubarbDateTime("now");
        $bookingRequest->EndTime = new RhubarbDateTime("now +5 minutes");
        $bookingRequest->NumberOfSeats = 20;
        $bookingRequest->Cancelled = false;
        $bookingRequest->WillingToShare = false;
        $bookingRequest->save();

        $booking = CreateBookingFromRequestUseCase::create()->execute($bookingRequest);
        $bookingRequest->reload();

        /*
         * Direct Match from Booking Request -> Booking
         */
        Assert::assertEquals(
            $bookingRequest->UserId,
            $booking->UserId,
            "UserID should match The Booking Request"
        );
        Assert::assertEquals(
            $bookingRequest->Id,
            $booking->BookingRequestId,
            "BookingRequestId should match The Booking Request"
        );
        Assert::assertEquals(
            $bookingRequest->StartTime,
            $booking->StartTime,
            "StartTime should match The Booking Request"
        );
        Assert::assertEquals(
            $bookingRequest->EndTime,
            $booking->EndTime,
            "EndTime should match The Booking Request"
        );
        Assert::assertEquals(
            $bookingRequest->NumberOfSeats,
            $booking->NumberOfSeats,
            "NumberOfSeats should match The Booking Request"
        );
        Assert::assertEquals(
            $bookingRequest->Cancelled,
            $booking->Cancelled,
            "Cancelled should match The Booking Request"
        );
        Assert::assertEquals(
            $bookingRequest->WillingToShare,
            $booking->WillingToShare,
            "WillingToShare should match The Booking Request"
        );
        Assert::assertEquals(
            $bookingRequest->CheckedIn,
            $booking->CheckedIn,
            "CheckedIn should match The Booking Request"
        );
        Assert::assertEquals(
            $user->UserID,
            $booking->UserId,
            "UserId should match The Booking Request"
        );

        /*
         * Ensure that the import didn't mess up IDs
         */

        Assert::assertEquals(
            2,
            Booking::all()->count(),
            "There should be two bookings created at this point"
        );
        Assert::assertEquals(
            1,
            BookingRequest::all()->count(),
            "There should only be one booking request created at this point"
        );

        Assert::assertNotEquals(
            $booking->Id,
            $bookingRequest->Id,
            "The Id of the Booking should not be the same as the BookingRequest"
        );
        /*
         * Ensure that The Booking details are passed back to the Booking Request
         */
        Assert::assertEquals(
            $bookingRequest->BookingId,
            $booking->Id,
            "BookingRequest should have the same BookingID as the Booking"
        );
    }
}