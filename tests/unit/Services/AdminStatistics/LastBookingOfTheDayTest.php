<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 29/03/18
 * Time: 11:38
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\AdminStatistics;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Services\Statistic\LastBookingTodayStatistic;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingHelper;

class LastBookingOfTheDayTest extends FlorpTestCase
{
    use BookingHelper;

    public function testWillReturnLastBookingTodayNotAnyOthers(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->RoomId=$room->Id;
        $secondBooking=$this->getBookingModel();
        $secondBooking->StartTime = new RhubarbDateTime("+1 hour");
        $secondBooking->RoomId=$room->Id;

        $this->assertEquals($secondBooking->StartTime->format('H:i:s'),(new LastBookingTodayStatistic())->getResult());
    }
    public function testWillReturnStringIfNoBookingsToday(){
        $this->assertEquals("No Bookings Today",(new LastBookingTodayStatistic())->getResult());
    }
}