<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 30/03/18
 * Time: 12:22
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\AdminStatistics;


use SpaceCadets\Florp\Services\Statistic\BookingCountAbstractStatistic;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingHelper;
use SpaceCadets\Florp\Tests\Unit\src\Models\FloorTest;

class TotalCountOfBookingsTest extends FlorpTestCase
{
    use BookingHelper;
    use StatisticHelper;

    public function testWillReturnCountOfBookings(){
        $arr[]=$this->getLargeBookingModel();
        $this->assertEquals(5,(new BookingCountAbstractStatistic())->getResult());
    }
    public function testWillReturn0IfNoBookings(){
        $this->assertEquals(0,(new BookingCountAbstractStatistic())->getResult());
    }
    public function testWillReturn0IfBookingHasBeenCancelled(){
        $booking=$this->getBookingModel();
        $booking->delete();
        $this->assertEquals(0,(new BookingCountAbstractStatistic())->getResult());
    }
}