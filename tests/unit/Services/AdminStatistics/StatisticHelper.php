<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 30/03/18
 * Time: 01:54
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\AdminStatistics;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Bookings\Booking;

trait StatisticHelper
{
    public function getLargeBookingModel($save = true): array
    {
        $booking = new Booking();
        $booking->StartTime=new RhubarbDateTime("now -2hours");
        $booking->EndTime=new RhubarbDateTime("now -1hours");
        $booking->save();
        $booking->reload();
        $booking2 = new Booking();
        $booking2->StartTime=new RhubarbDateTime("now -2hours");
        $booking2->EndTime=new RhubarbDateTime("now -1hours");
        $booking2->save();
        $booking2->reload();
        $booking3 = new Booking();
        $booking3->StartTime=new RhubarbDateTime("now -2hours");
        $booking3->EndTime=new RhubarbDateTime("now -1hours");
        $booking3->save();
        $booking3->reload();
        $booking4 = new Booking();
        $booking4->StartTime=new RhubarbDateTime("now -2hours");
        $booking4->EndTime=new RhubarbDateTime("now -1hours");
        $booking4->save();
        $booking4->reload();
        $booking5 = new Booking();
        $booking5->StartTime=new RhubarbDateTime("now -2hours");
        $booking5->EndTime=new RhubarbDateTime("now -1hours");
        $booking5->save();
        $booking5->reload();
        if ($save) {
            $booking->save();
            $booking2->save();
            $booking3->save();
            $booking4->save();
            $booking5->save();

        }
        $arr[]=$booking;
        array_push($arr,$booking2);
        array_push($arr,$booking3);
        array_push($arr,$booking4);
        array_push($arr,$booking5);

        return $arr;
    }
}