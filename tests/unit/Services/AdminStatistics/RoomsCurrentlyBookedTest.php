<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 29/03/18
 * Time: 12:20
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\AdminStatistics;


use SpaceCadets\Florp\Services\Statistic\RoomsCurrentlyBookedStatistic;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingHelper;

class RoomsCurrentlyBookedTest extends FlorpTestCase
{
    use BookingHelper;

    public function testWillReturnRoomsOccupiedRightNow(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->RoomId=$room->Id;
        $booking->checkIn();
        $booking->save();
        $room->save();
        $booking->reload();
        $room->reload();
        $this->assertEquals(["CheckInRoom"],(new RoomsCurrentlyBookedStatistic())->getResult());
    }

    public function testWillNotReturnRoomsNotOccupiedRightNow(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->RoomId=$room->Id;
        $booking->save();
        $room->save();
        $booking->reload();
        $room->reload();
        $this->assertEquals(["No Rooms Currently Checked In"],(new RoomsCurrentlyBookedStatistic())->getResult());
    }

    public function testWillReturnMultipleRoomsOccupied(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->RoomId=$room->Id;
        $booking->checkIn();

        $room2=$this->getRoomModel();
        $room2->Name = "CheckInRoom2";
        $booking2=$this->getBookingModel();
        $booking2->RoomId=$room2->Id;
        $booking2->checkIn();

        $booking->save();
        $booking2->save();
        $room->save();
        $room2->save();
        $booking->reload();
        $booking2->reload();
        $room->reload();
        $room2->reload();
        $this->assertEquals([$room,$room2],(new RoomsCurrentlyBookedStatistic())->getResult());
    }

    public function testWillIgnoreOtherBookingsNotOccupied(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->RoomId=$room->Id;
        $booking->checkIn();
        $roomNotBooked=$this->getRoomModel();
        $roomNotBooked->Name = "NotCheckInRoom";
        $booking->save();
        $room->save();
        $roomNotBooked->save();
        $booking->reload();
        $room->reload();
        $roomNotBooked->reload();
        $this->assertEquals(["CheckInRoom"],(new RoomsCurrentlyBookedStatistic())->getResult());
    }
}