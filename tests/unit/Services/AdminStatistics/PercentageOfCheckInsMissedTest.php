<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 29/03/18
 * Time: 22:46
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\AdminStatistics;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Services\Statistic\PercentageOfCheckInsMissedInTotalStatistic;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingHelper;

class PercentageOfCheckInsMissedTest extends FlorpTestCase
{
    use BookingHelper;
    use StatisticHelper;

    public function testWillReturnPercentageOfCheckInsMissed(){
            $arr=$this->getLargeBookingModel();
        for($i=0;$i<2;$i++){
            $arr[$i]->checkIn();
            $arr[$i]->save();
            $arr[$i]->reload();
        }
           $this->assertEquals("150%",(new PercentageOfCheckInsMissedInTotalStatistic())->getResult());
        }
    public function testWillReturnAppropriateResponseWhenNoDataAvailable(){
        $this->assertEquals("Not enough Bookings in the system",(new PercentageOfCheckInsMissedInTotalStatistic())->getResult());
    }
    public function testWillReturnAppropriateResponseWhenAllBookingsAreCheckedIn(){
        $arr=$this->getLargeBookingModel();
        for($i=0;$i<count($arr);$i++){
            $arr[$i]->checkIn();
            $arr[$i]->save();
            $arr[$i]->reload();
        }
        $this->assertEquals("0%",(new PercentageOfCheckInsMissedInTotalStatistic())->getResult());
    }
}