<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 29/03/18
 * Time: 12:10
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\AdminStatistics;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Services\Statistic\GraphOfBookingsStatistic;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingHelper;

class GraphOfBookingsStatisticsTest extends FlorpTestCase
{
    use BookingHelper;

    public function testWillReturnBookingsToday(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->RoomId=$room->Id;
        $booking->save();
        $booking->reload();
        $this->assertEquals([$booking],(new GraphOfBookingsStatistic())->getResult());
    }
    public function testWillNotReturnBookingsOutsideOfToday(){
        $room=$this->getRoomModel();

        //This booking is today and so it will be the only booking returned
        $booking=$this->getBookingModel();
        $booking->RoomId=$room->Id;
        $booking->save();
        $booking->reload();

        //This bookings is not today and will not be returned
        $yesterdaysBooking=$this->getBookingModel();
        $yesterdaysBooking->RoomId=$room->Id;
        $yesterdaysBooking->StartTime=(new RhubarbDateTime("yesterday -2hour"));
        $yesterdaysBooking->EndTime=(new RhubarbDateTime("yesterday -1hour"));
        $yesterdaysBooking->save();
        $yesterdaysBooking->reload();
        $this->assertNotEquals([$yesterdaysBooking],(new GraphOfBookingsStatistic())->getResult());
        $this->assertEquals([$booking],(new GraphOfBookingsStatistic())->getResult());
    }
    public function testWillReturnEmptyArrayIfNoBookingsToday(){
        //The reason this can be empty is because google charts will not display is there are no bookings
        $this->assertEquals([],(new GraphOfBookingsStatistic())->getResult());
    }
}