<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 29/03/18
 * Time: 20:16
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\AdminStatistics;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Services\Statistic\HowManyCheckInsMissedTodayStatistic;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingHelper;

class HowManyCheckInsMissedTodayTest extends FlorpTestCase
{
    use BookingHelper;

    public function testWillReturnCountOfMissedBookingsToday(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->StartTime=new RhubarbDateTime("now -2 hours");
        $booking->EndTime=new RhubarbDateTime("now -1hours");
        $booking->RoomId=$room->Id;
        $booking->save();
        $booking->reload();
        $this->assertEquals(1,(new HowManyCheckInsMissedTodayStatistic())->getResult());
    }

    public function testWillNotReturnCheckedInBookingsToday(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->StartTime=new RhubarbDateTime("now - 2hours");
        $booking->EndTime=new RhubarbDateTime("now - 1hours");
        $booking->RoomId=$room->Id;
        $booking->checkIn();
        $booking->save();
        $booking->reload();
        $this->assertEquals(0,(new HowManyCheckInsMissedTodayStatistic())->getResult());
    }
    public function testWillNotReturnBookingsLaterToday(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->StartTime=new RhubarbDateTime("now + 2hours");
        $booking->EndTime=new RhubarbDateTime("now + 1hours");
        $booking->RoomId=$room->Id;
        $booking->save();
        $booking->reload();
        $this->assertEquals(0,(new HowManyCheckInsMissedTodayStatistic())->getResult());
    }


}