<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 29/03/18
 * Time: 02:09
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\AdminStatistics;


use SpaceCadets\Florp\Services\Statistic\BookingCountAbstractStatistic;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingHelper;

class numberOfBookingsTodayTest extends FlorpTestCase
{
    use BookingHelper;

    public function testWillCountBookingsToday(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->RoomId=$room->Id;
        $booking->save();
        $booking->reload();
        $booking2=$this->getBookingModel();
        $booking2->RoomId=$room->Id;
        $booking2->save();
        $booking2->reload();
        $booking3=$this->getBookingModel();
        $booking3->RoomId=$room->Id;
        $booking3->save();
        $booking3->reload();
        $this->assertEquals(3,(new BookingCountAbstractStatistic())->getResult());
    }
    public function testWillReturn0IfNoBookingsToday(){
        $this->assertEquals(0,(new BookingCountAbstractStatistic())->getResult());
    }
}