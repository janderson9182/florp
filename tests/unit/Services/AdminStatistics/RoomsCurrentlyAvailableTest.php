<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 29/03/18
 * Time: 12:32
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\AdminStatistics;


use SpaceCadets\Florp\Services\Statistic\RoomsCurrentlyAvailableStatistic;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingHelper;

class RoomsCurrentlyAvailableTest extends FlorpTestCase
{
    use BookingHelper;

    public function testWillReturnAllRoomsAvailableRightNowIfNoBookingsAreMade(){
        //NO bookings in the database
        $room1=$this->getRoomModel();
        $room2=$this->getRoomModel();
        $room2->Name="CheckInRoom2";
        $room3=$this->getRoomModel();
        $room3->Name="CheckInRoom3";
        $room1->save();
        $room2->save();
        $room3->save();
        $this->assertEquals([$room1,$room2,$room3],(new RoomsCurrentlyAvailableStatistic())->getResult());
    }
    public function testWillReturnRoomsAvailableRightNowIfBookingsAreMade(){
        $room1=$this->getRoomModel();
        $room2=$this->getRoomModel();
        $room2->Name="CheckInRoom2";
        $room3=$this->getRoomModel();
        $room3->Name="CheckInRoom3";
        $booking=$this->getBookingModel();
        $booking->RoomId=$room1->Id;
        $booking->save();
        $room1->save();
        $booking->reload();
        $room1->reload();
        $room2->save();
        $room3->save();
        $this->assertEquals([$room2,$room3],(new RoomsCurrentlyAvailableStatistic())->getResult());
    }

    public function testWillReturnAppropriateResponseIfNoRoomsExistInDatabase(){
        $this->assertEquals(["No Rooms Currently In the System"],(new RoomsCurrentlyAvailableStatistic())->getResult());
    }

    public function testWillReturnNoRoomsIfNoneAreAvailable(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->RoomId=$room->Id;
        $booking->save();
        $room->save();
        $booking->reload();
        $room->reload();
        $this->assertEquals(["No Rooms Currently Available"],(new RoomsCurrentlyAvailableStatistic())->getResult());
    }

}