<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 29/03/18
 * Time: 02:18
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\AdminStatistics;


use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Services\Statistic\FirstBookingTodayStatistic;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingHelper;

class firstBookingOfTheDayTest extends FlorpTestCase
{
    use BookingHelper;

    public function testWillReturnFirstBookingTodayNotAnyOthers(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->RoomId=$room->Id;
        $secondBooking=$this->getBookingModel();
        $secondBooking->StartTime = new RhubarbDateTime("+1 hour");
        $secondBooking->RoomId=$room->Id;

        $this->assertEquals($booking->StartTime->format('H:i:s'),(new FirstBookingTodayStatistic())->getResult());
    }
    public function testWillReturnStringIfNoBookingsToday(){
        $this->assertEquals("No Bookings Today",(new FirstBookingTodayStatistic())->getResult());
    }
    public function testWillOnlyReturnBookingsFromToday(){
        $room=$this->getRoomModel();
        $booking=$this->getBookingModel();
        $booking->RoomId=$room->Id;
        $tomorrowBooking=$this->getBookingModel();
        $tomorrowBooking->StartTime = new RhubarbDateTime("tomorrow -1 hour");
        $tomorrowBooking->EndTime = new RhubarbDateTime("tomorrow +1 hour");
        $tomorrowBooking->RoomId=$room->Id;
        $yesterdayBooking=$this->getBookingModel();
        $yesterdayBooking->StartTime = new RhubarbDateTime("yesterday -1 hour");
        $yesterdayBooking->StartTime = new RhubarbDateTime("yesterday +1 hour");
        $yesterdayBooking->RoomId=$room->Id;
        $this->assertEquals($booking->StartTime->format('H:i:s'),(new FirstBookingTodayStatistic())->getResult());
    }
}