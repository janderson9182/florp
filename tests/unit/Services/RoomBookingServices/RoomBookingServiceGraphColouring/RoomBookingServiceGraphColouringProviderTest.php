<?php
/**
 * Created by PhpStorm.
 * User: ron_0
 * Date: 15/04/2018
 * Time: 22:29
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\RoomBookingServices\RoomBookingServiceGraphColouring;

use PHPUnit\Framework\Assert;
use SpaceCadets\Florp\Services\RoomBookingServices\GraphColouring\RoomBookingServiceGraphColouringProvider;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class RoomBookingServiceGraphColouringProviderTest extends FlorpTestCase
{

    public function testGetsName(){
        $roomBookingService = new RoomBookingServiceGraphColouringProvider();
        Assert::assertEquals("Graph Colouring Sort", $roomBookingService->getName(), "Graph colouring sort name returned correctly.");
    }

    public function testGetsDescription(){

        $roomBookingService = new RoomBookingServiceGraphColouringProvider();
        $roomBookingService->getDescription();
        Assert::assertEquals("When a user creates a booking request, the system will add the booking request to a graph"
            . "data structure as a vertex. The system then attempts to colour the graph with a set of 'colours'."
            . "These 'colours' are the rooms available. An edge in the graph will be created between two"
            . "booking request vertices if the two booking requests overlap (same time, date and room).",$roomBookingService->getDescription(),
            "Graph colouring sort description returned correctly.")
        ;
    }

}
