<?php
/**
 * Created by PhpStorm.
 * User: ron_0
 * Date: 15/04/2018
 * Time: 22:59
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\RoomBookingServices\RoomBookingServiceGraphColouring;


use SpaceCadets\Florp\Services\RoomBookingServices\GraphColouring\Graph;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use PHPUnit\Framework\Assert;

class GraphTest extends FlorpTestCase
{

    public function testEmptyGraphCreates(){
        $testGraph = new Graph(0);
        Assert::assertEquals(true, $testGraph->adjacencyList->isEmpty(), "Graph created with zero vertices as expected.");
    }

    public function testGraphCreatesWithOneVertex(){
        $testGraph = new Graph(1);
        Assert::assertEquals(1, $testGraph->adjacencyList->count(), "Graph created with one vertex as expected.");
    }

    public function testGraphCreatesWithMultipleVertices(){
        $testGraph = new Graph(5);
        Assert::assertEquals(5, $testGraph->adjacencyList->count(), "Graph created with multiple vertices as expected.");
    }

    public function testAddEdgeToGraph(){
        $testGraph = new Graph(2);
        $testGraph->addEdge(0,1);
        Assert::assertEquals(1, $testGraph->adjacencyList->bottom()->bottom(), "Second vertex successfully linked to first vertex.");
        Assert::assertEquals(0, $testGraph->adjacencyList->top()->bottom(), "First vertex successfully linked to second vertex.");
    }

}
