<?php

namespace SpaceCadets\Florp\Tests\Unit\Services\RoomBookingServices;

use PHPUnit\Framework\Assert;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingService;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceSmallestFirst\RoomBookingServiceSmallestFirstProvider;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class RoomBookingServiceTest extends FlorpTestCase
{
    public function testRoomBookingServiceStaticGetterAndSetterForClasses()
    {
        $classToRegister = UnitTestingRoomBookingService::class;
        $classNiceName = UnitTestingRoomBookingService::getName();
        RoomBookingService::registerRoomBookingServiceClass($classToRegister);
        Assert::equalTo(UnitTestingRoomBookingService::getName())
            ->evaluate(RoomBookingService::getRegisteredClasses()[$classNiceName]::getName());
        Assert::equalTo(UnitTestingRoomBookingService::getDescription())
            ->evaluate(RoomBookingService::getRegisteredClasses()[$classNiceName]::getDescription());
        Assert::stringContains(UnitTestingRoomBookingService::class)
            ->evaluate(RoomBookingService::findClassByName($classNiceName));
    }
}