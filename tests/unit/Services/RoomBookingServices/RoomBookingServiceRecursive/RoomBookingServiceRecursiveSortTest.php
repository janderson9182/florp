<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 27/03/18
 * Time: 18:45
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\RoomBookingServices\RoomBookingServiceRecursive;

use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceRecursive\RoomBookingServiceRecursiveProvider;
use SpaceCadets\Florp\Tests\Unit\Services\Assets\Room\RoomTestHelper;
use PHPUnit\Framework\TestResult;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use PHPUnit\Framework\Assert;
use Rhubarb\Crown\DateTime\RhubarbDate;
use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Scaffolds\BackgroundTasks\Models\BackgroundTaskStatus;
use SpaceCadets\Florp\FlorpSettings;
use SpaceCadets\Florp\Models\Assets\Properties\Property;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingService;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceSmallestFirst\RoomBookingServiceSmallestFirstProvider;
use SpaceCadets\Florp\Tests\Unit\src\Models\BackgroundTaskHelper;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingHelper;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingRequestHelper;
use SpaceCadets\Florp\Tests\unit\src\Models\User\UserTestHelper;


class RoomBookingServiceRecursiveSortTest extends FlorpTestCase
{

    use UserTestHelper;
    use RoomTestHelper;
    use BookingRequestHelper;
    use BackgroundTaskHelper;

    /**
     * @throws \Exception
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testShuffleTriesToHappen()
    {
        // Create 3 Rooms
        $room1 = $this->getRoomModel();
        $room1->NumberOfSeats = 10;
        $room1->save();

        $room2 = $this->getRoomModel();
        $room2->NumberOfSeats = 20;
        $room2->save();

        $room3 = $this->getRoomModel();
        $room3->NumberOfSeats = 40;
        $room3->save();

        // Put a booking in the second 2 room between 3 pm and 4 pm
        $startTime = new RhubarbDateTime("tomorrow 3pm");
        $endTime = new RhubarbDateTime("tomorrow 4pm");

        $bookingRequestRoom2 = new BookingRequest();
        $bookingRequestRoom2->StartTime = $startTime;
        $bookingRequestRoom2->EndTime = $endTime;
        $bookingRequestRoom2->NumberOfSeats = 5;
        $bookingRequestRoom2->save();

        $bookingRoom2 = new Booking();
        $bookingRoom2->StartTime = $startTime;
        $bookingRoom2->EndTime = $endTime;
        $bookingRoom2->RoomId = $room2->Id;
        $bookingRoom2->BookingRequestId = $bookingRequestRoom2->Id;
        $bookingRoom2->save();
        $bookingRequestRoom2->BookingId = $bookingRoom2->Id;
        $bookingRequestRoom2->save();

        $bookingRequestRoom3 = new BookingRequest();
        $bookingRequestRoom3->StartTime = $startTime;
        $bookingRequestRoom3->EndTime = $endTime;
        $bookingRequestRoom3->NumberOfSeats = 7;
        $bookingRequestRoom3->save();

        $bookingRoom3 = new Booking();
        $bookingRoom3->StartTime = $startTime;
        $bookingRoom3->EndTime = $endTime;
        $bookingRoom3->RoomId = $room3->Id;
        $bookingRoom3->BookingRequestId = $bookingRequestRoom3->Id;
        $bookingRoom3->save();
        $bookingRequestRoom3->BookingId = $bookingRoom3->Id;
        $bookingRequestRoom3->save();

        $bookingRequestNewShuffled = new BookingRequest();
        $bookingRequestNewShuffled->StartTime = $startTime;
        $bookingRequestNewShuffled->EndTime = $endTime;
        $bookingRequestNewShuffled->NumberOfSeats = 20;
        $bookingRequestNewShuffled->save();

        $roomBookingService = new RoomBookingServiceRecursiveProvider();
        $shuffleWorked = $roomBookingService->execute($this->getBackgroundTaskForBookingRequest($bookingRequestNewShuffled->Id));

        Assert::assertTrue($shuffleWorked, "The shuffle should have worked");

        // reload to get the updated values from the data repository
        $bookingRequestNewShuffled->reload();

        // Check that the original bookings have new rooms
        $bookingRoom2RoomIdBefore = $bookingRoom2->RoomId;
        $bookingRoom3RoomIdBefore = $bookingRoom3->RoomId;

        $bookingRoom2->reload();
        $bookingRoom3->reload();

        Assert::assertNotEquals($bookingRoom2RoomIdBefore, $bookingRoom2->RoomId, "Booking in room 2 should have moved");
    }

    public function testGetsDescription(){

        $roomBookingService = new RoomBookingServiceRecursiveProvider();
        $roomBookingService->getDescription();
        Assert::assertEquals("This sorting class recursively re-arranges bookings 
                to make as many as possible fit in a timetabled block.",$roomBookingService->getDescription(),
            "Recursive sort description returned correctly.")
        ;
    }

}