<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 20/02/2018
 * Time: 20:23
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\RoomBookingServices;

use Rhubarb\Scaffolds\BackgroundTasks\Models\BackgroundTaskStatus;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingService;

class UnitTestingRoomBookingService extends RoomBookingService
{
    /**
     * Executes the long running code.
     *
     * @param BackgroundTaskStatus $status
     * @return void
     */
    public function execute(BackgroundTaskStatus $status)
    {
    }

    public static function getName(): string
    {
        return "Unit Testing Name";
    }

    public static function getDescription(): string
    {
        return "Unit Testing Description";
    }
}