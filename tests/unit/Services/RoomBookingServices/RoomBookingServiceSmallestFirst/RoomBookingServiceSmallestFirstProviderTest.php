<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 14/02/2018
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\RoomBookingServices\RoomBookingServiceSmallestFirst;

use PHPUnit\Framework\Assert;
use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\FlorpSettings;
use SpaceCadets\Florp\Models\Assets\Properties\Property;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceSmallestFirst\RoomBookingServiceSmallestFirstProvider;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\Services\Assets\Room\RoomTestHelper;
use SpaceCadets\Florp\Tests\Unit\src\Models\BackgroundTaskHelper;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingRequestHelper;
use SpaceCadets\Florp\Tests\unit\src\Models\User\UserTestHelper;

class RoomBookingServiceSmallestFirstProviderTest extends FlorpTestCase
{
    use UserTestHelper;
    use RoomTestHelper;
    use BookingRequestHelper;
    use BackgroundTaskHelper;

    public function testClassExists()
    {
        new RoomBookingServiceSmallestFirstProvider();
    }

    public function testServiceIsAccessible()
    {
        FlorpSettings::singleton()->roomBookingProviderClass = RoomBookingServiceSmallestFirstProvider::class;

        Assert::stringContains(RoomBookingServiceSmallestFirstProvider::class)
            ->evaluate(
                FlorpSettings::singleton()->roomBookingProviderClass,
                "The RoomBookingServiceSmallestFirstProvider should be the registered class"
            );

        Assert::stringContains("First", RoomBookingServiceSmallestFirstProvider::getName());
        Assert::stringContains("First", RoomBookingServiceSmallestFirstProvider::getDescription());
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     * @throws \Rhubarb\Stem\Exceptions\RecordNotFoundException
     */
    public function testFirstObviousRoomIsFound()
    {
        $size = 5;
        $room = $this->getRoomOfSize($size);
        $bookingRequest = $this->createBookingRequestOfSize($size);
        $backgroundTaskStatus = $this->getBackgroundTaskForBookingRequest($bookingRequest->Id);

        $roomBookingProvider = new RoomBookingServiceSmallestFirstProvider();
        $roomBookingProvider->execute($backgroundTaskStatus);

        verify(Booking::findFirst()->Room->Id)->equals($room->Id);
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     * @throws \Rhubarb\Stem\Exceptions\RecordNotFoundException
     */
    public function testSmallestObviousRoomIsFilledFirst()
    {
        $size = 5;
        $this->getRoomOfSize(7);
        $smallestRoom = $this->getRoomOfSize($size);
        $this->getRoomOfSize(6);

        $bookingRequest = $this->createBookingRequestOfSize($size);

        $backgroundTaskStatus = $this->getBackgroundTaskForBookingRequest($bookingRequest->Id);

        $roomBookingProvider = new RoomBookingServiceSmallestFirstProvider();
        $roomBookingProvider->execute($backgroundTaskStatus);

        verify(Booking::findFirst()->Room->Id)->equals($smallestRoom->Id);
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testNoBookingIsCreatedIfNoLargeEnoughRoomsAreFound()
    {
        $this->getRoomOfSize(5);
        $bookingRequest = $this->createBookingRequestOfSize(6);
        $backgroundTaskStatus = $this->getBackgroundTaskForBookingRequest($bookingRequest->Id);
        $roomBookingProvider = new RoomBookingServiceSmallestFirstProvider();
        $roomBookingProvider->execute($backgroundTaskStatus);

        verify(Booking::all()->count())->equals(0);
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testRoomBookingCanBeCreatedAfterCancellation()
    {
        $roomWith10Seats = new Room();
        $roomWith10Seats->NumberOfSeats = 10;
        $roomWith10Seats->save();

        $roomWith20Seats = new Room();
        $roomWith20Seats->NumberOfSeats = 20;
        $roomWith20Seats->save();

        /* >> Room Which Holds 20 people <<
         0:00               00:30                01:00
        |                   |                    |
        [           [///////////////]               ] <- Create a booking for 10 people here
         */
        $cancelledBooking = new Booking();
        $cancelledBooking->StartTime = new RhubarbDateTime("2017-01-01 00:15:00");
        $cancelledBooking->EndTime = new RhubarbDateTime("2017-01-01 00:45:00");
        $cancelledBooking->NumberOfSeats = 10;
        $cancelledBooking->RoomId = $roomWith20Seats->Id;
        $cancelledBooking->Cancelled = true;
        $cancelledBooking->save();

        $bookingRequest = new BookingRequest();
        $bookingRequest->NumberOfSeats = 10;
        $bookingRequest->StartTime = new RhubarbDateTime("2017-01-01 00:15:00");
        $bookingRequest->EndTime = new RhubarbDateTime("2017-01-01 00:45:00");
        $bookingRequest->save();

        $roomBookingService = new RoomBookingServiceSmallestFirstProvider();
        $roomBookingService->execute($this->getBackgroundTaskForBookingRequest($bookingRequest->Id));

        $bookingRequest->reload();

        $this->assertGreaterThan(
            0,
            $bookingRequest->BookingId,
            "A booking should have been created and saved on top of cancelledBooking"
        );
        $this->assertEquals(
            $roomWith10Seats->Id,
            $bookingRequest->Booking->RoomId,
            "The Booking RoomID should be the smaller room"
        );
    }

    public function testLockedBookingsCannotBeOverwritten()
    {
        $room = new Room();
        $room->NumberOfSeats = 10;
        $room->save();

        $lockedBooking = new Booking();
        $lockedBooking->StartTime = new RhubarbDateTime("2017-01-01 00:15:00");
        $lockedBooking->EndTime = new RhubarbDateTime("2017-01-01 00:45:00");
        $lockedBooking->NumberOfSeats = 10;
        $lockedBooking->RoomId = $room->Id;
        $lockedBooking->Locked = true;
        $lockedBooking->save();

        $bookingRequest = new BookingRequest();
        $bookingRequest->NumberOfSeats = 10;
        $bookingRequest->StartTime = new RhubarbDateTime("2017-01-01 00:15:00");
        $bookingRequest->EndTime = new RhubarbDateTime("2017-01-01 00:45:00");
        $bookingRequest->save();

        $roomBookingService = new RoomBookingServiceSmallestFirstProvider();
        $roomBookingService->execute($this->getBackgroundTaskForBookingRequest($bookingRequest->Id));

        $bookingRequest->reload();
        $this->assertNull($bookingRequest->BookingId, "A booking should not have been created");
    }

    /**
     * @throws \Exception
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testSmallestFirstConsidersASelectedProperty()
    {
        $projector = new Property();
        $projector->Name = "Data Projector";
        $projector->save();

        $smallerRoomWithoutProjector = new Room();
        $smallerRoomWithoutProjector->NumberOfSeats = 10;
        $smallerRoomWithoutProjector->save();

        $largerRoomWithProjector = new Room();
        $largerRoomWithProjector->NumberOfSeats = 20;
        $largerRoomWithProjector->save();
        $largerRoomWithProjector->addProperty($projector);

        $bookingRequest = new BookingRequest();
        $bookingRequest->NumberOfSeats = 10;
        $bookingRequest->StartTime = new RhubarbDateTime("2017-01-01 00:15:00");
        $bookingRequest->EndTime = new RhubarbDateTime("2017-01-01 00:45:00");
        $bookingRequest->save();
        $bookingRequest->addProperty($projector);

        $roomBookingService = new RoomBookingServiceSmallestFirstProvider();
        $roomBookingService->execute($this->getBackgroundTaskForBookingRequest($bookingRequest->Id));

        $bookingRequest->reload();

        $this->assertEquals($largerRoomWithProjector->Id, $bookingRequest->Booking->RoomId);

        /*
         * Multiple Properties
         */

        $speakers = new Property();
        $speakers->Name = "Speakers";
        $speakers->save();

        $largerRoomWithProjectorAndSpeakers = new Room();
        $largerRoomWithProjectorAndSpeakers->NumberOfSeats = 20;
        $largerRoomWithProjectorAndSpeakers->save();
        $largerRoomWithProjectorAndSpeakers->addProperty($projector);
        $largerRoomWithProjectorAndSpeakers->addProperty($speakers);

        $bookingRequest->addProperty($projector);
        $bookingRequest->addProperty($speakers);

        $roomBookingService = new RoomBookingServiceSmallestFirstProvider();
        $roomBookingService->execute($this->getBackgroundTaskForBookingRequest($bookingRequest->Id));

        $bookingRequest->reload();

        $this->assertEquals($largerRoomWithProjectorAndSpeakers->Id, $bookingRequest->Booking->RoomId);
    }
}