<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 14/04/2018
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\RoomBookingServices\RoomBookingServiceSmallestFirst;

use PHPUnit\Framework\Assert;
use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceSmallestFirst\RoomBookingServiceSmallestFirstProvider;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\src\Models\BackgroundTaskHelper;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingHelper;

class SmallestFirstShuffleUseCaseTest extends FlorpTestCase
{
    use BookingHelper;
    use BackgroundTaskHelper;

    /**
     * @throws \Exception
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function testShuffleTriesToHappen()
    {
        /* Create 3 Rooms */
        $room1 = $this->getRoomModel();
        $room1->NumberOfSeats = 5;
        $room1->save();

        $room2 = $this->getRoomModel();
        $room2->NumberOfSeats = 10;
        $room2->save();

        $room3 = $this->getRoomModel();
        $room3->NumberOfSeats = 20;
        $room3->save();

        $startTime = new RhubarbDateTime("tomorrow 3pm");
        $endTime = new RhubarbDateTime("tomorrow 4pm");

        /* Create and Cancel a booking for 3 people */
        $cancelledBooking = new Booking();
        $cancelledBooking->StartTime = $startTime;
        $cancelledBooking->EndTime = $endTime;
        $cancelledBooking->RoomId = $room1->Id;
        $cancelledBooking->NumberOfSeats = 3;
        $cancelledBooking->Cancelled = true;
        $cancelledBooking->RoomId = $room1->Id;
        $cancelledBooking->save();

        $cancelledRequest = new BookingRequest();
        $cancelledRequest->StartTime = $startTime;
        $cancelledRequest->EndTime = $endTime;
        $cancelledRequest->BookingId = $cancelledBooking->Id;
        $cancelledRequest->NumberOfSeats = 3;
        $cancelledRequest->Cancelled = true;
        $cancelledRequest->save();

        $cancelledBooking->BookingRequestId = $cancelledBooking->Id;
        $cancelledBooking->save();

        $failedRequest = new BookingRequest();
        $failedRequest->StartTime = $startTime;
        $failedRequest->EndTime = $endTime;
        $failedRequest->NumberOfSeats = 13;
        $failedRequest->Failed = true;
        $failedRequest->save();

        /* Put a booking in the second 2 room between 3 pm and 4 pm */
        $bookingRoom2 = new Booking();
        $bookingRoom2->StartTime = $startTime;
        $bookingRoom2->EndTime = $endTime;
        $bookingRoom2->RoomId = $room2->Id;
        $bookingRoom2->save();

        $bookingRoom3 = new Booking();
        $bookingRoom3->StartTime = $startTime;
        $bookingRoom3->EndTime = $endTime;
        $bookingRoom3->RoomId = $room3->Id;
        $bookingRoom3->save();

        /*
         * Associate a booking request with the Bookings
         * The number of seats is more than is needed, this is to simulate the order of rooms and bookings
         * If there was a cancellation
         */

        $bookingRequestRoom2 = new BookingRequest();
        $bookingRequestRoom2->BookingId = $bookingRoom2->Id;
        $bookingRequestRoom2->StartTime = $startTime;
        $bookingRequestRoom2->EndTime = $endTime;
        $bookingRequestRoom2->NumberOfSeats = 5;
        $bookingRequestRoom2->save();
        $bookingRoom2->BookingRequestId = $bookingRequestRoom2->Id;
        $bookingRoom2->save();

        $bookingRequestRoom3 = new BookingRequest();
        $bookingRequestRoom3->BookingId = $bookingRoom3->Id;
        $bookingRequestRoom3->StartTime = $startTime;
        $bookingRequestRoom3->EndTime = $endTime;
        $bookingRequestRoom3->NumberOfSeats = 9;
        $bookingRequestRoom3->save();
        $bookingRoom3->BookingRequestId = $bookingRequestRoom3->Id;
        $bookingRoom3->save();

        /*
         * $bookingRequestRoom2 & $bookingRequestRoom3 could have been put into smaller rooms, but lets say that this
         * Didn't happen because there wasn't space at the time in the first room.
         * Now we create a booking request for 30 people, and check that after a shuffle, they get put into $room3
         * And that the other two still have room ids in rooms for fewer
         */

        $bookingRequestNewShuffled = new BookingRequest();
        $bookingRequestNewShuffled->StartTime = $startTime;
        $bookingRequestNewShuffled->EndTime = $endTime;
        $bookingRequestNewShuffled->NumberOfSeats = 13;
        $bookingRequestNewShuffled->save();

        $roomBookingService = new RoomBookingServiceSmallestFirstProvider();
        $shuffleWorked = $roomBookingService->execute($this->getBackgroundTaskForBookingRequest($bookingRequestNewShuffled->Id));

        Assert::assertTrue($shuffleWorked, "The shuffle should have worked");

        /* reload to get the updated values from the data repository */
        $bookingRequestNewShuffled->reload();

        /* Check our booking request got a booking */
        Assert::assertNotNull(
            $bookingRequestNewShuffled->BookingId,
            "A booking should have been created"
        );

        Assert::equalTo($room3->Id)
            ->evaluate(
                $bookingRequestNewShuffled->Booking->RoomId,
                "The booking should be in room 3"
            );

        /* Check that the original bookings have new rooms */
        $bookingRoom2RoomIdBefore = $bookingRoom2->RoomId;
        $bookingRoom3RoomIdBefore = $bookingRoom3->RoomId;

        $bookingRoom2->reload();
        $bookingRoom3->reload();

        /* The rooms for the first two bookings should have at least moved */
        Assert::assertNotEquals(
            $bookingRoom2RoomIdBefore,
            $bookingRoom2->RoomId,
            "Booking in room 2 should have moved"
        );
        Assert::assertNotEquals(
            $bookingRoom3RoomIdBefore,
            $bookingRoom3->RoomId,
            "Booking in room 3 should have moved"
        );

        /* The rooms we know they should have moved to */
        Assert::assertEquals(
            $room1->Id,
            $bookingRoom2->RoomId,
            "Booking in room 2 should be in room 1"
        );
        Assert::assertEquals(
            $room2->Id,
            $bookingRoom3->RoomId,
            "Booking in room 3 should be in room 2"
        );
        Assert::assertEquals(
            $room3->Id,
            $bookingRequestNewShuffled->Booking->RoomId,

            "new request should be in room 3"
        );
    }
}