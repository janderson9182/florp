<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 27/03/18
 * Time: 18:44
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\RoomBookingServices\RoomBookingServiceRecursive;

use PHPUnit\Framework\Assert;
use Rhubarb\Crown\DateTime\RhubarbDateTime;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Services\RoomBookingServices\Entities\BookingEntity;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;
use SpaceCadets\Florp\Tests\Unit\Services\Assets\Room\RoomTestHelper;
use SpaceCadets\Florp\Tests\Unit\src\Models\Booking\BookingRequestHelper;

class BookingEntityTest extends FlorpTestCase
{
    use RoomTestHelper;
    use BookingRequestHelper;

    public function testImportSetsValues()
    {

        $Id = null;
        $UserId = 2;
        $RoomId = 3;
        $BookingRequestId = 4;
        $StartTime = new RhubarbDateTime("now");
        $EndTime = new RhubarbDateTime("now + 2 hours");
        $NumberOfSeats = 10;
        $Cancelled = false;
        $WillingToShare = false;
        $Locked = false;
        $CheckedIn = false;

        $booking = new Booking();

        $booking->UserId = $UserId;
        $booking->RoomId = $RoomId;
        $booking->BookingRequestId = $BookingRequestId;
        $booking->StartTime = $StartTime;
        $booking->EndTime = $EndTime;
        $booking->NumberOfSeats = $NumberOfSeats;
        $booking->Cancelled = $Cancelled;
        $booking->WillingToShare = $WillingToShare;
        $booking->Locked = $Locked;
        $booking->CheckedIn = $CheckedIn;

        $booking->save();
        $booking->reload();
        $Id = $booking->Id;

        $bookingEntity = new BookingEntity();
        $bookingEntity->import($booking->exportData());

        Assert::assertEquals($bookingEntity->Id, $booking->Id);
        Assert::assertEquals($bookingEntity->RoomId, $booking->RoomId);
        Assert::assertEquals($bookingEntity->BookingRequestId, $booking->BookingRequestId);
        Assert::assertEquals($bookingEntity->StartTime, $booking->StartTime);
        Assert::assertEquals($bookingEntity->EndTime, $booking->EndTime);
        Assert::assertEquals($bookingEntity->NumberOfSeats, $booking->NumberOfSeats);
        Assert::assertEquals($bookingEntity->Cancelled, $booking->Cancelled);
        Assert::assertEquals($bookingEntity->WillingToShare, $booking->WillingToShare);
        Assert::assertEquals($bookingEntity->Locked, $booking->Locked);
        Assert::assertEquals($bookingEntity->CheckedIn, $booking->$CheckedIn);
    }
}