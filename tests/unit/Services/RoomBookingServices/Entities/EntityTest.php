<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 29/03/2018
 * Time: 12:46
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\RoomBookingServices\Entities;

use PHPUnit\Framework\Assert;
use SpaceCadets\Florp\Tests\Unit\FlorpTestCase;

class EntityTest extends FlorpTestCase
{
    public $variables;

    public function setUp()
    {
        $this->variables = [
            "integer" => 134,
            "string" => "a string",
            "boolean" => true
        ];
    }

    public function testImport()
    {
        $entity = new TestingEntity();
        $entity->import($this->variables);
        foreach ($this->variables as $name => $value) {
            Assert::equalTo($value)->evaluate($entity->$name);
        }
    }

    public function testToArray()
    {
        $entity = new TestingEntity();
        $entity->import($this->variables);
        foreach ($entity->toArray() as $name => $value) {
            Assert::equalTo($this->variables[$name])->evaluate($value);
        }
    }

    public function testMagicGetAndSet()
    {
        $entity = new TestingEntity();
        foreach ($this->variables as $name => $value) {
            $entity->$name = $value;
            Assert::equalTo($value)->evaluate($entity->$name);
        }
    }
}