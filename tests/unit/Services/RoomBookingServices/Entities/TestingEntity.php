<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 29/03/2018
 * Time: 12:47
 */

namespace SpaceCadets\Florp\Tests\Unit\Services\RoomBookingServices\Entities;

use SpaceCadets\Florp\Services\RoomBookingServices\Entities\Entity;

class TestingEntity extends Entity
{
    /** @var int $integer */
    protected $integer;
    /** @var string $string */
    protected $string;
    /** @var bool $boolean */
    protected $boolean;
}