<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 18/02/2018
 * Time: 18:25
 */

namespace SpaceCadets\Florp;

use Rhubarb\Scaffolds\ApplicationSettings\Settings\ApplicationSettings;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingService;

/**
 * Class FlorpSettings
 * @package SpaceCadets\Florp
 * @property RoomBookingService $roomBookingProviderClass
 * @property string $adminEmailAddress
 */
class FlorpSettings extends ApplicationSettings
{
    /**
     * This class should not contain any settings variables of its own
     * Instead add the names of the variables you want to use into the doc blocks
     * @see vendor/rhubarbphp/scaffold-application-settings/docs/index.md
     */
}