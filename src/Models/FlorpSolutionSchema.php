<?php

namespace SpaceCadets\Florp\Models;

use Rhubarb\Stem\Schema\SolutionSchema;
use SpaceCadets\Florp\Models\Assets\Building;
use SpaceCadets\Florp\Models\Assets\Floor;
use SpaceCadets\Florp\Models\Assets\Properties\BookingRequestProperty;
use SpaceCadets\Florp\Models\Assets\Properties\Property;
use SpaceCadets\Florp\Models\Assets\Properties\RoomImage;
use SpaceCadets\Florp\Models\Assets\Properties\RoomProperty;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;

class FlorpSolutionSchema extends SolutionSchema
{
    const BOOKING_REQUEST = "BookingRequest";
    const BOOKING_REQUEST_PROPERTY = "BookingRequestProperty";
    const BOOKING = "Booking";

    public function __construct($version = 1.3)
    {
        parent::__construct($version);
        $this->addModel("Room", Room::class);
        $this->addModel("Floor", Floor::class);
        $this->addModel(self::BOOKING, Booking::class);
        $this->addModel("Property", Property::class);
        $this->addModel("RoomProperty", RoomProperty::class);
        $this->addModel("RoomImage", RoomImage::class);
        $this->addModel(self::BOOKING_REQUEST, BookingRequest::class);
        $this->addModel("User", FlorpUser::class);
        $this->addModel(self::BOOKING_REQUEST_PROPERTY, BookingRequestProperty::class);
        $this->addModel("Building",Building::class);
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\RelationshipDefinitionException
     */
    protected function defineRelationships()
    {
        $this->declareOneToManyRelationship(Floor::class, "Id", "Rooms", Room::class, "FloorId", "Floor");
        $this->declareOneToManyRelationship(Room::class, "Id", "Bookings", Booking::class, "RoomId", "Room");
        $this->declareOneToManyRelationship(Room::class, "Id", "Images", RoomImage::class, "RoomId", "Room");
        $this->declareOneToOneRelationship("BookingRequest", self::BOOKING, "BookingId", "Id", self::BOOKING);
        $this->declareOneToOneRelationship(self::BOOKING, self::BOOKING_REQUEST, "BookingRequestId", "Id", "BookingRequest");

        $this->declareOneToManyRelationships([
            "User" =>
                [
                    "BookingRequests" => "BookingRequest.UserId"
                ],
            "Building" =>
                [
                    "Floors" =>"Floor.BuildingId"
                ]
        ]);

        $this->declareManyToManyRelationships([
            "Room" => [
                "Properties" => "RoomProperty.RoomId_PropertyId.Property:Rooms"
            ],
            "BookingRequest" => [
                "Properties" => "BookingRequestProperty.BookingRequestId_PropertyId.Property:BookingRequests"
            ]
        ]);

    }
}
