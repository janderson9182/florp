<?php

namespace SpaceCadets\Florp\Models\Bookings;

use Rhubarb\Crown\Exceptions\RhubarbException;

class BookingAlgorithmNotSpecifiedException extends RhubarbException
{
}