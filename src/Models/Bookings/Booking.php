<?php
/**
 * Created by Team Space Cadets
 * User: james
 */
namespace SpaceCadets\Florp\Models\Bookings;

use Rhubarb\Crown\Sendables\Email\EmailProvider;
use Rhubarb\Crown\Sendables\Email\EmailSettings;
use Rhubarb\Stem\Models\Model;
use Rhubarb\Stem\Schema\Columns\AutoIncrementColumn;
use Rhubarb\Stem\Schema\Columns\BooleanColumn;
use Rhubarb\Stem\Schema\Columns\DateTimeColumn;
use Rhubarb\Stem\Schema\Columns\ForeignKeyColumn;
use Rhubarb\Stem\Schema\Columns\IntegerColumn;
use Rhubarb\Stem\Schema\ModelSchema;
use SpaceCadets\Florp\Models\FlorpUser;
use SpaceCadets\Florp\Services\src\Models\Bookings\CheckInToBookingUseCase;


/**
 *
 *
 * @property int $Id Repository field
 * @property int $UserId Repository field
 * @property int $RoomId Repository field
 * @property int $BookingRequestId Repository field
 * @property \Rhubarb\Crown\DateTime\RhubarbDateTime $StartTime Repository field
 * @property \Rhubarb\Crown\DateTime\RhubarbDateTime $EndTime Repository field
 * @property int $NumberOfSeats Repository field
 * @property bool $Cancelled Repository field
 * @property bool $WillingToShare Repository field
 * @property bool $Locked Repository field
 * @property-read \SpaceCadets\Florp\Models\Assets\Room $Room Relationship
 * @property-read BookingRequest $BookingRequest Relationship
 * @property bool $CheckedIn Repository field
 */
class Booking extends Model
{
    const COLUMN_BOOKINGREQUEST_ID = "BookingRequestId";
    public const COLUMN_START_TIME = "StartTime";
    const COLUMN_END_TIME = "EndTime";
    const COLUMN_CANCELLED = "Cancelled";
    const COLUMN_LOCKED = "Locked";
    const COLUMN_CHECKEDIN = "CheckedIn";

    const DateFormat = "D jS M Y H:i";

    /**
     * Returns the schema for this data object.
     *
     * @return \Rhubarb\Stem\Schema\ModelSchema
     */
    protected function createSchema()
    {

        $schema = new ModelSchema("Booking");
        $schema->addColumn(
            new AutoIncrementColumn("Id"),
            new ForeignKeyColumn("UserId"),
            new ForeignKeyColumn("RoomId"),
            new ForeignKeyColumn("BookingRequestId"),
            new DateTimeColumn(self::COLUMN_START_TIME),
            new DateTimeColumn(self::COLUMN_END_TIME),
            new IntegerColumn("NumberOfSeats"),
            new BooleanColumn(self::COLUMN_CANCELLED),
            new BooleanColumn("WillingToShare"),
            new BooleanColumn("CheckedIn"),
            new BooleanColumn(self::COLUMN_LOCKED)

        );

        return $schema;
    }
    public function checkIn()
    {
        CheckInToBookingUseCase::create()->execute($this);
    }

}