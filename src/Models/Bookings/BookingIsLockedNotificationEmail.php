<?php
/**
 * Created by PhpStorm.
 * User: ron_0
 * Date: 29/03/2018
 * Time: 00:43
 */

namespace SpaceCadets\Florp\Models\Bookings;


use Rhubarb\Crown\Sendables\Email\TemplateEmail;

class BookingIsLockedNotificationEmail extends TemplateEmail
{

    /**
     * @var Booking $Booking
     */

    protected $Booking;

    /**
     * BookingIsLockedNotificationEmail constructor
     * @param array $recipientData
     * @param Booking $booking
     */

    public function __construct($recipientData, Booking $booking)
    {
        parent::__construct($recipientData);
        $this->recipients = $recipientData;
        $this->Booking = $booking;
    }

    protected function getTextTemplateBody()
    {
        return $this->getHtml();
    }

    protected function getHtmlTemplateBody()
    {
        return <<<HTML
<h1>Your booking has been locked in successfully!</h1>
<h2>Your booking details:</h2>
<p><b>Your User ID:</b>{$this->Booking->UserId}</p>
<p><b>Your Booking Start Time:</b>{$this->Booking->StartTime}</p>
<p><b>Your Booking End Time:</b>{$this->Booking->EndTime}</p>
<p><b>Room Name:</b>{$this->Booking->Room->Name}</p>
<p><b>Room Location:</b>{$this->Booking->Room->Floor}.{$this->Booking->Room->Floor->Building}</p>
HTML;

    }

    protected function getSubjectTemplate()
    {
        return "Room Booking Successfully Locked In!";
    }

    public function getRecipientList(){
        return parent::getRecipientList();
    }
}