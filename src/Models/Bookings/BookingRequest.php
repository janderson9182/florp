<?php

/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 13-Dec-17
 * Time: 12:44
 */

namespace SpaceCadets\Florp\Models\Bookings;

use Rhubarb\Crown\Exceptions\ClassMappingException;
use Rhubarb\Stem\Models\Model;
use SpaceCadets\Florp\Routes;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingService;
use Rhubarb\Stem\Schema\Columns\AutoIncrementColumn;
use Rhubarb\Stem\Schema\Columns\BooleanColumn;
use Rhubarb\Stem\Schema\Columns\DateTimeColumn;
use Rhubarb\Stem\Schema\Columns\ForeignKeyColumn;
use Rhubarb\Stem\Schema\Columns\IntegerColumn;
use Rhubarb\Stem\Schema\ModelSchema;
use SpaceCadets\Florp\Models\Assets\Properties\BookingRequestProperty;
use SpaceCadets\Florp\Models\Assets\Properties\Property;
use SpaceCadets\Florp\Services\src\Models\Bookings\BookingRequests\CancelBookingRequestUseCase;
use SpaceCadets\Florp\Services\src\Models\Bookings\BookingRequests\GetBookingForBookingRequestUseCase;

/**
 *
 *
 * @property int $Id Repository field
 * @property int $UserId Repository field
 * @property \Rhubarb\Crown\DateTime\RhubarbDateTime $StartTime Repository field
 * @property \Rhubarb\Crown\DateTime\RhubarbDateTime $EndTime Repository field
 * @property int $BookingId Repository field
 * @property int $Duration Repository field
 * @property int $NumberOfSeats Repository field
 * @property bool $Cancelled Repository field
 * @property bool $WillingToShare Repository field
 * @property-read Booking $Booking Relationship
 * @property-read \SpaceCadets\Florp\Models\FlorpUser $User Relationship
 * @property-read BookingRequestProperty[]|\Rhubarb\Stem\Collections\RepositoryCollection $PropertiesRaw Relationship
 * @property-read Property[]|\Rhubarb\Stem\Collections\RepositoryCollection $Properties Relationship
 * @property bool $CheckedIn Repository field
 * @property-read mixed $ViewLink {@link getViewLink()}
 * @property bool $Failed Repository field
 */
class BookingRequest extends Model
{
    const COLUMN_USER_ID = "UserId";
    const COLUMN_START_TIME = "StartTime";
    const COLUMN_END_TIME = "EndTime";

    /**
     * Returns the schema for this data object.
     *
     * @return \Rhubarb\Stem\Schema\ModelSchema
     */
    protected function createSchema()
    {
        $schema = new ModelSchema("BookingRequest");
        $schema->addColumn(
            new AutoIncrementColumn("Id"),
            new ForeignKeyColumn(self::COLUMN_USER_ID),
            new DateTimeColumn(self::COLUMN_START_TIME),
            new DateTimeColumn(self::COLUMN_END_TIME),
            new ForeignKeyColumn("BookingId"),
            new IntegerColumn("Duration"),
            new IntegerColumn("NumberOfSeats"),
            new BooleanColumn("Cancelled"),
            new BooleanColumn("WillingToShare"),
            new BooleanColumn("Failed", false)
        );
        return $schema;
    }

    //Overwrite delete method incase of accidental deletion
    public function delete()
    {
        $this->cancel();
        $this->afterDelete();
        $this->raiseEvent("AfterDelete");
    }

    /**
     * Cancel is a parameter within bookingRequest and booking. The reason we don't just remove the entry of booking
     * is because we wish to analyse cancellations. In this way we still have a copy of deleted bookings in the db
     */
    public function cancel()
    {
        CancelBookingRequestUseCase::create()->execute($this);
    }

    public function addProperty(Property $property): BookingRequestProperty
    {
        $bookingRequestProperty = new BookingRequestProperty();
        $bookingRequestProperty->BookingRequestId = $this->Id;
        $bookingRequestProperty->PropertyId = $property->Id;
        $bookingRequestProperty->save();
        return $bookingRequestProperty;
    }

    public function getViewLink()
    {
        $link = "/" . Routes::APP;
        $link = $this->BookingId > 0 ? $link . Routes::BOOKING . $this->BookingId . "/" : $link . Routes::BOOKING_REQUEST . $this->Id . "/";
        return '<a href="' . $link . '" class="view-link"><i class="fa fa-eye fa-2" aria-hidden="true"></i> view</a>';
    }
}