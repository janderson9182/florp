<?php

/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 14-Jan-18
 * Time: 12:06
 */


namespace SpaceCadets\Florp\Models\Assets\Properties;

use Rhubarb\Stem\Models\Model;
use Rhubarb\Stem\Schema\Columns\AutoIncrementColumn;
use Rhubarb\Stem\Schema\Columns\ForeignKeyColumn;
use Rhubarb\Stem\Schema\ModelSchema;

/**
 *
 *
 * @property int $Id Repository field
 * @property int $BookingRequestId Repository field
 * @property int $PropertyId Repository field
 * @property-read \SpaceCadets\Florp\Models\Bookings\BookingRequest $BookingRequest Relationship
 * @property-read Property $Property Relationship
 */
class BookingRequestProperty extends Model
{
    protected function createSchema()
    {
        $schema = new ModelSchema("BookingRequestProperty");

        $schema->addColumn(
            new AutoIncrementColumn("Id"),
            new ForeignKeyColumn("BookingRequestId"),
            new ForeignKeyColumn("PropertyId")
        );

        return $schema;
    }
}