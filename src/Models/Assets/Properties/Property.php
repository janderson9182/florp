<?php

namespace SpaceCadets\Florp\Models\Assets\Properties;

use Rhubarb\Stem\Models\Model;
use Rhubarb\Stem\Schema\Columns\AutoIncrementColumn;
use Rhubarb\Stem\Schema\Columns\LongStringColumn;
use Rhubarb\Stem\Schema\Columns\StringColumn;
use Rhubarb\Stem\Schema\ModelSchema;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;


/**
 *
 *
 * @property int $Id Repository field
 * @property string $Name Repository field
 * @property string $Description Repository field
 * @property-read RoomProperty[]|\Rhubarb\Stem\Collections\RepositoryCollection $RoomsRaw Relationship
 * @property-read Room[]|\Rhubarb\Stem\Collections\RepositoryCollection $Rooms Relationship
 * @property-read BookingRequestProperty[]|\Rhubarb\Stem\Collections\RepositoryCollection $BookingRequestsRaw Relationship
 * @property-read BookingRequest[]|\Rhubarb\Stem\Collections\RepositoryCollection $BookingRequests Relationship
 */
class Property extends Model
{
    /**
     * Returns the schema for this data object.
     *
     * @return \Rhubarb\Stem\Schema\ModelSchema
     */
    protected function createSchema()
    {
        $schema = new ModelSchema("Property");
        $schema->addColumn(
            new AutoIncrementColumn("Id"),
            new StringColumn("Name", 255),
            new LongStringColumn("Description")
        );
        $schema->labelColumnName = "Name";
        return $schema;
    }

    protected function getPublicPropertyList()
    {
        $publicPropertyList = parent::getPublicPropertyList();
        $publicPropertyList[] = "Description";
        return $publicPropertyList;
    }

    public function addRoom(Room $room): RoomProperty
    {
        $roomProperty = new RoomProperty();
        $roomProperty->RoomId = $room->Id;
        $roomProperty->PropertyId = $this->Id;
        $roomProperty->save();
        return $roomProperty;
    }
    public function addBookingRequest(BookingRequest $bookingRequest): BookingRequestProperty
    {
        $bookingRequestProperty = new BookingRequestProperty();
        $bookingRequestProperty->BookingRequestId=$bookingRequest->Id;
        $bookingRequestProperty->PropertyId = $this->Id;
        $bookingRequestProperty->save();
        return $bookingRequestProperty;
    }
}