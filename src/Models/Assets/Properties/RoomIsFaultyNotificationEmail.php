<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 20/03/2018
 * Time: 19:22
 */

namespace SpaceCadets\Florp\Models\Assets\Properties;

use Rhubarb\Crown\Sendables\Email\TemplateEmail;

class RoomIsFaultyNotificationEmail extends TemplateEmail
{
    /**
     * @var RoomProperty $RoomProperty
     */
    protected $RoomProperty;

    /**
     * TempRoomIsFaultyNotificationEmail constructor.
     * @param array $recipientData
     * @param RoomProperty $roomProperty
     */
    public function __construct($recipientData, RoomProperty $roomProperty)
    {
        parent::__construct($recipientData);
        $this->recipients = $recipientData;
        $this->RoomProperty = $roomProperty;
    }

    protected function getTextTemplateBody()
    {
        return $this->getHtml();
    }

    protected function getHtmlTemplateBody()
    {
        return <<<HTML
<h1>Room Property Has been reported faulty!</h1>
<h2>Room Details</h2>
<p><b>Room Name:</b>{$this->RoomProperty->Room->Name}</p>
<p><b>Building</b>{$this->RoomProperty->Room->Floor->Building->Name}</p>
<p><b>Floor Number</b>{$this->RoomProperty->Room->Floor->FloorNumber}</p>
<h2>Property Details</h2>
<p><b>Property Name</b>{$this->RoomProperty->Property->Name}</p>
HTML;

    }

    protected function getSubjectTemplate()
    {
        return "Room Property has been reported faulty";
    }

    public function getRecipientList()
    {
        return parent::getRecipientList();
    }
}