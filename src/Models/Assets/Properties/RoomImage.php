<?php
/**
 * Created by PhpStorm.
 * User: james
 * Date: 04/12/2017
 * Time: 12:45
 */

namespace SpaceCadets\Florp\Models\Assets\Properties;

use Rhubarb\Stem\Models\Model;
use Rhubarb\Stem\Schema\Columns\AutoIncrementColumn;
use Rhubarb\Stem\Schema\Columns\ForeignKeyColumn;
use Rhubarb\Stem\Schema\Columns\LongStringColumn;
use Rhubarb\Stem\Schema\Columns\StringColumn;
use Rhubarb\Stem\Schema\ModelSchema;

/**
 *
 *
 * @property int $Id Repository field
 * @property int $RoomId Repository field
 * @property string $FilePath Repository field
 * @property string $ImageName Repository field
 * @property string $Description Repository field
 * @property-read \SpaceCadets\Florp\Models\Assets\Room $Room Relationship
 */
class RoomImage extends Model
{
    /**
     * @return ModelSchema
     */
    protected function createSchema()
    {
        $schema = new ModelSchema("RoomImage");
        $schema->addColumn(
            new AutoIncrementColumn("Id"),
            new ForeignKeyColumn("RoomId"),
            new StringColumn("FilePath", 255),
            new StringColumn("ImageName", 255),
            new LongStringColumn("Description")
        );

        return $schema;

    }
}