<?php

namespace SpaceCadets\Florp\Models\Assets\Properties;

use Rhubarb\Crown\Sendables\Email\EmailProvider;
use Rhubarb\Crown\Sendables\Email\EmailSettings;
use Rhubarb\Stem\Models\Model;
use Rhubarb\Stem\Schema\Columns\AutoIncrementColumn;
use Rhubarb\Stem\Schema\Columns\BooleanColumn;
use Rhubarb\Stem\Schema\Columns\ForeignKeyColumn;
use Rhubarb\Stem\Schema\ModelSchema;
use SpaceCadets\Florp\FlorpSettings;

/**
 *
 *
 * @property int $Id Repository field
 * @property int $RoomId Repository field
 * @property int $PropertyId Repository field
 * @property-read \SpaceCadets\Florp\Models\Assets\Room $Room Relationship
 * @property-read Property $Property Relationship
 * @property bool $Faulty Repository field
 */
class RoomProperty extends Model
{
    protected function createSchema()
    {
        $schema = new ModelSchema("RoomProperty");
        $schema->addColumn(
            new AutoIncrementColumn("Id"),
            new ForeignKeyColumn("RoomId"),
            new ForeignKeyColumn("PropertyId"),
            new BooleanColumn("Faulty")
        );
        return $schema;
    }

    public function setFaulty($value)
    {
        EmailSettings::singleton()->OnlyRecipient = false;
        $florpSettings = FlorpSettings::singleton();
        if ($value === true && $florpSettings->adminEmailAddress != "") {
            $email = new RoomIsFaultyNotificationEmail([$florpSettings->adminEmailAddress], $this);
            EmailProvider::selectProviderAndSend($email);
        }
        $this->modelData["Faulty"] = $value;
    }
}