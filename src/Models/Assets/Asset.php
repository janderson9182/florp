<?php

namespace SpaceCadets\Florp\Models\Assets;

use Rhubarb\Stem\Models\Model;
use Rhubarb\Stem\Schema\Columns\AutoIncrementColumn;
use Rhubarb\Stem\Schema\Columns\StringColumn;
use Rhubarb\Stem\Schema\ModelSchema;

abstract class Asset extends Model
{
    /**
     * Returns the schema for this data object.
     *
     * @return \Rhubarb\Stem\Schema\ModelSchema
     */
    protected function createSchema()
    {
        $schema = new ModelSchema($this->getSchemaName());
        $schema->addColumn(
            new AutoIncrementColumn("Id"),
            new StringColumn("Name", 255)
        );
        $schema->labelColumnName = "Name";

        $extendedSchema = $this->getDomainSpecificColumns($schema);

        return $extendedSchema;
    }

    /**
     * @param ModelSchema $modelSchema
     * @return ModelSchema
     *
     * Use this method to add anything extra that you need in your table, such as description,
     * alternatively, return $modelSchema to get the base columns only
     */
    abstract protected function getDomainSpecificColumns(ModelSchema $modelSchema);

    /**
     * @return string
     *
     * Use this to set the schema name for your specific table
     */
    abstract protected function getSchemaName();
}