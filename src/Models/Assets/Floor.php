<?php

namespace SpaceCadets\Florp\Models\Assets;

use Rhubarb\Stem\Schema\Columns\BooleanColumn;
use Rhubarb\Stem\Schema\Columns\ForeignKeyColumn;
use Rhubarb\Stem\Schema\Columns\IntegerColumn;
use Rhubarb\Stem\Schema\ModelSchema;


/**
 *
 *
 * @property int $Id Repository field
 * @property string $Name Repository field
 * @property int $FloorNumber Repository field
 * @property bool $WheelchairAccess Repository field
 * @property-read Room[]|\Rhubarb\Stem\Collections\RepositoryCollection $Rooms Relationship
 * @property int $BuildingId Repository field
 * @property-read Building $Building Relationship
 */
class Floor extends Asset
{
    /**
     * @param ModelSchema $modelSchema
     * @return ModelSchema
     *
     * Use this method to add anything extra that you need in your table, such as description,
     * alternatively, return $modelSchema to get the base columns only
     */
    protected function getDomainSpecificColumns(ModelSchema $modelSchema)
    {
        $modelSchema->addColumn(
            new IntegerColumn("FloorNumber"),
            new BooleanColumn("WheelchairAccess"),
            new ForeignKeyColumn("BuildingId")
        );
        return $modelSchema;
    }
    /**
     * @return string
     *
     * Use this to set the schema name for your specific table
     */
    protected function getSchemaName()
    {
        return "Floor";
    }
}