<?php
/**
 * Created by Team Space Cadets
 * User: james
 */

namespace SpaceCadets\Florp\Models\Assets;

use Rhubarb\Stem\Collections\Collection;
use Rhubarb\Stem\Collections\RepositoryCollection;
use Rhubarb\Stem\Filters\Equals;
use Rhubarb\Stem\Schema\Columns\BooleanColumn;
use Rhubarb\Stem\Schema\Columns\ForeignKeyColumn;
use Rhubarb\Stem\Schema\Columns\IntegerColumn;
use Rhubarb\Stem\Schema\Columns\LongStringColumn;
use Rhubarb\Stem\Schema\ModelSchema;
use SpaceCadets\Florp\Models\Assets\Properties\Property;
use SpaceCadets\Florp\Models\Assets\Properties\RoomProperty;

/**
 *
 *
 * @property int $Id Repository field
 * @property string $Name Repository field
 * @property string $Description Repository field
 * @property int $FloorId Repository field
 * @property int $NumberOfSeats Repository field
 * @property bool $CanBookIndividualSeats Repository field
 * @property-read Floor $Floor Relationship
 * @property-read \SpaceCadets\Florp\Models\Bookings\Booking[]|\Rhubarb\Stem\Collections\RepositoryCollection $Bookings Relationship
 * @property-read Properties\RoomImage[]|\Rhubarb\Stem\Collections\RepositoryCollection $Images Relationship
 * @property-read Properties\RoomProperty[]|\Rhubarb\Stem\Collections\RepositoryCollection $PropertiesRaw Relationship
 * @property-read Properties\Property[]|\Rhubarb\Stem\Collections\RepositoryCollection $Properties Relationship
 */
class Room extends Asset
{
    public const COLUMN_NUMBER_OF_SEATS = "NumberOfSeats";

    /**
     * @param ModelSchema $modelSchema
     * @return ModelSchema
     *
     * Use this method to add anything extra that you need in your table, such as description,
     * alternatively, return $modelSchema to get the base columns only
     */
    protected function getDomainSpecificColumns(ModelSchema $modelSchema)
    {
        $modelSchema->addColumn(
            new LongStringColumn("Description"),
            new ForeignKeyColumn("FloorId"),
            new IntegerColumn(self::COLUMN_NUMBER_OF_SEATS),
            new BooleanColumn("CanBookIndividualSeats")
        );
        return $modelSchema;
    }

    /**
     * @return string
     *
     * Use this to set the schema name for your specific table
     */
    protected function getSchemaName()
    {
        return "Room";
    }

    public function addProperty(Property $property): RoomProperty
    {
        $roomProperty = new RoomProperty();
        $roomProperty->RoomId = $this->Id;
        $roomProperty->PropertyId = $property->Id;
        $roomProperty->save();
        return $roomProperty;
    }

    /**
     * @param Property $property
     * @return bool
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    public function hasProperty(Property $property): bool
    {
        return $this->PropertiesRaw->filter(new Equals("PropertyId", $property->Id))->count() > 0;
    }

    /**
     * @param RepositoryCollection|Property[] $properties
     * @return bool
     * @throws \Rhubarb\Stem\Exceptions\FilterNotSupportedException
     */
    public function hasProperties(RepositoryCollection $properties)
    {
        if ($properties->count() < 1) {
            return true;
        }
        foreach ($properties as $property) {
            if (!$this->hasProperty($property)) {
                return false;
            }
        }
        return true;
    }
}