<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 06/03/18
 * Time: 21:03
 */

namespace SpaceCadets\Florp\Models\Assets;


use Rhubarb\Stem\Schema\ModelSchema;

/**
 *
 *
 * @property int $Id Repository field
 * @property string $Name Repository field
 * @property-read Floor[]|\Rhubarb\Stem\Collections\RepositoryCollection $Floors Relationship
 */
class Building extends Asset
{

    /**
     * @param ModelSchema $modelSchema
     * @return ModelSchema
     *
     * Use this method to add anything extra that you need in your table, such as description,
     * alternatively, return $modelSchema to get the base columns only
     */
    protected function getDomainSpecificColumns(ModelSchema $modelSchema)
    {
        return $modelSchema;
    }

    /**
     * @return string
     *
     * Use this to set the schema name for your specific table
     */
    protected function getSchemaName()
    {
        return "Building";
    }
}