<?php

namespace SpaceCadets\Florp;

use Rhubarb\Crown\Application;
use Rhubarb\Crown\Encryption\HashProvider;
use Rhubarb\Crown\Encryption\Sha512HashProvider;
use Rhubarb\Crown\Layout\LayoutModule;
use Rhubarb\Crown\LoginProviders\UrlHandlers\ValidateLoginUrlHandler;
use Rhubarb\Crown\Sendables\Email\EmailProvider;
use Rhubarb\Crown\Sendables\Email\PhpMailEmailProvider;
use Rhubarb\Crown\String\StringTools;
use Rhubarb\Crown\UrlHandlers\ClassMappedUrlHandler;
use Rhubarb\Leaf\Crud\CrudModule;
use Rhubarb\Leaf\Crud\UrlHandlers\CrudUrlHandler;
use Rhubarb\Leaf\LayoutProviders\LayoutProvider;
use Rhubarb\Leaf\LeafModule;
use Rhubarb\Scaffolds\ApplicationSettings\ApplicationSettingModule;
use Rhubarb\Scaffolds\Authentication\Leaves\Login;
use Rhubarb\Scaffolds\Authentication\LoginProviders\LoginProvider;
use Rhubarb\Scaffolds\AuthenticationWithRoles\AuthenticationWithRolesModule;
use Rhubarb\Scaffolds\BackgroundTasks\BackgroundTasksModule;
use Rhubarb\Stem\Custard\SeedDemoDataCommand;
use Rhubarb\Stem\Repositories\MySql\MySql;
use Rhubarb\Stem\Repositories\Repository;
use Rhubarb\Stem\Schema\SolutionSchema;
use Rhubarb\Stem\StemModule;
use SpaceCadets\Florp\DataSeeder\FlorpDataSeeder;
use SpaceCadets\Florp\DataSeeder\SmallestFirstDemoDataSeeder;
use SpaceCadets\Florp\Models\Assets\Building;
use SpaceCadets\Florp\Models\Assets\Floor;
use SpaceCadets\Florp\Models\Assets\Properties\Property;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\Booking;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Models\FlorpSolutionSchema;
use SpaceCadets\Florp\Models\FlorpUser;
use SpaceCadets\Florp\Services\RoomBookingServices\GraphColouring\RoomBookingServiceGraphColouringProvider;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingService;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceRecursive\RoomBookingServiceRecursiveProvider;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceRecursive\RoomBookingServiceRecursiveSort;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceSmallestFirst\RoomBookingServiceSmallestFirstProvider;
use SpaceCadets\Florp\Website\Admin\Assets\Building\BuildingCollection;
use SpaceCadets\Florp\Website\Admin\Assets\Floor\FloorCollection;
use SpaceCadets\Florp\Website\Admin\Assets\Properties\PropertiesCollection;
use SpaceCadets\Florp\Website\Admin\Assets\Room\RoomCollection;
use SpaceCadets\Florp\Website\Admin\Settings\Settings;
use SpaceCadets\Florp\Website\Admin\Statistics\Statistic;
use SpaceCadets\Florp\Website\Admin\Users\UsersCollection;
use SpaceCadets\Florp\Website\BookingRequest\Booking\BookingCollection;
use SpaceCadets\Florp\Website\BookingRequest\BookingRequestCollection;
use SpaceCadets\Florp\Website\Index\Index;
use SpaceCadets\Florp\Website\Layouts\DefaultLayout;
use SpaceCadets\Florp\Website\Layouts\FlorpLayoutProvider;
use SpaceCadets\Florp\Website\Login\SiteLogin;
use SpaceCadets\Florp\Website\Pi\CheckInUrlHandler;
use SpaceCadets\Florp\Website\SplashPage\SplashPage;
use SpaceCadets\Florp\Website\TimeTable\TimeTable;

class Florp extends Application
{
    /**
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    protected function initialise()
    {
        parent::initialise();
        $this->developerMode = true;
        if (file_exists(APPLICATION_ROOT_DIR . "/settings/site.config.php")) {
            include_once(APPLICATION_ROOT_DIR . "/settings/site.config.php");
        }

        LayoutProvider::setProviderClassName(FlorpLayoutProvider::class);
        HashProvider::setProviderClassName(Sha512HashProvider::class);
        LoginProvider::setProviderClassName(SiteLogin::class);
        SolutionSchema::registerSchema("Florp", FlorpSolutionSchema::class);
        Repository::setDefaultRepositoryClassName(MySql::class);
        EmailProvider::setProviderClassName(PhpMailEmailProvider::class);

        RoomBookingService::registerRoomBookingServiceClass(RoomBookingServiceSmallestFirstProvider::class);
        RoomBookingService::registerRoomBookingServiceClass(RoomBookingServiceRecursiveProvider::class);
        RoomBookingService::registerRoomBookingServiceClass(RoomBookingServiceGraphColouringProvider::class);
    }

    protected function registerUrlHandlers()
    {
        parent::registerUrlHandlers();
        $this->addUrlHandlers(
            [
                "/pi" => new CheckInUrlHandler()
            ]
        );
        $this->addUrlHandlers(
            ["/" . Routes::APP => new ValidateLoginUrlHandler(new SiteLogin(), "/login/")]
        );

        $this->addUrlHandlers(
            [
                "/" => new ClassMappedUrlHandler(SplashPage::class, [
                    Routes::APP => new ClassMappedUrlHandler(Index::class, [
                        Routes::LOGIN => new ClassMappedUrlHandler(Login::class),
                        Routes::ADMIN_BUILDING => new CrudUrlHandler(Building::class,
                            StringTools::getNamespaceFromClass(BuildingCollection::class)),
                        Routes::ADMIN_FLOOR => new CrudUrlHandler(Floor::class,
                            StringTools::getNamespaceFromClass(FloorCollection::class)),
                        Routes::ADMIN_ROOM => new CrudUrlHandler(Room::class,
                            StringTools::getNamespaceFromClass(RoomCollection::class)),
                        Routes::ADMIN_USER => new CrudUrlHandler(FlorpUser::class,
                            StringTools::getNamespaceFromClass(UsersCollection::class)),
                        Routes::ADMIN_PROPERTY => new CrudUrlHandler(Property::class,
                            StringTools::getNamespaceFromClass(PropertiesCollection::class)),
                        Routes::ADMIN_PROPERTY => new CrudUrlHandler(Property::class,
                            StringTools::getNamespaceFromClass(PropertiesCollection::class)),
                        Routes::BOOKING_REQUEST => new CrudUrlHandler(BookingRequest::class,
                            StringTools::getNamespaceFromClass(BookingRequestCollection::class)),
                        Routes::ADMIN_SETTINGS => new ClassMappedUrlHandler(Settings::class),
                        Routes::ADMIN_STATISTICS=> new ClassMappedUrlHandler(Statistic::class),
                        Routes::TIMETABLE => new ClassMappedUrlHandler(TimeTable::class),

                        Routes::BOOKING => new CrudUrlHandler(Booking::class,
                            StringTools::getNamespaceFromClass(BookingCollection::class))
                    ])
                ])
            ]
        );
    }

    protected function getModules()
    {
        return [
            new LayoutModule(DefaultLayout::class),
            new LeafModule(),
            new StemModule(),
            new CrudModule(),
            new AuthenticationWithRolesModule(SiteLogin::class, "/" . Routes::APP),
            new BackgroundTasksModule(),
            new ApplicationSettingModule()
        ];
    }

    public function getCustardCommands()
    {
        SeedDemoDataCommand::registerDemoDataSeeder(new FlorpDataSeeder());
        return parent::getCustardCommands();
    }
}

