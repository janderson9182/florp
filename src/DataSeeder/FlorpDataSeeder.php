<?php

namespace SpaceCadets\Florp\DataSeeder;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Crown\Exceptions\RhubarbException;
use SpaceCadets\Florp\FlorpSettings;
use SpaceCadets\Florp\Models\Assets\Building;
use SpaceCadets\Florp\Models\Assets\Floor;
use SpaceCadets\Florp\Models\Assets\Properties\Property;
use SpaceCadets\Florp\Models\Assets\Properties\RoomImage;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Models\FlorpUser;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceSmallestFirst\RoomBookingServiceSmallestFirstProvider;
use Symfony\Component\Console\Output\OutputInterface;

class FlorpDataSeeder extends AbstractFlorpDataSeeder
{
    /**
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    protected function seedUsers()
    {
        parent::seedUsers();

        $userQub = new FlorpUser();
        $userQub->Forename = "Qub";
        $userQub->Surname = "Queens";
        $userQub->Email = "Space@qub.ac.uk";
        $userQub->Username = "Qub";
        $userQub->setNewPassword("password");
        $userQub->save();



        $userCiti = new FlorpUser();
        $userCiti->Forename = "Citi";
        $userCiti->Surname = "Bank";
        $userCiti->Email="Space@citi.com";
        $userCiti->Username="Citi";
        $userCiti->setNewPassword("password");
        $userCiti->save();


    }

    protected function seedRooms()
    {
        $rooms = ["0G006 Malone rd", "0G001 CSB Lab", "1G CSB Lab"];
        $roomArr = [];
        foreach ($rooms as $room) {
            $roomArr[] = $this->generateRoom($room);
        }
        $floor = $this->generateFloor();
        $properties = ["Projector", "Computer", "Flipchart"];
        $propArr = [];
        foreach ($properties as $property) {
            $propArr[] = $this->generatePropertyFromString($property);
        }
        /** @var Room $room */
        foreach ($roomArr as $room) {
            $floor->Rooms->append($room);
            foreach ($propArr as $property) {
                $room->addProperty($property);
            }
        }
    }

    /**
     * @return Floor
     */
    private function generateFloor()
    {
        $csb = new Building();
        $csb->Name="Computer Science Building";
        $csb->save();
        $floor = new Floor();
        $floor->Name = "Ground Floor";
        $floor->WheelchairAccess = true;
        $floor->BuildingId=$csb->Id;
        $floor->save();
        return $floor;
    }

    /**
     * @param string $propertyName
     * @return Property
     */
    private function generatePropertyFromString(string $propertyName)
    {
        $property = new Property();
        $property->Name = $propertyName;
        $property->Description = $this->getLoremIpsum();
        $property->save();
        return $property;
    }

    /**
     * @param string $roomName
     * @return Room
     */
    private function generateRoom(string $roomName)
    {
        $room = new Room();
        $room->Name = $roomName;
        $room->Description = $this->getLoremIpsum();
        $room->NumberOfSeats = 100;
        $room->CanBookIndividualSeats = true;

        $room->save();

        $roomImage = new RoomImage();
        $roomImage->FilePath = "https://s3.us-east-2.amazonaws.com/florpdata/room-images/sample-meeting-room.jpg";
        $roomImage->ImageName = "Sample room image";
        $roomImage->Description = "Labled for reuse: https://www.flickr.com/photos/publiclibrariesnsw/20882150555";
        $roomImage->RoomId = $room->Id;
        $roomImage->save();
        return $room;
    }

    public function generateBookingRequest(FlorpUser $user): BookingRequest
    {
        $bookingRequest = new BookingRequest();
        $bookingRequest->UserId = $user->UniqueIdentifier;
        $bookingRequest->StartTime = new RhubarbDateTime("now");
        $bookingRequest->EndTime = new RhubarbDateTime("now");
        $bookingRequest->NumberOfSeats = 10;
        $bookingRequest->wilingToShare = false;
        $bookingRequest->save();
        return $bookingRequest;
    }

    protected function seedBookings()
    {
        $this->generateBookingRequest($this->getFlorpUser());
        $this->generateBookingRequest($this->getFlorpUser());
        $this->generateBookingRequest($this->getFlorpUser());
        $this->generateBookingRequest($this->getFlorpUser());
    }

    protected function seedSettings()
    {
        $florpSettings = FlorpSettings::singleton();
        $florpSettings->roomBookingProviderClass = RoomBookingServiceSmallestFirstProvider::class;
    }


}