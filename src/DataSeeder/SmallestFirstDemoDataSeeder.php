<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 18/02/2018
 * Time: 17:56
 */

namespace SpaceCadets\Florp\DataSeeder;

use Rhubarb\Crown\DateTime\RhubarbDateTime;
use Rhubarb\Stem\Filters\Equals;
use SpaceCadets\Florp\Florp;
use SpaceCadets\Florp\FlorpSettings;
use SpaceCadets\Florp\Models\Assets\Floor;
use SpaceCadets\Florp\Models\Assets\Room;
use SpaceCadets\Florp\Models\Bookings\BookingRequest;
use SpaceCadets\Florp\Models\FlorpUser;
use SpaceCadets\Florp\Services\RoomBookingServices\RoomBookingServiceSmallestFirst\RoomBookingServiceSmallestFirstProvider;
use SpaceCadets\Florp\Services\src\Models\Bookings\CreateBookingFromRequestUseCase;

class SmallestFirstDemoDataSeeder extends AbstractFlorpDataSeeder
{
    /**
     * @var Floor
     */
    private $groundFloor;
    /**
     * @var FlorpUser
     */
    private $cameronUser;

    /**
     * @return FlorpUser
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    private function getCameronUser() : FlorpUser
    {
        if(!isset($this->cameronUser))
        {
            $this->cameronUser = new FlorpUser();
            $this->cameronUser->Forename = "Cameron";
            $this->cameronUser->Surname = "Sharp";
            $this->cameronUser->Email = "csharp49@qub.ac.uk";
            $this->cameronUser->Username = "csharp49";
            $this->cameronUser->setNewPassword("password");
            $this->cameronUser->save();
        }
        return $this->cameronUser;
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    protected function seedRooms()
    {
        $this->seedProjectRooms();
        $this->seedComputerLabs();
        $this->seedLectureTheatres();
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     * @throws \Rhubarb\Stem\Exceptions\RecordNotFoundException
     */
    protected function seedBookings()
    {
        $bookingRequest = new BookingRequest();
        $bookingRequest->UserId = $this->getCameronUser()->UserID;
        $bookingRequest->StartTime = new RhubarbDateTime("now");
        $bookingRequest->EndTime = new RhubarbDateTime("+ 5 hours");
        $bookingRequest->NumberOfSeats = 1;
        $bookingRequest->Cancelled = false;
        $bookingRequest->WillingToShare = false;
        $bookingRequest->save();
        $booking = CreateBookingFromRequestUseCase::create()->execute($bookingRequest);
        $booking->RoomId = 1;
        $booking->save();
    }

    /**
     * @param Room $room
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    private function addRoomToGroundFloor(Room $room)
    {
        if (!isset($this->groundFloor)) {
            $this->groundFloor = new Floor();
            $this->groundFloor->Name = "Ground Floor";
            $this->groundFloor->FloorNumber = 0;
            $this->groundFloor->WheelchairAccess = true;
            $this->groundFloor->save();
        }

        $this->groundFloor->Rooms->append($room);
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    private function seedProjectRooms()
    {
        $room = new Room();
        $room->Name = "Green Project Room";
        $room->Description = "Project room with green wall and a small table";
        $room->NumberOfSeats = 6;
        $room->save();
        $this->addRoomToGroundFloor($room);

        $room = new Room();
        $room->Name = "Red Project Room";
        $room->Description = "Project room with red wall and a large table";
        $room->NumberOfSeats = 12;
        $room->save();
        $this->addRoomToGroundFloor($room);

        $room = new Room();
        $room->Name = "Picnic Project Room";
        $room->Description = "Project room with yellow wall and 4 picnic tables";
        $room->NumberOfSeats = 24;
        $room->save();
        $this->addRoomToGroundFloor($room);
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    private function seedComputerLabs()
    {
        $room = new Room();
        $room->Name = "Malone Road 14";
        $room->Description = "Small Computer Lab with 16 chairs at computers";
        $room->NumberOfSeats = 16;
        $room->save();
        $this->addRoomToGroundFloor($room);

        $room = new Room();
        $room->Name = "Malone Road 6";
        $room->Description = "Small Computer Lab with 20 chairs at computers";
        $room->NumberOfSeats = 20;
        $room->save();
        $this->addRoomToGroundFloor($room);

        $room = new Room();
        $room->Name = "OG 14";
        $room->Description = "Large Computer lab with 64 Computers";
        $room->NumberOfSeats = 64;
        $room->save();
        $this->addRoomToGroundFloor($room);
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    private function seedLectureTheatres()
    {
        $room = new Room();
        $room->Name = "Lecture Theatre";
        $room->Description = "Medium sized lecture theatre with two projectors and a white board";
        $room->NumberOfSeats = 100;
        $room->save();
        $this->addRoomToGroundFloor($room);
    }

    protected function seedSettings()
    {
        $florpSettings = FlorpSettings::singleton();
        $florpSettings->roomBookingProviderClass = RoomBookingServiceSmallestFirstProvider::class;
    }
}