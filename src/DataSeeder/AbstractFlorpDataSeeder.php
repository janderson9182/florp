<?php
/**
 * Created by Team Space Cadets
 * User: james
 * Date: 18/02/2018
 * Time: 17:40
 */

namespace SpaceCadets\Florp\DataSeeder;

use Rhubarb\Scaffolds\AuthenticationWithRoles\Role;
use Rhubarb\Stem\Custard\DemoDataSeederInterface;
use SpaceCadets\Florp\Models\FlorpUser;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractFlorpDataSeeder implements DemoDataSeederInterface
{
    /**
     * @var FlorpUser
     */
    private $florpUser;

    private $loremIpsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum";

    /**
     * @param OutputInterface $output
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    public function seedData(OutputInterface $output)
    {
        $this->seedUsers();
        $this->seedRooms();
        $this->seedBookings();
        $this->seedSettings();

    }

    protected function getFlorpUser()
    {
        return $this->florpUser;
    }

    protected function getLoremIpsum()
    {
        return $this->loremIpsum;
    }

    /**
     * @throws \Rhubarb\Stem\Exceptions\ModelConsistencyValidationException
     * @throws \Rhubarb\Stem\Exceptions\ModelException
     */
    protected function seedUsers()
    {
        $user = new FlorpUser();
        $user->Forename = "Florp";
        $user->Surname = "SpaceCadet";
        $user->Email = "Space@florp.com";
        $user->Username = "Florp";
        $user->setNewPassword("password");
        $user->save();
        $this->florpUser = $user;

        $role = new Role();
        $role->RoleName = FlorpUser::USER_ROLE_ADMIN;
        $role->save();

        $user->addToRole($role);
        $user->save();
    }

    abstract protected function seedRooms();
    abstract protected function seedBookings();
    abstract protected function seedSettings();
}