<?php
/**
 * Created by PhpStorm.
 * User: stephen
 * Date: 26-Feb-18
 * Time: 19:03
 */

use SpaceCadets\Florp\Services\src\Models\Bookings\CreateCheckInForRoomUseCase;

include_once __DIR__ . "/../../vendor/rhubarbphp/rhubarb/platform/execute-cli.php";

CreateCheckInForRoomUseCase::create()->execute(1);