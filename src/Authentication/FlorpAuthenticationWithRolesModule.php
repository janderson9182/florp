<?php
/**
 * Created by PhpStorm.
 * User: james
 * Date: 14/12/2017
 * Time: 12:51
 */

namespace SpaceCadets\Florp\Authentication;

use Rhubarb\Crown\LoginProviders\LoginProvider;
use Rhubarb\Scaffolds\AuthenticationWithRoles\AuthenticationWithRolesModule;

class FlorpAuthenticationWithRolesModule extends AuthenticationWithRolesModule
{
    /**
     * FlorpAuthenticationWithRolesModule constructor.
     * @param null $loginProviderClassName
     * @param string $urlToProtect
     * @param string $loginUrl
     */
    public function __construct($loginProviderClassName = null, $urlToProtect = '/', $loginUrl = '/login/')
    {
        // From Module
        $this->moduleName = str_ireplace("Module", "", get_class($this));

        // From AuthenticationModule
        if ($loginProviderClassName != null) {
            LoginProvider::setProviderClassName($loginProviderClassName);
        }

        if ($loginProviderClassName !== null) {
            $this->registerProtectedUrl(new FlorpProtectedUrl(
                $urlToProtect,
                $loginProviderClassName,
                $loginUrl
            ));
        }
    }
}