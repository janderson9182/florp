<?php
/**
 * Created by PhpStorm.
 * User: james
 * Date: 14/12/2017
 * Time: 12:56
 */

namespace SpaceCadets\Florp\Authentication;

use Rhubarb\Scaffolds\Authentication\Settings\ProtectedUrl;
use SpaceCadets\Florp\Website\Login\FlorpLogin;

class FlorpProtectedUrl extends ProtectedUrl
{
    public $loginLeafClassName = FlorpLogin::class;
}