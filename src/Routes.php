<?php

namespace SpaceCadets\Florp;

final class Routes
{
    const APP = "app/";
    const LOGIN = "login/";
    const ADMIN_FLOOR = "floor/";
    const ADMIN_ROOM = "room/";
    const ADMIN_PROPERTY = "property/";
    const ADMIN_USER = "user/";
    const ADMIN_STATISTICS = "statistics/";
    const BOOKING = "bookings/";
    const BOOKING_REQUEST = "booking-request/";
    const PI = "pi/";
    const ADMIN_SETTINGS = "settings/";
    const ADMIN_BUILDING = "building/";
    const TIMETABLE ="timetable/";
}